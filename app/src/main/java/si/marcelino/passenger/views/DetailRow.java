package si.marcelino.passenger.views;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import si.marcelino.passenger.R;

/**
 * Created by Andraž Hribar on 7.5.2017.
 * andraz.hribar@gmail.com
 */

public class DetailRow extends RelativeLayout {

    //region Variables

    @DrawableRes
    private int mImage;
    @StringRes
    private int mTitle;
    private String mValue;
    private Type mType = Type.SINGLE_LINE;
    private boolean mOffset = false;

    //endregion Variables

    //region UI Elements

    @BindView(R.id.icon_image)
    ImageView mIconImage;

    @BindView(R.id.title_text)
    TextView mTitleText;

    @BindView(R.id.value_text)
    TextView mValueText;

    //endregion UI Elements

    //region Constructor

    public DetailRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        initiate(context, attrs);
        setupContent(context);
    }

    //endregion Constructor

    //region Private Methods

    private void initiate(Context context, AttributeSet attrs) {
        inflate(context, R.layout.view_detail_row, this);
        ButterKnife.bind(this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DetailRow, 0, 0);
        try {
            mImage = a.getResourceId(R.styleable.DetailRow_row_image, 0);
            mTitle = a.getResourceId(R.styleable.DetailRow_row_title, 0);
            mValue = a.getString(R.styleable.DetailRow_row_value);
            mType = Type.values()[a.getInteger(R.styleable.DetailRow_row_type, 0)];
            mOffset = a.getBoolean(R.styleable.DetailRow_row_offset, false);
        } finally {
            a.recycle();
        }

        if (mType == Type.SINGLE_LINE) {
            setMinimumHeight(context.getResources().getDimensionPixelSize(R.dimen.row_height_1_line));
        }  else if (mType == Type.DOUBLE_LINE) {
            setMinimumHeight(context.getResources().getDimensionPixelSize(R.dimen.row_height_2_line));
        }
    }

    private void setupContent(Context context) {
        if (mImage != 0) {
            mIconImage.setImageDrawable(ContextCompat.getDrawable(context, mImage));
            mIconImage.setVisibility(VISIBLE);
        } else if (!mOffset) {
            mIconImage.setVisibility(GONE);
        } else {
            mIconImage.setVisibility(VISIBLE);
            mIconImage.setImageDrawable(null);
        }

        if (mTitle != 0) {
            mTitleText.setText(mTitle);
        }

        mValueText.setText(mValue);
    }

    //endregion Private Methods

    //region Public Methods

    public void setValueText(String text) {
        mValueText.setText(text);
    }

    //endregion Public Methods

    //region Nested Classes

    private enum Type {
        SINGLE_LINE, DOUBLE_LINE
    }

    //endregion Nested Classes
}
