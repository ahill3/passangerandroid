package si.marcelino.passenger.views;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import static android.view.View.MeasureSpec.EXACTLY;

/**
 * A layout extension that automatically sets its width and height the same.
 * <p>
 * Created by Andraž Hribar on 5.3.2017.
 * andraz.hribar@gmail.com
 */

public class SquareButton extends androidx.appcompat.widget.AppCompatButton {

    public SquareButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int size = MeasureSpec.makeMeasureSpec(width > height ? height : width, EXACTLY);
        super.onMeasure(size, size);
    }
}
