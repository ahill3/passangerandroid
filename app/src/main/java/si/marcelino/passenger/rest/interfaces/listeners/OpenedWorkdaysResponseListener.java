package si.marcelino.passenger.rest.interfaces.listeners;

import java.util.List;

import si.marcelino.passenger.rest.models.data.WorkdayData;

/**
 * Created by Andraž Hribar on 10.4.2017.
 * andraz.hribar@gmail.com
 */

public interface OpenedWorkdaysResponseListener extends BaseResponseListener {
    void onSuccess(List<WorkdayData> workdayDataList);
}
