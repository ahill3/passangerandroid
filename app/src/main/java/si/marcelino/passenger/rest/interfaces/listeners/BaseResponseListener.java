package si.marcelino.passenger.rest.interfaces.listeners;

import si.marcelino.passenger.rest.enums.FailureType;

/**
 * Created by Andraž Hribar on 7.3.2017.
 * andraz.hribar@gmail.com
 */

public interface BaseResponseListener {
    void onLoadFailure(FailureType failureType);
}
