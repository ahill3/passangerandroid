package si.marcelino.passenger.rest.managers;

import android.content.Context;
import androidx.collection.LongSparseArray;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.ModelMapper;
import si.marcelino.passenger.cache.models.domain.Image;
import si.marcelino.passenger.cache.models.domain.Report;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.cache.models.mapping.ContractMap;
import si.marcelino.passenger.cache.models.mapping.ReportMap;
import si.marcelino.passenger.cache.models.mapping.StoreMap;
import si.marcelino.passenger.cache.models.mapping.UserMap;
import si.marcelino.passenger.rest.clients.ApiClient;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.IdResponseListener;
import si.marcelino.passenger.rest.interfaces.listeners.ListResponseListener;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.models.data.ContractData;
import si.marcelino.passenger.rest.models.data.PointData;
import si.marcelino.passenger.rest.models.data.ReportData;
import si.marcelino.passenger.rest.models.data.StoreData;
import si.marcelino.passenger.rest.models.data.WorkdayData;
import si.marcelino.passenger.rest.models.requests.ActivateContractRequest;
import si.marcelino.passenger.rest.models.requests.AddAccountRequest;
import si.marcelino.passenger.rest.models.requests.AddReportRequest;
import si.marcelino.passenger.rest.models.requests.WorkdayEndRequest;
import si.marcelino.passenger.rest.models.requests.WorkdayStartRequest;
import si.marcelino.passenger.rest.utils.ApiUtils;
import si.marcelino.passenger.rest.utils.WorkdayUtils;
import si.marcelino.passenger.utils.SettingsUtil;
import si.marcelino.passenger.utils.TrackingUtil;
import si.marcelino.passenger.utils.models.Photo;

import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_CONTRACT_ID;

/**
 * Created by Andraž Hribar on 27.3.2017.
 * andraz.hribar@gmail.com
 */

public class PassengerApiManager {

    public static void profile(final Context context, final ResponseListener listener) {
        User user = DataManager.getInstance().getUser();
        if (user == null) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        String accessToken = user.getAccessToken();
        String refreshToken = user.getRefreshToken();
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .accountsMe(ApiUtils.createAuthorization(accessToken)),
                accountsResponse -> {
                    UserMap userMap =
                            ModelMapper.createUserMap(accountsResponse, accessToken, refreshToken);
                    DataManager.getInstance().storeUser(userMap);
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void register(final Context context,
                                final String address,
                                final String email,
                                final String firstName,
                                final String lastName,
                                final String password,
                                final String phone,
                                final int postCode,
                                final String postName,
                                final ResponseListener listener) {
        AddAccountRequest addAccountRequest = new AddAccountRequest(address, email, firstName,
                lastName, password, phone, postCode, postName);
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .addAccount("registration", addAccountRequest),
                accountsResponse -> {
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void getReports(final Context context, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .getReports(ApiUtils.createAuthorization(accessToken)),
                reports -> {
                    DataManager.getInstance().removeReports(true);
                    Long contractId = SettingsUtil.readLongPreference(context, PARAM_OPEN_WORKDAY_CONTRACT_ID);
                    if (contractId == null || contractId <= 0) {
                        if (listener != null) {
                            listener.onSuccess();
                        }
                        return;
                    }
                    List<ReportMap> reportMapList = new ArrayList<>();
                    for (ReportData reportData : reports) {
                        if (reportData.getContract() == null ||
                                reportData.getContract().getId().longValue() != contractId.longValue()) {
                            continue;
                        }
                        PointData point = reportData.getPoint();
                        ReportMap reportMap = ModelMapper.createReportMap(reportData, contractId, true,
                                point.getLatitude(), point.getLongitude());
                        reportMapList.add(reportMap);
                    }
                    DataManager.getInstance().storeReports(reportMapList);
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> {
                    ApiUtils.handleErrorCallback(throwable, listener);
                },
                listener);

    }

    public static void addReport(final Context context, final long reportId,
                                 final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        Report report = DataManager.getInstance().getReport(reportId);
        if (TextUtils.isEmpty(accessToken) || report == null || report.getContractId() == null ||
                report.getLatitude() == null || report.getLongitude() == null) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }

        if (report.isUploaded() && !report.areImagesUploaded()) {
            Log.d("PassengerApiManager", "re-upload images of report " + reportId);
            List<Image> images = report.getImages();
            LongSparseArray<Photo> photoQueue = new LongSparseArray<>();
            if (images != null && !images.isEmpty()) {
                for (final Image image : images) {
                    if (image.isUploaded()) {
                        continue;
                    }
                    Photo photo = new Photo(image.getPrimaryKey(), reportId, report.getLatitude(),
                            report.getLongitude(), new File(image.getFilePath()));
                    photoQueue.put(photo.getImageId(), photo);
                }
                uploadImages(context, reportId, photoQueue, listener);
            }
            return;
        }

        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .addReport(ApiUtils.createAuthorization(accessToken),
                                new AddReportRequest(report.getContractId(),
                                        report.getDescription(), report.getName(),
                                        report.getStoreName(), report.getLatitude(),
                                        report.getLongitude())),
                reportData -> {
                    DataManager.getInstance().syncReport(reportId, reportData.getId());
                    Report r = DataManager.getInstance().getReport(reportData.getId());
                    if (r == null || !r.isValid()) {
                        listener.onLoadFailure(FailureType.REQUEST_ERROR);
                        return;
                    }
                    final Long rId = r.getId(); // Gets the new ID generated by backend
//                    final Double lat = r.getLatitude();
//                    final Double lng = r.getLongitude();
                    List<Image> images = r.getImages();
//                    final Map<String, Integer> uploadMap = new HashMap<>();
                    LongSparseArray<Photo> photoQueue = new LongSparseArray<>();
                    if (images != null && !images.isEmpty()) {
                        for (final Image image : images) {
                            Photo photo = new Photo(image.getPrimaryKey(), rId, r.getLatitude(), r.getLongitude(),
                                    new File(image.getFilePath()));

                            photoQueue.put(photo.getImageId(), photo);
//                            final String fileName = image.getFilePath();
//                            final File file = new File(fileName);
//                            uploadMap.put(fileName, 0);
//                            addPhoto(context, lat, lng, rId, file, new ResponseListener() {
//                                @Override
//                                public void onSuccess() {
//                                    uploadMap.remove(fileName);
//                                    DataManager.getInstance()
//                                            .setImageUploaded(image.getPrimaryKey(), true);
//                                    if (uploadMap.isEmpty() && listener != null) {
//                                        listener.onSuccess();
//                                    }
//                                }
//
//                                @Override
//                                public void onLoadFailure(FailureType failureType) {
//                                    Integer tryCount = uploadMap.get(fileName);
//                                    DataManager.getInstance()
//                                            .setImageUploaded(image.getPrimaryKey(), false);
//                                    if (tryCount != null && tryCount < 3) {
//                                        uploadMap.put(fileName, tryCount + 1);
//                                        addPhoto(context, lat, lng, rId, file, this);
//                                    } else if (listener != null) {
//                                        listener.onLoadFailure(FailureType.REQUEST_ERROR);
//                                    }
//                                }
//                            });
                        }
                    }

                    uploadImages(context, rId, photoQueue, listener);

//                    if (listener != null) {
//                        listener.onSuccess();
//                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    private static void uploadImages(final Context context,
                                     final long reportId,
                                     final LongSparseArray<Photo> photoQueue,
                                     final ResponseListener listener) {
        Log.d("PassengerApiManager", "uploadImages enter");
        if (photoQueue == null || photoQueue.size() == 0) {
            List<Image> images = DataManager.getInstance().getUnuploadedImages();
            if (images.isEmpty()) {
                DataManager.getInstance().setReportImagesUploaded(reportId, true);
            }
            if (listener != null) {
                listener.onSuccess();
            }
            return;
        }
        final Long imageId = photoQueue.keyAt(0);
        final Photo p = photoQueue.get(imageId);
        if (p == null) {
            photoQueue.remove(imageId); // Will retry on next sync
            DataManager.getInstance().setReportImagesUploaded(reportId, false);
            uploadImages(context, reportId, photoQueue, listener);
            return;
        }
        Log.d("PassengerApiManager", "image " + p.getImageId());
        addPhoto(context, p.getLatitude(), p.getLongitude(), reportId, p.getFile(),
                new ResponseListener() {
                    @Override
                    public void onSuccess() {
                        Log.d("PassengerApiManager", "Image " + imageId + " successfully uploaded");
                        DataManager.getInstance().setImageUploaded(imageId, true);
                        photoQueue.remove(imageId);
                        uploadImages(context, reportId, photoQueue, listener);
                    }

                    @Override
                    public void onLoadFailure(FailureType failureType) {
                        Log.w("PassengerApiManager", "Image " + imageId + " not uploaded");
                        p.incrementTryCount();
                        if (p.getTryCount() > 10) {
                            photoQueue.remove(imageId); // Will retry on next sync
                            DataManager.getInstance().setReportImagesUploaded(reportId, false);
                        }
                        uploadImages(context, reportId, photoQueue, listener);
                    }
                });
    }

    public static void getPhotos(final Context context, final long reportId, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
    }

    public static void addPhoto(final Context context, final Double latitude, final Double longitude,
                                final Long reportId, final File file, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }

        RequestBody rbLat = RequestBody.create(MediaType.parse("text/plain"), latitude == null ? "0" : latitude.toString());
        RequestBody rbLng = RequestBody.create(MediaType.parse("text/plain"), longitude == null ? "0" : longitude.toString());
        RequestBody rbId = RequestBody.create(MediaType.parse("text/plain"), reportId.toString());
        RequestBody rbFile = RequestBody.create(MediaType.parse("image/jpg"), file);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), rbFile);

        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .addPhoto(ApiUtils.createAuthorization(accessToken),
                                rbLat, rbLng, rbId, filePart),
                o -> {
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void getContracts(final Context context, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .getContracts(ApiUtils.createAuthorization(accessToken)),
                contracts -> {
                    List<ContractMap> contractMapList = new ArrayList<>();
                    for (ContractData contractData : contracts) {
                        ContractMap contractMap = ModelMapper.createContractMap(contractData);
                        contractMapList.add(contractMap);
                    }
                    DataManager.getInstance().storeContracts(contractMapList);
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void activateContract(final Context context, final String pin, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .activateContract(ApiUtils.createAuthorization(accessToken),
                                new ActivateContractRequest(pin)),
                o -> {
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void addWorkday(final Context context, final long contractId,
                                  final IdResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .postWorkDays(ApiUtils.createAuthorization(accessToken),
                                new WorkdayStartRequest(contractId, System.currentTimeMillis())),
                workdayData -> {
                    if (listener != null) {
                        if (workdayData.getContract() != null && workdayData.getContract().getId() != null) {
                            listener.onSuccess(workdayData.getId());
                        } else {
                            listener.onLoadFailure(FailureType.REQUEST_ERROR);
                        }
                    }
                },
                throwable -> {
                    // A work day is already in progress
                    if (ApiUtils.isConflictError(throwable)) {
                        new WorkdayUtils(new ResponseListener() {
                            @Override
                            public void onSuccess() {
                                addWorkday(context, contractId, listener);
                            }

                            @Override
                            public void onLoadFailure(FailureType failureType) {
                                ApiUtils.handleErrorCallback(throwable, listener);
                            }
                        }).fetchAndCloseOpenedWorkdays(context);
                    } else {
                        ApiUtils.handleErrorCallback(throwable, listener);
                    }
                },
                listener);
    }

    public static void getOpenedWorkdays(final Context context, final ListResponseListener<WorkdayData> listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .getOpenedWorkDays(ApiUtils.createAuthorization(accessToken)),
                workdayDataList -> {
                    if (listener != null) {
                        listener.onSuccess(workdayDataList);
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void closeWorkday(final Context context, final long workdayId,
                                    final long contractId, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .closeWorkDay(ApiUtils.createAuthorization(accessToken), workdayId,
                                new WorkdayEndRequest(contractId, System.currentTimeMillis())),
                o -> {
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void addLocations(final Context context, final boolean intermediateUpload,
                                    final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            if (listener != null) {
                listener.onLoadFailure(FailureType.REQUEST_ERROR);
            }
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .addLocations(ApiUtils.createAuthorization(accessToken),
                                DataManager.getInstance().getLocations(context)),
                o -> {
                    if (intermediateUpload) {
                        DataManager.getInstance().syncLocations();
                    } else {
                        DataManager.getInstance().removeLocations();
                    }

                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }

    public static void getStores(final Context context, final long companyId, final ResponseListener listener) {
        String accessToken = ApiUtils.getActualAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        NetworkManager.requestWithAccessToken(context,
                ApiClient.getInstance()
                        .getPassengerRestService()
                        .getStores(ApiUtils.createAuthorization(accessToken), companyId),
                storeDataList -> {
                    List<StoreMap> storeMapList = new ArrayList<>();
                    for (StoreData storeData : storeDataList) {
                        StoreMap storeMap = ModelMapper.createStoreMap(storeData, TrackingUtil.getLastKnownLocation(context));
                        storeMapList.add(storeMap);
                    }
                    DataManager.getInstance().storeStores(storeMapList);
                    if (listener != null) {
                        listener.onSuccess();
                    }
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, listener),
                listener);
    }
}
