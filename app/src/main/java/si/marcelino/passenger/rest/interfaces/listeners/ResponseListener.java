package si.marcelino.passenger.rest.interfaces.listeners;

/**
 * Created by Andraz Hribar on 22. 03. 2017.
 */

public interface ResponseListener extends BaseResponseListener {
    void onSuccess();
}
