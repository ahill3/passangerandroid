package si.marcelino.passenger.rest.utils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.HttpException;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.rest.clients.ApiClient;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.BaseResponseListener;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;

import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static si.marcelino.passenger.BuildConfig.CLIENT_ID;
import static si.marcelino.passenger.BuildConfig.CLIENT_SECRET;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_GRANT_TYPE_REFRESH;
import static si.marcelino.passenger.utils.Constants.AUTH_TOKEN_;
import static si.marcelino.passenger.utils.Constants.BASIC_TOKEN_;

/**
 * Created by Andraž Hribar on 25.3.2017.
 * andraz.hribar@gmail.com
 */
public class ApiUtils {
    /**
     * @param authToken Authentication token received from Keycloak
     * @return Bearer XYZ, where XYZ is base64 encoded auth token
     */
    public static String createAuthorization(String authToken) {
        return AUTH_TOKEN_ + authToken;
    }

    /**
     * Tries to parse response error body from Json to object of the provided class.
     *
     * @param errorBody
     * @param output
     * @param <T>
     * @return Object created from Json or null in case of error
     */
    public static <T> T parseErrorBody(ResponseBody errorBody, Class<T> output) {
        if (errorBody != null) {
            try {
                return new Gson().fromJson(errorBody.string(), output);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Parses the request error and notifies the callback subscriber.
     *
     * @param throwable Throwable from the request
     * @param listener  Callback for notifications
     */
    public static void handleErrorCallback(Throwable throwable, BaseResponseListener listener) {
        if (listener != null) {
            if (throwable instanceof HttpException) {
                switch (((HttpException) throwable).code()) {
                    case HTTP_UNAUTHORIZED:
                        listener.onLoadFailure(FailureType.USER_CREDENTIALS);
                        break;
                    case HTTP_FORBIDDEN:
                        listener.onLoadFailure(FailureType.FORBIDDEN);
                        break;
                    case HTTP_NOT_FOUND:
                        listener.onLoadFailure(FailureType.NOT_FOUND);
                        break;
                    default:
                        listener.onLoadFailure(FailureType.REQUEST_ERROR);
                }
            } else {
                listener.onLoadFailure(FailureType.REQUEST_ERROR);
            }
        }
    }

    public static boolean isConflictError(Throwable throwable) {
        return throwable instanceof HttpException &&
                ((HttpException) throwable).code() == HTTP_CONFLICT;
    }

    public static void refreshAccessToken(@NonNull ResponseListener listener) {
        final DataManager dataManager = DataManager.getInstance();
        final User user = dataManager.getUser();
        if (user == null) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        ApiClient.getInstance()
                .getKeycloakRestService()
                .refresh(PARAM_GRANT_TYPE_REFRESH, CLIENT_ID, CLIENT_SECRET, generateAuthToken(user.getRefreshToken()))
                .subscribe(
                        tokenResponse -> {
                            if (tokenResponse != null && tokenResponse.getAccessToken() != null) {
                                dataManager.storeUserTokens(tokenResponse.getAccessToken(),
                                        tokenResponse.getRefreshToken());
                                listener.onSuccess();
                            } else {
                                listener.onLoadFailure(FailureType.REQUEST_ERROR);
                            }
                            dataManager.close();
                        },
                        throwable -> {
                            dataManager.close();
                            handleErrorCallback(throwable, listener);
                        });
    }

    public static boolean isHttpUnauthorizedError(Throwable throwable) {
        return throwable instanceof HttpException &&
                ((HttpException) throwable).code() == HTTP_UNAUTHORIZED;
    }

    public static String getActualAccessToken() {
        User user = DataManager.getInstance().getUser();
        return user == null ? "" : user.getAccessToken();
    }

    public static String generateBasicAuthToken(String token) {
        return BASIC_TOKEN_ + token;
    }

    private static String generateAuthToken(String token) {
        return AUTH_TOKEN_ + token;
    }
}