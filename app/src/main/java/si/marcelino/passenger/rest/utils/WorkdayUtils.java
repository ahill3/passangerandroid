package si.marcelino.passenger.rest.utils;

import android.content.Context;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ListResponseListener;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.models.data.WorkdayData;

/**
 * Created by Andraž Hribar on 10.4.2017.
 * andraz.hribar@gmail.com
 */

public class WorkdayUtils {

    private int requestCount = 0;
    private ResponseListener listener;
    private Set<Long> openedWorkdaySet;

    public WorkdayUtils(ResponseListener listener) {
        this.listener = listener;
    }

    public void fetchAndCloseOpenedWorkdays(final Context context) {
        if (openedWorkdaySet == null) {
            openedWorkdaySet = new HashSet<>();
        } else {
            openedWorkdaySet.clear();
        }
        if (++requestCount > 3) {
            if (listener != null) {
                listener.onLoadFailure(FailureType.REQUEST_ERROR);
            }
            requestCount = 0;
            return;
        }
        PassengerApiManager.getOpenedWorkdays(context, new ListResponseListener<WorkdayData>() {
            @Override
            public void onSuccess(List<WorkdayData> workdayDataList) {
                closeOpenedWorkdays(context, workdayDataList);
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                listener.onLoadFailure(failureType);
            }
        });
    }

    private void closeOpenedWorkdays(final Context context,
                                     final List<WorkdayData> workdayDataList) {
        if (openedWorkdaySet == null) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }
        if (workdayDataList.isEmpty()) {
            listener.onSuccess();
        }
        for (WorkdayData workdayData : workdayDataList) {
            if (workdayData.getContract() == null) {
                continue;
            }
            final long contractId = workdayData.getContract().getId();
            final long workdayId = workdayData.getId();
            openedWorkdaySet.add(workdayId);
            PassengerApiManager.closeWorkday(context, workdayId, contractId,
                    new ResponseListener() {
                        @Override
                        public void onSuccess() {
                            WorkdayUtils.this.notify(workdayId);
                        }

                        @Override
                        public void onLoadFailure(FailureType failureType) {
                            fetchAndCloseOpenedWorkdays(context);
                        }
                    });
        }
    }

    private void notify(long id) {
        if (openedWorkdaySet == null || !openedWorkdaySet.contains(id)) {
            return;
        }
        openedWorkdaySet.remove(id);
        if (listener != null && openedWorkdaySet.size() == 0) {
            listener.onSuccess();
        }
    }
}
