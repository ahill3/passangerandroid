package si.marcelino.passenger.rest.clients;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import si.marcelino.passenger.BuildConfig;
import si.marcelino.passenger.R;
import si.marcelino.passenger.rest.interfaces.services.KeycloakRestService;
import si.marcelino.passenger.rest.interfaces.services.PassengerRestService;
import si.marcelino.passenger.rest.utils.ApiUtils;
import si.marcelino.passenger.rest.utils.Constants;

import static si.marcelino.passenger.rest.utils.Constants.PARAM_FIELD_AUTHORIZATION;

/**
 * Singleton class that implements the HTTP client and Retrofit framework for communicating with
 * the the Keycloak and Potnik APIs.
 * <p>
 * Created by Andraž Hribar on 7.3.2017.
 * andraz.hribar@gmail.com
 */

public class ApiClient {
    private static ApiClient instance;

    private boolean overrideAccessToken = false;
    private String accessToken;
    private KeycloakRestService keycloakRestService;
    private PassengerRestService passengerRestService;

    public ApiClient(Context context) {
        final String serverBaseUrl = context.getResources().getString(R.string.server_base_url);

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

        okHttpClient.readTimeout(30, TimeUnit.SECONDS);
        okHttpClient.writeTimeout(30, TimeUnit.SECONDS);
        okHttpClient.connectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.cache(null);

        okHttpClient.addInterceptor(chain -> {
            Request originalRequest = chain.request();
            Request.Builder requestBuilder = originalRequest.newBuilder();
            requestBuilder.addHeader("Cache-Control", "no-cache");
            if (overrideAccessToken && !TextUtils.isEmpty(accessToken)) {
                requestBuilder.header(PARAM_FIELD_AUTHORIZATION, ApiUtils.createAuthorization(accessToken));
                overrideAccessToken = false;
            }
            return chain.proceed(requestBuilder.build());
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.addInterceptor(loggingInterceptor);
        }

        OkHttpClient httpClient = okHttpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverBaseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        keycloakRestService = retrofit.create(KeycloakRestService.class);
        passengerRestService = retrofit.create(PassengerRestService.class);
    }


    public static void initiate(Context context) {
        if (instance == null) {
            instance = new ApiClient(context);
        }
    }

    public static ApiClient getInstance() {
        return instance;
    }

    public KeycloakRestService getKeycloakRestService() {
        return keycloakRestService;
    }

    public PassengerRestService getPassengerRestService() {
        return passengerRestService;
    }

    public void updateAccessToken(String newAccessToken) {
        Log.d("ApiClient", "new access token " + newAccessToken);
        accessToken = newAccessToken;
        overrideAccessToken = true;
    }
}
