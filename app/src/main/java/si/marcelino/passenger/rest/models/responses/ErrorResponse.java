package si.marcelino.passenger.rest.models.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andraž Hribar on 5.3.2017.
 * andraz.hribar@gmail.com
 */

public class ErrorResponse {
    private String error;
    @SerializedName("error_description")
    private String errorDescription;

    public ErrorResponse() {
    }

    public ErrorResponse(String error, String errorDescription) {
        this.error = error;
        this.errorDescription = errorDescription;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
