package si.marcelino.passenger.rest.models.requests;

/**
 * Created by Andraž Hribar on 10.4.2017.
 * andraz.hribar@gmail.com
 */

public class WorkdayEndRequest {

    private Long contractId;
    private Long endAt;

    public WorkdayEndRequest() {
    }

    public WorkdayEndRequest(Long contractId, Long endAt) {
        this.contractId = contractId;
        this.endAt = endAt;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getEndAt() {
        return endAt;
    }

    public void setEndAt(Long endAt) {
        this.endAt = endAt;
    }
}
