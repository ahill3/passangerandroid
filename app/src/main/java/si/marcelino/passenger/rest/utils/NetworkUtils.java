package si.marcelino.passenger.rest.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Andraž Hribar on 7.3.2017.
 * andraz.hribar@gmail.com
 */

public class NetworkUtils {

    /**
     * Checks if internet connection is established.
     *
     * @param context The context to use, usually an {@link android.app.Application} or {@link android.app.Activity} object.
     * @return The current internet connectivity status.
     */
    public static boolean hasInternetConnection(Context context) {
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
