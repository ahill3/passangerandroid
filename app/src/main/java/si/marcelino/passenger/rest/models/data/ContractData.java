package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 6.4.2017.
 * andraz.hribar@gmail.com
 */

public class ContractData {

    //region Variables

    private Long id;
    private AccountData account;
    private CompanyData company;
    private Long createdAt;
    private Boolean enabled;
    private String pin;
    private Long updatedAt;

    //endregion Variables

    //region Constructors

    public ContractData() {
    }

    public ContractData(AccountData account,
                        CompanyData company,
                        Long createdAt,
                        Boolean enabled,
                        Long id,
                        String pin,
                        Long updatedAt) {
        this.account = account;
        this.company = company;
        this.createdAt = createdAt;
        this.enabled = enabled;
        this.id = id;
        this.pin = pin;
        this.updatedAt = updatedAt;
    }

    //endregion Constructors

    //region Setters&Getters

    public AccountData getAccount() {
        return account;
    }

    public void setAccount(AccountData account) {
        this.account = account;
    }

    public CompanyData getCompany() {
        return company;
    }

    public void setCompany(CompanyData company) {
        this.company = company;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    //endregion Setters&Getters
}
