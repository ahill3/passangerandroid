package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 6.4.2017.
 * andraz.hribar@gmail.com
 */

public class ReportData {
    //region Variables

    private Long id;
    private Long createdAt;
    private Long updatedAt;
    private String name;
    private String description;
    private String storeName;
    private PointData point;
    private ContractData contract;

    //endregion Variables

    //region Constructors

    public ReportData() {
    }

    public ReportData(Long id,
                      Long createdAt,
                      Long updatedAt,
                      String name,
                      String description,
                      String storeName,
                      PointData point,
                      ContractData contract) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.name = name;
        this.description = description;
        this.storeName = storeName;
        this.point = point;
        this.contract = contract;
    }
    //endregion Constructors

    //region Setters&Getters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public PointData getPoint() {
        return point;
    }

    public void setPoint(PointData point) {
        this.point = point;
    }

    public ContractData getContract() {
        return contract;
    }

    public void setContract(ContractData contract) {
        this.contract = contract;
    }

    //endregion Setters&Getters
}
