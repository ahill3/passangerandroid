package si.marcelino.passenger.rest.enums;

import si.marcelino.passenger.R;

/**
 *
 * Created by Andraž Hribar on 21. 03. 2017.
 * andraz.hribar@gmail.com
 */

public enum FailureType {
    NO_INTERNET(R.string.general_no_network),
    REQUEST_ERROR(R.string.general_request_error),
    USER_CREDENTIALS(R.string.general_user_auth_error),
    FORBIDDEN(R.string.general_forbidden_error),
    NOT_FOUND(R.string.general_not_found_error);

    private int errorMessageResId;

    FailureType(int errorMessageResId) {
        this.errorMessageResId = errorMessageResId;
    }

    public int getErrorMessageResId() {
        return errorMessageResId;
    }
}
