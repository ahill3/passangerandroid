package si.marcelino.passenger.rest.models.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andraž Hribar on 4.3.2017.
 * andraz.hribar@gmail.com
 */

public class UserInfoResponse {

    //region Variables

    @SerializedName("sub")
    private String id;
    private String name;
    @SerializedName("preferred_username")
    private String prefferedUsername;
    @SerializedName("given_name")
    private String firstName;
    @SerializedName("family_name")
    private String lastName;
    @SerializedName("email")
    private String email;

    //endregion Variables

    //region Constructors

    public UserInfoResponse() {
    }

    public UserInfoResponse(String id, String name, String prefferedUsername, String firstName, String lastName, String email) {
        this.id = id;
        this.name = name;
        this.prefferedUsername = prefferedUsername;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    //endregion Constructors

    //region Setters & Getters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefferedUsername() {
        return prefferedUsername;
    }

    public void setPrefferedUsername(String prefferedUsername) {
        this.prefferedUsername = prefferedUsername;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //endregion Setters & Getters
}
