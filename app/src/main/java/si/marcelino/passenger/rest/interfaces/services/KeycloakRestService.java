package si.marcelino.passenger.rest.interfaces.services;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;
import si.marcelino.passenger.rest.models.responses.TokenResponse;
import si.marcelino.passenger.rest.models.responses.UserInfoResponse;

import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_CLIENT_ID;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_CLIENT_SECRET;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_GRANT_TYPE;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_PASSWORD;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_REFRESH_TOKEN;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_USERNAME;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PATH_LOGOUT;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PATH_TOKEN;
import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PATH_USER_INFO;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_FIELD_AUTHORIZATION;

/**
 * Keycloak API interface, used for authentications.
 * <p>
 * Created by Andraž Hribar on 4.3.2017.
 * andraz.hribar@gmail.com
 */

public interface KeycloakRestService {
    @FormUrlEncoded
    @POST(PATH_TOKEN)
    Observable<TokenResponse> authentication(
            @Field(PARAM_FIELD_GRANT_TYPE) String grantType,
            @Field(PARAM_FIELD_CLIENT_ID) String clientId,
            @Field(PARAM_FIELD_CLIENT_SECRET) String clientSecret,
            @Field(PARAM_FIELD_USERNAME) String username,
            @Field(PARAM_FIELD_PASSWORD) String password);

    @FormUrlEncoded
    @POST(PATH_TOKEN)
    Observable<TokenResponse> refresh(
            @Field(PARAM_FIELD_GRANT_TYPE) String grantType,
            @Field(PARAM_FIELD_CLIENT_ID) String clientId,
            @Field(PARAM_FIELD_CLIENT_SECRET) String clientSecret,
            @Field(PARAM_FIELD_REFRESH_TOKEN) String refresh_token);

    @FormUrlEncoded
    @POST(PATH_LOGOUT)
    Observable<Object> logout(
            @Header(PARAM_FIELD_AUTHORIZATION) String authorization, // basic base64(client_id + client_secret)
            @Field(PARAM_FIELD_REFRESH_TOKEN) String refreshToken);  // refreshToken only
}
