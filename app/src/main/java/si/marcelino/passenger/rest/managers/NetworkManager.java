package si.marcelino.passenger.rest.managers;

import android.content.Context;
import android.util.Log;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import si.marcelino.passenger.BuildConfig;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.rest.clients.ApiClient;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.BaseResponseListener;
import si.marcelino.passenger.rest.models.data.Authentication;
import si.marcelino.passenger.rest.utils.ApiUtils;
import si.marcelino.passenger.rest.utils.NetworkUtils;

import static si.marcelino.passenger.BuildConfig.CLIENT_ID;
import static si.marcelino.passenger.BuildConfig.CLIENT_SECRET;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_GRANT_TYPE_PASSWORD;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_GRANT_TYPE_REFRESH;

/**
 * Created by Andraž Hribar on 7.3.2017.
 * andraz.hribar@gmail.com
 */

public class NetworkManager {

    private static BaseResponseListener listener;

    /**
     * Performs the request with default error handling: calls {@link FailureType#REQUEST_ERROR} on
     * the {@link BaseResponseListener listener}.
     * <p>
     * To have more control over the error response, use {@link #request(Context, Observable,
     * Action1, Action1, BaseResponseListener)}.
     * </p>
     * <p>
     * <i>Checks for internet connection before executing the call. If no connection, calls
     * {@link FailureType#NO_INTERNET} on the {@link BaseResponseListener listener}.</i>
     * </p>
     *
     * @param context    The context to use, usually an {@link android.app.Application} or
     *                   {@link android.app.Activity} object.
     * @param observable Call to execute.
     * @param onAction   Action to be called on response received.
     * @param listener   Callback for errors.
     * @return
     */
    public static <T> Subscription request(Context context,
                                           Observable<T> observable,
                                           Action1<? super T> onAction,
                                           BaseResponseListener listener) {
        return request(context, observable, onAction, onDefaultError, listener);
    }

    /**
     * Performs the request with no response error handling.
     * <p>
     * <i>Checks for internet connection before executing the call. If no connection, calls
     * {@link FailureType#NO_INTERNET} on the {@link BaseResponseListener listener}.</i>
     * </p>
     *
     * @param context    The context to use, usually an {@link android.app.Application} or
     *                   {@link android.app.Activity} object.
     * @param observable Call to execute.
     * @param onAction   Action to be called on response received.
     * @param onError    Action to be called on response error.
     * @param listener   Callback for errors.
     * @return
     */
    public static <T> Subscription request(Context context,
                                           Observable<T> observable,
                                           Action1<? super T> onAction,
                                           Action1<Throwable> onError,
                                           BaseResponseListener listener) {
        if (NetworkUtils.hasInternetConnection(context)) {
            NetworkManager.listener = listener;
            return observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(onAction, onError);
        } else {
            if (listener != null) {
                listener.onLoadFailure(FailureType.NO_INTERNET);
            }
            return null;
        }
    }

    /**
     * Performs the request that requires Oauth token to be send with it. If the token has expired,
     * it will try to refresh it and then retry the original call.
     * <p>
     * For more information see {@link #request(Context, Observable, Action1, Action1,
     * BaseResponseListener)}.
     * </p>
     *
     * @param context
     * @param observable
     * @param onAction
     * @param onError
     * @param listener
     * @return
     */
    public static <T> Subscription requestWithAccessToken(Context context,
                                                          Observable<T> observable,
                                                          Action1<? super T> onAction,
                                                          Action1<Throwable> onError,
                                                          BaseResponseListener listener) {
        if (NetworkUtils.hasInternetConnection(context)) {
            NetworkManager.listener = listener;
            return observable
                    .onErrorResumeNext(refreshTokenAndRetry(observable))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(onAction, onError);
        } else {
            if (listener != null) {
                listener.onLoadFailure(FailureType.REQUEST_ERROR);
            }
            return null;
        }
    }

    /**
     * If a request encounters 401 HTTP_UNAUTHORIZED error, it will try to refresh the access token
     * and then retry the original request.
     *
     * @param pendingCall Observable (API call) to be called after success token was retrieved.
     * @param <T>
     * @return
     */
    private static <T> Func1<Throwable, ? extends Observable<? extends T>> refreshTokenAndRetry(final Observable<T> pendingCall) {
        return (Func1<Throwable, Observable<? extends T>>) throwable -> {
            // Here check if the error thrown really is a 401
            if (ApiUtils.isHttpUnauthorizedError(throwable)) {
                Log.d("NetManager", "refresh");
                User user = DataManager.getInstance().getUser();
                if (user != null) {
                    String refreshToken = user.getRefreshToken();
                    Authentication authentication = new Authentication.Builder()
                            .setClientId(BuildConfig.CLIENT_ID)
                            .setClientSecret(BuildConfig.CLIENT_SECRET)
                            .setGrantType(PARAM_GRANT_TYPE_REFRESH)
                            .setRefreshToken(refreshToken)
                            .build();
                    // get new token and retry original call
                    return ApiClient.getInstance()
                            .getKeycloakRestService()
                            .refresh(
                                    authentication.getGrantType(),
                                    authentication.getClientId(),
                                    authentication.getClientSecret(),
                                    authentication.getRefreshToken())
                            .flatMap(tokenResponse -> {
                                if (tokenResponse != null &&
                                        tokenResponse.getAccessToken() != null &&
                                        tokenResponse.getRefreshToken() != null) {
                                    ApiClient.getInstance().updateAccessToken(tokenResponse.getAccessToken());
                                    DataManager.getInstance().storeUserTokens(tokenResponse.getAccessToken(),
                                            tokenResponse.getRefreshToken());
                                }
                                return pendingCall;
                            }).onErrorResumeNext(reLogin(pendingCall));
                }
            }
            // re-throw this error because it's not recoverable from here
            return Observable.error(throwable);
        };
    }

    private static <T> Func1<Throwable, ? extends Observable<? extends T>> reLogin(final Observable<T> pendingCall) {
        return (Func1<Throwable, Observable<? extends T>>) throwable -> {
            Log.d("NetManager", "reLogin");
            User user = DataManager.getInstance().getUser();
            if (user != null) {
                Authentication auth = new Authentication.Builder()
                        .setGrantType(PARAM_GRANT_TYPE_PASSWORD)
                        .setClientId(CLIENT_ID)
                        .setClientSecret(CLIENT_SECRET)
                        .setUsername(user.getUsername())
                        .setPassword(user.getPassword())
                        .build();
                // get new token and retry original call
                return ApiClient.getInstance()
                        .getKeycloakRestService()
                        .authentication(
                                auth.getGrantType(),
                                auth.getClientId(),
                                auth.getClientSecret(),
                                auth.getUsername(),
                                auth.getPassword())
                        .flatMap(tokenResponse -> {
                            Log.d("NetManager", "token reLogin");
                            if (tokenResponse != null &&
                                    tokenResponse.getAccessToken() != null &&
                                    tokenResponse.getRefreshToken() != null) {
                                ApiClient.getInstance().updateAccessToken(tokenResponse.getAccessToken());
                                DataManager.getInstance().storeUserTokens(tokenResponse.getAccessToken(),
                                        tokenResponse.getRefreshToken());
                            }
                            return pendingCall;
                        });
            }
            return Observable.error(throwable);
        };
    }

    /**
     * Default error handling.
     */
    private static Action1<Throwable> onDefaultError = throwable -> {
        if (listener != null) {
            listener.onLoadFailure(FailureType.REQUEST_ERROR);
        }
    };
}
