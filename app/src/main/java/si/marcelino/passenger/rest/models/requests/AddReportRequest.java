package si.marcelino.passenger.rest.models.requests;

import si.marcelino.passenger.rest.models.data.PointData;

/**
 * Created by Andraž Hribar on 17.4.2017.
 * andraz.hribar@gmail.com
 */

public class AddReportRequest {
    private Long contractId;
    private String description;
    private String name;
    private PointData point;
    private String storeName;

    public AddReportRequest(Long contractId, String description, String name, String storeName, double latitude, double longitude) {
        this.contractId = contractId;
        this.description = description;
        this.name = name;
        this.point = new PointData(latitude, longitude);
        this.storeName = storeName;
    }
}
