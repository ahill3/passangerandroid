package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 6.4.2017.
 * andraz.hribar@gmail.com
 */

public class CompanyData {

    //region Variables

    private Long id;
    private Long createdAt;
    private Long updatedAt;
    private String name;
    private String description;
    private AccountData account;

    //endregion Variables

    //region Constructors

    public CompanyData() {
    }

    public CompanyData(Long id,
                       Long createdAt,
                       Long updatedAt,
                       String name,
                       String description,
                       AccountData account) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.name = name;
        this.description = description;
        this.account = account;
    }
    //endregion Constructors

    //region Setters&Getters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccountData getAccount() {
        return account;
    }

    public void setAccount(AccountData account) {
        this.account = account;
    }

    //endregion Setters&Getters
}
