package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 15.4.2017.
 * andraz.hribar@gmail.com
 */

public class StoreData {

    //region Variables

    private Long id;
    private Long createdAt;
    private String name;
    private String phone;
    private PointData point;
    private Long updatedAt;

    //endregion Variables

    //region Constructors

    public StoreData() {
    }

    public StoreData(Long id, Long createdAt, String name, String phone, PointData point, Long updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.name = name;
        this.phone = phone;
        this.point = point;
        this.updatedAt = updatedAt;
    }

    //endregion Constructors

    //region Setters&Getters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public PointData getPoint() {
        return point;
    }

    public void setPoint(PointData point) {
        this.point = point;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    //endregion Setters&Getters
}
