package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class LocationData {
    private Long moment;
    private Long workDayId;
    private PointData point;

    public LocationData() {
    }

    public LocationData(Long moment, Long workDayId, Double latitude, Double longitude) {
        this.moment = moment;
        this.workDayId = workDayId;
        this.point = new PointData(latitude, longitude);
    }

    public Long getMoment() {
        return moment;
    }

    public void setMoment(Long moment) {
        this.moment = moment;
    }

    public Long getWorkDayId() {
        return workDayId;
    }

    public void setWorkDayId(Long workDayId) {
        this.workDayId = workDayId;
    }

    public PointData getPoint() {
        return point;
    }

    public void setPoint(PointData point) {
        this.point = point;
    }
}
