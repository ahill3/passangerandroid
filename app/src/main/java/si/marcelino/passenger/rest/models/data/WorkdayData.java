package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 10.4.2017.
 * andraz.hribar@gmail.com
 */

public class WorkdayData {

    //region Variables

    private ContractData contract;
    private Long createdAt;
    private Long endAt;
    private Long id;
    private Long startAt;
    private Long updatedAt;

    //endregion Variables

    //region Constructors

    public WorkdayData() {
    }

    public WorkdayData(ContractData contract,
                       Long createdAt,
                       Long endAt,
                       Long id,
                       Long startAt,
                       Long updatedAt) {
        this.contract = contract;
        this.createdAt = createdAt;
        this.endAt = endAt;
        this.id = id;
        this.startAt = startAt;
        this.updatedAt = updatedAt;
    }

    //endregion Constructors

    //region Setters&Getters

    public ContractData getContract() {
        return contract;
    }

    public void setContract(ContractData contract) {
        this.contract = contract;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getEndAt() {
        return endAt;
    }

    public void setEndAt(Long endAt) {
        this.endAt = endAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStartAt() {
        return startAt;
    }

    public void setStartAt(Long startAt) {
        this.startAt = startAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    //endregion Setters&Getters
}
