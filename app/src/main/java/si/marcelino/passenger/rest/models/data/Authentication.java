package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 7.3.2017.
 * andraz.hribar@gmail.com
 */

public class Authentication {
    //region Variables

    private String grantType;
    private String clientId;
    private String clientSecret;
    private String username;
    private String password;
    private String refreshToken;

    //endregion Variables

    //region Constructors

    public Authentication() {
    }

    public Authentication(String grantType,
                          String clientId,
                          String clientSecret,
                          String username,
                          String password,
                          String refreshToken) {
        this.grantType = grantType;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.username = username;
        this.password = password;
        this.refreshToken = refreshToken;
    }

    //endregion Constructors

    //region Setters & Getters

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    //endregion Setters & Getters

    //region Builder

    public static class Builder {

        //region Variables

        private String grantType;
        private String clientId;
        private String clientSecret;
        private String username;
        private String password;
        private String refreshToken;

        //endregion Variables

        //region Builder Methods

        public Builder setGrantType(String grantType) {
            this.grantType = grantType;
            return this;
        }

        public Builder setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }

        public Builder setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }

        //endregion Builder Methods

        public Authentication build() {
            return new Authentication(grantType, clientId, clientSecret, username, password, refreshToken);
        }
    }

    //endregion Builder
}
