package si.marcelino.passenger.rest.interfaces.listeners;

import java.util.List;

/**
 * Created by Andraž Hribar on 15.4.2017.
 * andraz.hribar@gmail.com
 */

public interface ListResponseListener<T> extends BaseResponseListener {
    void onSuccess(List<T> list);
}
