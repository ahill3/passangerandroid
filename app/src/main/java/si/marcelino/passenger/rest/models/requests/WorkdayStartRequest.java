package si.marcelino.passenger.rest.models.requests;

/**
 * Created by Andraž Hribar on 10.4.2017.
 * andraz.hribar@gmail.com
 */

public class WorkdayStartRequest {

    private Long contractId;
    private Long startAt;

    public WorkdayStartRequest() {
    }

    public WorkdayStartRequest(Long contractId, Long startAt) {
        this.contractId = contractId;
        this.startAt = startAt;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getStartAt() {
        return startAt;
    }

    public void setStartAt(Long startAt) {
        this.startAt = startAt;
    }
}
