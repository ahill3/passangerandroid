package si.marcelino.passenger.rest.models.responses;

/**
 * Created by Andraž Hribar on 5.3.2017.
 * andraz.hribar@gmail.com
 */

public class AccountsResponse {

    //region Variables

    private String address;
    private Long createdAt;
    private String email;
    private String firstName;
    private Long id;
    private String keycloakId;
    private String lastName;
    private String phone;
    private Integer postCode;
    private String postName;
    private Long updatedAt;
    private String username;

    //endregion Variables

    //region Constructors

    public AccountsResponse() {
    }

    public AccountsResponse(String address,
                            Long createdAt,
                            String email,
                            String firstName,
                            Long id,
                            String keycloakId,
                            String lastName,
                            String phone,
                            Integer postCode,
                            String postName,
                            Long updatedAt,
                            String username) {
        this.address = address;
        this.createdAt = createdAt;
        this.email = email;
        this.firstName = firstName;
        this.id = id;
        this.keycloakId = keycloakId;
        this.lastName = lastName;
        this.phone = phone;
        this.postCode = postCode;
        this.postName = postName;
        this.updatedAt = updatedAt;
        this.username = username;
    }

    //endregion Constructors

    //region Setters & Getters

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeycloakId() {
        return keycloakId;
    }

    public void setKeycloakId(String keycloakId) {
        this.keycloakId = keycloakId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPostCode() {
        return postCode;
    }

    public void setPostCode(Integer postCode) {
        this.postCode = postCode;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //endregion Setters & Getters
}
