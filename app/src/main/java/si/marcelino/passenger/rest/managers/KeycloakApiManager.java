package si.marcelino.passenger.rest.managers;

import android.content.Context;
import android.util.Base64;

import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.cache.models.mapping.UserMap;
import si.marcelino.passenger.rest.clients.ApiClient;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.models.data.Authentication;
import si.marcelino.passenger.rest.utils.ApiUtils;

import static si.marcelino.passenger.BuildConfig.CLIENT_ID;
import static si.marcelino.passenger.BuildConfig.CLIENT_SECRET;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_GRANT_TYPE_PASSWORD;

/**
 * Created by Andraž Hribar on 5.3.2017.
 * andraz.hribar@gmail.com
 */

public class KeycloakApiManager {
    public static void token(Context context, String username, String password,
                             final ResponseListener responseListener) {
        Authentication auth = new Authentication.Builder()
                .setGrantType(PARAM_GRANT_TYPE_PASSWORD)
                .setClientId(CLIENT_ID)
                .setClientSecret(CLIENT_SECRET)
                .setUsername(username)
                .setPassword(password)
                .build();
        NetworkManager.request(context,
                ApiClient.getInstance()
                        .getKeycloakRestService()
                        .authentication(
                                auth.getGrantType(),
                                auth.getClientId(),
                                auth.getClientSecret(),
                                auth.getUsername(),
                                auth.getPassword()),
                tokenResponse -> {
                    UserMap userMap = new UserMap.Builder()
                            .setUsername(username)
                            .setAccessToken(tokenResponse.getAccessToken())
                            .setRefreshToken(tokenResponse.getRefreshToken())
                            .setPassword(password)
                            .build();
                    DataManager.getInstance().storeUser(userMap);
                    responseListener.onSuccess();
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, responseListener),
                responseListener);
    }

    public static void logout(Context context, ResponseListener responseListener) {
        User user = DataManager.getInstance().getUser();
        if (user == null || !user.isValid()) {
            responseListener.onLoadFailure(FailureType.REQUEST_ERROR);
            return;
        }

        String tokenString = CLIENT_ID + ":" + CLIENT_SECRET;
        String token = Base64.encodeToString(tokenString.getBytes(), Base64.NO_WRAP);
        NetworkManager.request(context,
                ApiClient.getInstance()
                        .getKeycloakRestService()
                        .logout(ApiUtils.generateBasicAuthToken(token), user.getRefreshToken()),
                o -> {
                    DataManager.getInstance().clearUser();
                    responseListener.onSuccess();
                },
                throwable -> ApiUtils.handleErrorCallback(throwable, responseListener),
                responseListener);
    }
}
