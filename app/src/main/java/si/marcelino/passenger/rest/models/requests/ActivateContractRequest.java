package si.marcelino.passenger.rest.models.requests;

/**
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class ActivateContractRequest {
    private String pin;

    public ActivateContractRequest(String pin) {
        this.pin = pin;
    }
}
