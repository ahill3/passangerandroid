package si.marcelino.passenger.rest.utils;

/**
 * A class containing constants for API package only.
 * <p>
 * Created by Andraž Hribar on 4.3.2017.
 * andraz.hribar@gmail.com
 */

public class Constants {

    //region Parameter Fields

    public static final String PARAM_FIELD_AUTHORIZATION = "Authorization";
    public static final String PARAM_FIELD_LATITUDE = "latitude";
    public static final String PARAM_FIELD_LONGITUDE = "longitude";
    public static final String PARAM_FIELD_REPORT_ID = "reportId";
    public static final String PARAM_FIELD_FILE = "file";

    //endregion Parameter Fields

    //region Parameter Values

    public static final String PARAM_GRANT_TYPE_PASSWORD = "password";
    public static final String PARAM_GRANT_TYPE_REFRESH = "refresh_token";

    //endregion Parameter Values

    /**
     * A class containing constants for Potnik API only.
     */
    public static class PotnikRest {
        //region Paths
        private static final String PATH_ROOT = "/api";

        // Accounts
        public static final String PATH_ACCOUNTS = PATH_ROOT + "/accounts";
        public static final String PATH_ACCOUNTS_ME = PATH_ACCOUNTS + "/me";

        // Reports
        public static final String PATH_REPORTS = PATH_ROOT + "/reports";
        public static final String PATH_ADD_REPORT = PATH_REPORTS;

        // Photos
        public static final String PATH_PHOTOS = PATH_ROOT + "/photos";

        // Contracts
        public static final String PATH_CONTRACTS = PATH_ROOT + "/contracts";
        public static final String PATH_CONTRACT_ACTIVATE = PATH_ROOT + "/contracts/activate";

        // Work Days
        public static final String PATH_WORK_DAYS_ADD = PATH_ROOT + "/work-days";
        public static final String PATH_OPENED_WORK_DAYS = PATH_ROOT + "/work-days/opened";
        public static final String PATH_END_WORK_DAYS = PATH_ROOT + "/work-days/{id}";

        // Locations
        public static final String PATH_LOCATIONS_MULTIPLE = PATH_ROOT + "/locations/multiple";

        // Stores
        public static final String PATH_STORES_LIST = PATH_ROOT + "/companies/{id}/stores";

        //endregion Paths
    }

    /**
     * A class containing constants for Keycloak API only.
     */
    public static class KeycloakRest {

        //region Parameter Fields

        public static final String PARAM_FIELD_GRANT_TYPE = "grant_type";
        public static final String PARAM_FIELD_CLIENT_ID = "client_id";
        public static final String PARAM_FIELD_CLIENT_SECRET = "client_secret";
        public static final String PARAM_FIELD_USERNAME = "username";
        public static final String PARAM_FIELD_PASSWORD = "password";
        public static final String PARAM_FIELD_REFRESH_TOKEN = "refresh_token";
        public static final String PARAM_FIELD_ID = "id";

        //endregion Parameter Fields

        //region Parameter Values

        public static final String PARAM_VALUE_GRANT_TYPE_PASSWORD = "password";
        public static final String PARAM_VALUE_GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
        public static final String PARAM_VALUE_CLIENT_ID = "potnikapp-android";

        //endregion Parameter Values

        //region Paths

        private static final String PATH_ROOT = "/auth/realms/PotnikApp/protocol/openid-connect";
        public static final String PATH_TOKEN = PATH_ROOT + "/token";
        public static final String PATH_USER_INFO = PATH_ROOT + "/userinfo";
        public static final String PATH_LOGOUT = PATH_ROOT + "/logout";

        //endregion Paths
    }
}
