package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraž Hribar on 6.4.2017.
 * andraz.hribar@gmail.com
 */

public class PointData {

    //region Variables

    private Double latitude;
    private Double longitude;

    //endregion Variables

    //region Constructors

    public PointData() {
    }

    public PointData(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    //endregion Constructors

    //region Setters&Getters

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    //endregion Setters&Getters
}
