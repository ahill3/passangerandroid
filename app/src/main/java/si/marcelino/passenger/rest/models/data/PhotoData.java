package si.marcelino.passenger.rest.models.data;

/**
 * Created by Andraz Hribar on 22. 04. 2017.
 */

public class PhotoData {
    private Long id;
    private Long createdAt;
    private Long updatedAt;
    private PointData point;
    private String fileHandle;
    private ReportData report;

    public PhotoData() {
    }

    public PhotoData(Long id, Long createdAt, Long updatedAt, PointData point, String fileHandle, ReportData report) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.point = point;
        this.fileHandle = fileHandle;
        this.report = report;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PointData getPoint() {
        return point;
    }

    public void setPoint(PointData point) {
        this.point = point;
    }

    public String getFileHandle() {
        return fileHandle;
    }

    public void setFileHandle(String fileHandle) {
        this.fileHandle = fileHandle;
    }

    public ReportData getReport() {
        return report;
    }

    public void setReport(ReportData report) {
        this.report = report;
    }
}
