package si.marcelino.passenger.rest.interfaces.listeners;

/**
 * Created by Andraž Hribar on 10.4.2017.
 * andraz.hribar@gmail.com
 */

public interface IdResponseListener extends BaseResponseListener {
    void onSuccess(Long id);
}
