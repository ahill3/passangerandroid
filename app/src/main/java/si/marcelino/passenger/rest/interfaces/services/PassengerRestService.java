package si.marcelino.passenger.rest.interfaces.services;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;
import si.marcelino.passenger.rest.models.data.ContractData;
import si.marcelino.passenger.rest.models.data.LocationData;
import si.marcelino.passenger.rest.models.data.PhotoData;
import si.marcelino.passenger.rest.models.data.ReportData;
import si.marcelino.passenger.rest.models.data.StoreData;
import si.marcelino.passenger.rest.models.data.WorkdayData;
import si.marcelino.passenger.rest.models.requests.ActivateContractRequest;
import si.marcelino.passenger.rest.models.requests.AddAccountRequest;
import si.marcelino.passenger.rest.models.requests.AddReportRequest;
import si.marcelino.passenger.rest.models.requests.WorkdayEndRequest;
import si.marcelino.passenger.rest.models.requests.WorkdayStartRequest;
import si.marcelino.passenger.rest.models.responses.AccountsResponse;

import static si.marcelino.passenger.rest.utils.Constants.KeycloakRest.PARAM_FIELD_ID;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_FIELD_AUTHORIZATION;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_FIELD_LATITUDE;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_FIELD_LONGITUDE;
import static si.marcelino.passenger.rest.utils.Constants.PARAM_FIELD_REPORT_ID;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_ACCOUNTS;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_ACCOUNTS_ME;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_CONTRACTS;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_CONTRACT_ACTIVATE;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_END_WORK_DAYS;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_LOCATIONS_MULTIPLE;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_OPENED_WORK_DAYS;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_PHOTOS;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_REPORTS;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_STORES_LIST;
import static si.marcelino.passenger.rest.utils.Constants.PotnikRest.PATH_WORK_DAYS_ADD;


/**
 * Retrofit2 service interface, containing all backend API calls.
 * <p>
 * Created by Andraž Hribar on 4.3.2017.
 * andraz.hribar@gmail.com
 */

public interface PassengerRestService {

    //region Accounts

    @GET(PATH_ACCOUNTS_ME)
    Observable<AccountsResponse> accountsMe(@Header(PARAM_FIELD_AUTHORIZATION) String auth);

    @POST(PATH_ACCOUNTS)
    Observable<AccountsResponse> addAccount(@Header(PARAM_FIELD_AUTHORIZATION) String dummy,
                                            @Body AddAccountRequest addAccountRequest);

    //endregion Accounts

    //region Reports

    @GET(PATH_REPORTS)
    Observable<List<ReportData>> getReports(@Header(PARAM_FIELD_AUTHORIZATION) String auth);

    @POST(PATH_REPORTS)
    Observable<ReportData> addReport(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                     @Body AddReportRequest addReportRequest);

    //endregion Reports

    //region Photos

    @GET(PATH_PHOTOS)
    Observable<List<PhotoData>> getPhotos(@Header(PARAM_FIELD_AUTHORIZATION) String auth);

    @Multipart
    @POST(PATH_PHOTOS)
    Observable<Object> addPhoto(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                @Part(PARAM_FIELD_LATITUDE) RequestBody latitude,
                                @Part(PARAM_FIELD_LONGITUDE) RequestBody longitude,
                                @Part(PARAM_FIELD_REPORT_ID) RequestBody reportId,
                                @Part MultipartBody.Part file);

    //endregion Photos

    //region Contracts

    @GET(PATH_CONTRACTS)
    Observable<List<ContractData>> getContracts(@Header(PARAM_FIELD_AUTHORIZATION) String auth);

    @POST(PATH_CONTRACT_ACTIVATE)
    Observable<Object> activateContract(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                        @Body ActivateContractRequest activateContractRequest);

    //endregion Contracts

    //region Work Days

    @POST(PATH_WORK_DAYS_ADD)
    Observable<WorkdayData> postWorkDays(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                         @Body WorkdayStartRequest workdayStartRequest);

    @GET(PATH_OPENED_WORK_DAYS)
    Observable<List<WorkdayData>> getOpenedWorkDays(@Header(PARAM_FIELD_AUTHORIZATION) String auth);

    @PUT(PATH_END_WORK_DAYS)
    Observable<Object> closeWorkDay(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                    @Path(PARAM_FIELD_ID) long workdayId,
                                    @Body WorkdayEndRequest workdayEndRequest);

    //endregion Work Days

    //region Locations

    @POST(PATH_LOCATIONS_MULTIPLE)
    Observable<Object> addLocations(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                    @Body List<LocationData> inputs);

    //endregion Locations

    //region Stores

    @GET(PATH_STORES_LIST)
    Observable<List<StoreData>> getStores(@Header(PARAM_FIELD_AUTHORIZATION) String auth,
                                          @Path(PARAM_FIELD_ID) long workdayId);

    //endregion Stores
}
