package si.marcelino.passenger.cache.models.domain;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Andraž Hribar on 4.4.2017.
 * andraz.hribar@gmail.com
 */

public class Report extends RealmObject {

    @PrimaryKey
    private Integer primaryKey;
    private Long id;
    private Long contractId;
    private String description;
    private String name;
    private Double latitude;
    private Double longitude;
    private String storeName;
    private Long storeId;
    private Long time;
    private Double distance;
    private Boolean uploaded;
    private Boolean imagesUploaded;
    private RealmList<Image> images;

    public Report() {
    }

    public Report(Long id,
                  Long contractId,
                  String description,
                  String name,
                  Double latitude,
                  Double longitude,
                  String storeName,
                  Long storeId,
                  Long time,
                  Boolean uploaded) {
        this.id = id;
        this.contractId = contractId;
        this.description = description;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.storeName = storeName;
        this.storeId = storeId;
        this.time = time;
        this.uploaded = uploaded;
    }

    public Integer getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Integer primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public boolean isUploaded() {
        return uploaded == null ? false : uploaded;
    }

    public void setUploaded(Boolean uploaded) {
        this.uploaded = uploaded;
    }

    public boolean areImagesUploaded() {
        return imagesUploaded == null ? false : imagesUploaded;
    }

    public void setImagesUploaded(Boolean imagesUploaded) {
        this.imagesUploaded = imagesUploaded;
    }

    public RealmList<Image> getImages() {
        return images;
    }

    public void setImages(RealmList<Image> images) {
        this.images = images;
    }

    public void addImage(Image image) {
        if (this.images == null) {
            this.images = new RealmList<>();
        }
        this.images.add(image);
    }
}
