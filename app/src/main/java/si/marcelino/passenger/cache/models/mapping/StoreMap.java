package si.marcelino.passenger.cache.models.mapping;

/**
 * Created by Andraž Hribar on 15.4.2017.
 * andraz.hribar@gmail.com
 */

public class StoreMap {

    //region Variables

    private Long id;
    private String name;
    private String phone;
    private Double latitude;
    private Double longitude;
    private Boolean local;
    private Double distance;

    //endregion Variables

    //region Constructors

    public StoreMap(Long id, String name, String phone, Double latitude, Double longitude, Boolean local, Double distance) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.local = local;
        this.distance = distance;
    }

    //endregion Constructors

    //region Getters

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Boolean isLocal() {
        return local;
    }

    public Double getDistance() {
        return distance;
    }

    //endregion Getters

    public static class Builder {

        //region Variables

        private Long id;
        private String name;
        private String phone;
        private Double latitude;
        private Double longitude;
        private Boolean local;
        private Double distance;

        //endregion Variables

        //region Setters

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setLocal(Boolean local) {
            this.local = local;
            return this;
        }

        public Builder setDistance(Double distance) {
            this.distance = distance;
            return this;
        }

        //endregion Setters

        public StoreMap build() {
            return new StoreMap(id, name, phone, latitude, longitude, local, distance);
        }
    }
}
