package si.marcelino.passenger.cache.models.domain;

import io.realm.RealmObject;

/**
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class TrackingLocation extends RealmObject {
    private Double latitude;
    private Double longitude;
    private Long timestamp;
    private Boolean gpsLocation;
    private Boolean synced;

    public TrackingLocation() {
    }

    public TrackingLocation(Double latitude, Double longitude, Long timestamp, Boolean gpsLocation) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
        this.gpsLocation = gpsLocation;
        this.synced = false;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(Boolean gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public boolean isSynced() {
        return synced != null && synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}
