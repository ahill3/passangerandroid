package si.marcelino.passenger.cache;

import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_ID;

import android.content.Context;
import android.location.Location;
import android.os.Build;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.Sort;
import si.marcelino.passenger.cache.models.domain.Contract;
import si.marcelino.passenger.cache.models.domain.Image;
import si.marcelino.passenger.cache.models.domain.Report;
import si.marcelino.passenger.cache.models.domain.Store;
import si.marcelino.passenger.cache.models.domain.TrackingLocation;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.cache.models.mapping.ContractMap;
import si.marcelino.passenger.cache.models.mapping.ReportMap;
import si.marcelino.passenger.cache.models.mapping.StoreMap;
import si.marcelino.passenger.cache.models.mapping.TrackingLocationMap;
import si.marcelino.passenger.cache.models.mapping.UserMap;
import si.marcelino.passenger.rest.models.data.LocationData;
import si.marcelino.passenger.utils.ComplianceUtil;
import si.marcelino.passenger.utils.SettingsUtil;
import si.marcelino.passenger.utils.TrackingUtil;

/**
 * Created by Andraž Hribar on 26.3.2017.
 * andraz.hribar@gmail.com
 */

public class DataManager {

    //region Constants

    private static final String ID = "id";

    //endregion Constants

    //region Variables

    private static DataManager instance;

    private Realm realm;

    //endregion Variables

    //region Constructors

    private DataManager() {
        instantiateRealm();
    }

    //endregion Constructors

    //region Public Methods

    public static DataManager getInstance() {
        if (instance == null)
            instance = new DataManager();
        return instance;
    }

    public void close() {
        try {
            realm.close();
        } catch (IllegalStateException ignore) {

        }
        realm = null;
        instance = null;
    }

    //endregion Public Methods

    //region Private Methods

    private void instantiateRealm() {
        try {
            if (realm == null || realm.isClosed())
                realm = Realm.getDefaultInstance();
        } catch (IllegalStateException e) {
            realm = Realm.getDefaultInstance();
        }
    }

    private Realm getRealmInstance() {
        instantiateRealm();
        return realm;
    }

    private <E extends RealmModel> E detachFromRealm(E realmObject) {
        return realmObject == null ? null : getRealmInstance().copyFromRealm(realmObject);
    }

    private <E extends RealmModel> List<E> detachFromRealm(Iterable<E> realmObjects) {
        return getRealmInstance().copyFromRealm(realmObjects);
    }

    private int getNextKey(Class clazz) {
        try {
            return realm.where(clazz).max("primaryKey").intValue() + 1;
        } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
            return 0;
        }
    }

    //endregion Private Methods

    //region User Methods

    public User getLiveUser() {
        return getRealmInstance().where(User.class).findFirst();
    }

    public User getUser() {
        return detachFromRealm(getLiveUser());
    }

    public void storeUser(final UserMap userMap) {
        getRealmInstance().executeTransaction(realm -> {
            User user = getLiveUser();
            if (user == null) {
                user = realm.createObject(User.class);
            }
            ModelMapper.mapUser(user, userMap);
            storeUserTokens(user, userMap.getAccessToken(), userMap.getRefreshToken());
        });
    }

    public void storeUserTokens(String accessToken, String refreshToken) {
        getRealmInstance().executeTransaction(realm -> {
            User user = getUser();
            if (user != null) {
                storeUserTokens(user, accessToken, refreshToken);
            }
        });
    }

    private void storeUserTokens(User user, String accessToken, String refreshToken) {
        user.setAccessToken(accessToken);
        user.setRefreshToken(refreshToken);
    }

    public void clearUser() {
        getRealmInstance().executeTransaction(realm -> realm.delete(User.class));
    }

    //endregion User Methods

    //region Workday Methods

    public void fetchWorkday() {
//        getRealmInstance().where();
    }

    public void clearCache() {
        getRealmInstance().executeTransaction(realm -> {
            realm.delete(Report.class);
            realm.delete(Store.class);
        });
    }

    //endregion Workday Methods

    //region Report Methods

    public void storeReport(final ReportMap reportMap) {
        getRealmInstance().executeTransaction(realm -> realm.copyToRealmOrUpdate(createReport(reportMap, realm)));
    }

    public void storeReports(final List<ReportMap> reportMapList) {
        getRealmInstance().executeTransaction(realm -> {
            RealmList<Report> reportRealmList = new RealmList<>();
            for (ReportMap reportMap : reportMapList) {
                reportRealmList.add(createReport(reportMap, realm));
            }
            realm.copyToRealmOrUpdate(reportRealmList);
        });
    }

    public Report getReport(long id) {
        return getRealmInstance().where(Report.class).equalTo(ID, id).findFirst();
    }

    public Report getReport(long id, boolean detachFromRealm) {
        Report report = getReport(id);
        if (report != null && detachFromRealm) {
            return detachFromRealm(report);
        } else {
            return report;
        }
    }

    public Report getDetechedReport(long id) {
        return detachFromRealm(getReport(id));
    }

    public OrderedRealmCollection<Report> getReports(long contractId) {
        return getRealmInstance().where(Report.class).equalTo("contractId", contractId)
                .sort("distance", Sort.ASCENDING).findAll();
    }

    public List<Report> getLocalReports(long contractId) {
        return getRealmInstance()
                .where(Report.class)
                .equalTo("contractId", contractId)
                .equalTo("uploaded", false)
                .findAll();
    }

    public List<Report> getPartialReports(long contractId) {
        return getRealmInstance()
                .where(Report.class)
                .equalTo("contractId", contractId)
                .equalTo("imagesUploaded", false)
                .findAll();
    }

    public void removeReport(long id) {
        if (id < 0) {
            return;
        }
        getRealmInstance().executeTransaction(realm -> {
            Report report = getReport(id);
            if (report != null) {
                report.deleteFromRealm();
            }
        });
    }

    public void removeReports(boolean onlyUploaded) {
        getRealmInstance().executeTransaction(realm -> {
            RealmQuery q = realm.where(Report.class);
            if (onlyUploaded) {
                q.equalTo("uploaded", true);
            }
            q.findAll().deleteAllFromRealm();
        });
    }

    public void syncReport(long reportId, long backendId) {
        getRealmInstance().executeTransaction(realm -> {
            Report report = realm.where(Report.class).equalTo(ID, reportId).findFirst();
            if (report == null) {
                return;
            }
            report.setId(backendId);
            report.setUploaded(true);
            List<Image> images = report.getImages();
            for (Image image : images) {
                if (image == null || !image.isValid()) {
                    continue;
                }
                image.setReportId(backendId);
            }
        });
    }

    public void setReportImagesUploaded(final long reportId, final boolean isUploaded) {
        getRealmInstance().executeTransaction(realm -> {
            Report report = realm.where(Report.class).equalTo(ID, reportId).findFirst();
            if (report != null) {
                report.setImagesUploaded(isUploaded);
            }
        });
    }

    public List<Image> getUnuploadedImages() {
        return getRealmInstance()
                .where(Image.class)
                .equalTo("uploaded", false)
                .findAll();
    }

    public Image getImage(long id) {
        return getRealmInstance().where(Image.class).equalTo("primaryKey", id).findFirst();
    }

    public OrderedRealmCollection<Image> getImages(long reportId) {
        Report report = getReport(reportId);
        if (report == null) {
            return new RealmList<>();
        } else {
            return report.getImages();
        }
    }

    public void setImageUploaded(final long imageId, final boolean uploaded) {
        getRealmInstance().executeTransaction(realm -> {
            Image image = getImage(imageId);
            if (image != null) {
                image.setUploaded(uploaded);
            }
        });
    }

    public void addImage(long reportId, String filePath, String originalImagePath) {
        Image image = new Image();
        image.setPrimaryKey(new Date().getTime());
        image.setFilePath(filePath);
        image.setReportId(reportId);
        image.setUploaded(false);
        getRealmInstance().executeTransaction(realm -> {
            Report report = getRealmInstance().where(Report.class).equalTo(ID, reportId).findFirst();
            if (report != null) {
                Image realmImage = realm.copyToRealm(image);
                report.addImage(realmImage);
                String filename;
                String[] split = originalImagePath.split("/");
                if (split.length > 0) {
                    filename = split[split.length - 1];
                } else {
                    filename = "Invalid filename";
                }
                Long imageTime = ComplianceUtil.getExifDate(originalImagePath);
                if (imageTime == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        imageTime = ComplianceUtil.getCreationDate(originalImagePath);
                    } else {
                        // API 25 and less cannot access creation date
                        imageTime = ComplianceUtil.getLastModifiedDate(originalImagePath);
                    }
                }
                String tsString;
                if (imageTime != null) {
                    tsString = new SimpleDateFormat("dd. MM. yyyy HH:mm", Locale.getDefault())
                            .format(new Date(imageTime));
                } else {
                    tsString = "Invalid timestamp";
                }
                String desc = report.getDescription();
                if (desc == null || desc.equals("null")) {
                    desc = "";
                }
                report.setDescription(desc + "\n***\nimage name: " + filename + " date: " + tsString);
            }
        });
    }

    public void removeImage(String filePath) {
        getRealmInstance().executeTransaction(realm ->
                realm.where(Image.class)
                        .equalTo("filePath", filePath)
                        .findAll()
                        .deleteAllFromRealm());
    }

    public void storeLocation(final TrackingLocationMap trackingLocationMap) {
        getRealmInstance().executeTransaction(realm ->
                realm.copyToRealm(createTrackingLocation(trackingLocationMap, realm)));
    }

    public void syncLocations() {
        getRealmInstance().executeTransaction(realm -> {
            List<TrackingLocation> trackingLocations = getTrackingLocations();
            if (trackingLocations == null || trackingLocations.isEmpty() || trackingLocations.size() < 10) {
                return;
            }

            for (TrackingLocation trackingLocation : trackingLocations) {
                if (!trackingLocation.isSynced()) {
                    trackingLocation.setSynced(true);
                }
            }
        });
    }

    public void removeLocations() {
        getRealmInstance().executeTransaction(realm ->
                realm.where(TrackingLocation.class).findAll().deleteAllFromRealm());
    }

    private List<TrackingLocation> getTrackingLocations() {
        List<TrackingLocation> networkLocations = getRealmInstance().where(TrackingLocation.class).equalTo("gpsLocation", false).findAll();
        List<TrackingLocation> gpsLocations = getRealmInstance().where(TrackingLocation.class).equalTo("gpsLocation", true).findAll();
        return gpsLocations.size() > 5 ? gpsLocations : networkLocations;
    }

    public List<LocationData> getLocations(Context context) {
        List<LocationData> result = new ArrayList<>();
        Long workdayId = SettingsUtil.readLongPreference(context, PARAM_OPEN_WORKDAY_ID);
        if (workdayId == null) {
            return result;
        }

        List<TrackingLocation> trackingLocations = getTrackingLocations();

        for (TrackingLocation trackingLocation : trackingLocations) {
            if (trackingLocation == null || trackingLocation.isSynced()) {
                continue;
            }
            result.add(new LocationData(trackingLocation.getTimestamp(), workdayId,
                    trackingLocation.getLatitude(), trackingLocation.getLongitude()));
        }
        return result;
    }

    public int getLocationsCount() {
        List<TrackingLocation> trackingLocations = getTrackingLocations();
        return trackingLocations == null ? 0 : trackingLocations.size();
    }

    /**
     * Call this method only inside a realm transaction.
     *
     * @param reportMap The report data model
     * @param realm     The {@link Realm} object from the current transaction
     * @return A new Report object to save to realm
     */
    private Report createReport(final ReportMap reportMap, final Realm realm) {
        Report report = realm.where(Report.class).equalTo(ID, reportMap.getId()).findFirst();
        if (report == null) {
            report = realm.createObject(Report.class, getNextKey(Report.class));
        }
        ModelMapper.mapReport(report, reportMap);
        return report;
    }

    /**
     * Call this method only inside a realm transaction.
     *
     * @param trackingLocationMap The location data model
     * @param realm               The {@link Realm} object from the current transaction
     * @return A new Report object to save to realm
     */
    private TrackingLocation createTrackingLocation(final TrackingLocationMap trackingLocationMap, final Realm realm) {
        TrackingLocation trackingLocation = realm.createObject(TrackingLocation.class);
        ModelMapper.mapTrackingLocation(trackingLocation, trackingLocationMap);
        return trackingLocation;
    }

    //endregion Report Methods

    //region Contract Methods

    public List<Contract> getContracts() {
        return detachFromRealm(getLiveContracts());
    }

    public OrderedRealmCollection<Contract> getLiveContracts() {
        return getRealmInstance().where(Contract.class).findAll();
    }

    public Contract getContract(long id) {
        return detachFromRealm(getRealmInstance().where(Contract.class).equalTo(ID, id).findFirst());
    }

    public void storeContracts(final List<ContractMap> contractMapList) {
        getRealmInstance().executeTransaction(realm -> {
            realm.delete(Contract.class);
            RealmList<Contract> contractRealmList = new RealmList<>();
            for (ContractMap contractMap : contractMapList) {
                contractRealmList.add(createContract(contractMap, realm));
            }
            realm.copyToRealmOrUpdate(contractRealmList);
        });
    }

    /**
     * Call this method only inside a realm transaction.
     *
     * @param contractMap The {@link ContractMap} object with data
     */
    private Contract createContract(final ContractMap contractMap, final Realm realm) {
        Contract contract = realm.where(Contract.class).equalTo(ID, contractMap.getId()).findFirst();
        if (contract == null) {
            contract = realm.createObject(Contract.class, contractMap.getId());
        }
        ModelMapper.mapContract(contract, contractMap);
        return contract;
    }

    //endregion Contract Methods

    //region Store Methods

    public void updateStoreDistance(final Context context) {
        Location currentLocation = TrackingUtil.getLastKnownLocation(context);
        if (currentLocation == null) {
            return;
        }

        getRealmInstance().executeTransaction(realm -> {
            Location storeLocation = new Location("store");
            for (Store store : realm.where(Store.class).findAll()) {
                if (store.getLatitude() == null || store.getLongitude() == null) {
                    store.setDistance(null);
                    continue;
                }
                storeLocation.setLatitude(store.getLatitude());
                storeLocation.setLongitude(store.getLongitude());
                store.setDistance((double) currentLocation.distanceTo(storeLocation));
            }
        });
    }

    public OrderedRealmCollection<Store> getStores(boolean sortByDistance) {
        if (sortByDistance) {
            String[] fieldNames = new String[]{"local", "distance", "name"};
            Sort[] sortOrders = new Sort[]{Sort.DESCENDING, Sort.ASCENDING, Sort.DESCENDING};
            return getRealmInstance()
                    .where(Store.class)
                    .sort(fieldNames, sortOrders)
                    .findAll();
        } else {
            return getRealmInstance()
                    .where(Store.class)
                    .sort("local", Sort.DESCENDING, "name", Sort.ASCENDING)
                    .findAll();
        }
    }

    public Store getStore(long id) {
        return getRealmInstance().where(Store.class).equalTo(ID, id).findFirst();
    }

    public Store getStore(long id, boolean detachFromRealm) {
        Store store = getStore(id);
        if (store != null && detachFromRealm) {
            return detachFromRealm(store);
        } else {
            return store;
        }
    }

    public void addStore(final StoreMap storeMap) {
        getRealmInstance().executeTransaction(realm -> {
            Store store = createStore(storeMap, realm);
            realm.copyToRealmOrUpdate(store);
        });
    }

    public void storeStores(final List<StoreMap> storeMapList) {
        getRealmInstance().executeTransaction(realm -> {
            realm.delete(Store.class);
            RealmList<Store> storeRealmList = new RealmList<>();
            for (StoreMap storeMap : storeMapList) {
                storeRealmList.add(createStore(storeMap, realm));
            }
            realm.copyToRealmOrUpdate(storeRealmList);
        });
    }

    /**
     * Call this method only inside a realm transaction.
     *
     * @param storeMap The {@link StoreMap} object with data
     */
    private Store createStore(final StoreMap storeMap, final Realm realm) {
        Store store = realm.where(Store.class).equalTo(ID, storeMap.getId()).findFirst();
        if (store == null) {
            store = realm.createObject(Store.class, storeMap.getId());
        }
        ModelMapper.mapStore(store, storeMap);
        return store;
    }

    //endregion Store Methods
}
