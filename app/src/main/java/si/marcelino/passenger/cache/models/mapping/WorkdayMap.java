package si.marcelino.passenger.cache.models.mapping;

/**
 * Created by Andraž Hribar on 1.4.2017.
 * andraz.hribar@gmail.com
 */

public class WorkdayMap {

    //region Variables


    //endregion Variables

    //region Constructor



    //endregion Constructor

    //region Getters



    //endregion Getters

    public static class Builder {

        //region Variables



        //endregion Variables

        //region Setters



        //endregion Setters

        public WorkdayMap build() {
            return new WorkdayMap();
        }
    }
}
