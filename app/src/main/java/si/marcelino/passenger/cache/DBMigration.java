package si.marcelino.passenger.cache;

import java.util.Date;
import java.util.Random;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

import static io.realm.FieldAttribute.PRIMARY_KEY;

/**
 * Created by Andraž Hribar on 15. 04. 2018.
 * andraz.hribar@gmail.com
 */
public class DBMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            schema.get("Report")
                    .addField("imagesUploaded", Boolean.class);
            schema.get("Image")
                    .addField("primaryKey", Long.class, PRIMARY_KEY)
                    .transform(obj -> obj.set("primaryKey", new Random(new Date().getTime()).nextInt()))
                    .addField("uploaded", Boolean.class)
                    .addField("reportId", Long.class);
        }
    }
}
