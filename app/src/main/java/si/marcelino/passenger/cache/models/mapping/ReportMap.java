package si.marcelino.passenger.cache.models.mapping;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Andraž Hribar on 6.4.2017.
 * andraz.hribar@gmail.com
 */

public class ReportMap {

    //region Variables

    private Long id;
    private Long contractId;
    private String description;
    private String name;
    private Double latitude;
    private Double longitude;
    private String storeName;
    private Long storeId;
    private Long time;
    private Double distance;
    private Boolean uploaded;

    //endregion Variables

    //region Constructors

    public ReportMap(Long id,
                     Long contractId,
                     String description,
                     String name,
                     Double latitude,
                     Double longitude,
                     String storeName,
                     Long storeId,
                     Long time,
                     Double distance,
                     Boolean uploaded) {
        this.id = id;
        this.contractId = contractId;
        this.description = description;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.storeName = storeName;
        this.storeId = storeId;
        this.time = time;
        this.distance = distance;
        this.uploaded = uploaded;
    }

    //endregion Constructors

    //region Getters

    public Long getId() {
        return id;
    }

    public Long getContractId() {
        return contractId;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getStoreName() {
        return storeName;
    }

    public Long getStoreId() {
        return storeId;
    }

    public Long getTime() {
        return time;
    }

    public Double getDistance() {
        return distance;
    }

    public Boolean getUploaded() {
        return uploaded;
    }
    //endregion Getters

    public static class Builder {

        //region Variables

        private Long id;
        private Long contractId;
        private String description;
        private String name;
        private Double latitude;
        private Double longitude;
        private String storeName;
        private Long storeId;
        private Long time;
        private Double distance;
        private Boolean uploaded;

        //endregion Variables

        //region Setters

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setContractId(Long contractId) {
            this.contractId = contractId;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setLatLng(LatLng latLng) {
            this.latitude = latLng.latitude;
            this.longitude = latLng.longitude;
            return this;
        }

        public Builder setStoreName(String storeName) {
            this.storeName = storeName;
            return this;
        }

        public Builder setStoreId(Long storeId) {
            this.storeId = storeId;
            return this;
        }

        public Builder setTime(Long time) {
            this.time = time;
            return this;
        }

        public Builder setDistance(Double distance) {
            this.distance = distance;
            return this;
        }

        public Builder setUploaded(Boolean uploaded) {
            this.uploaded = uploaded;
            return this;
        }

        //endregion Setters

        public ReportMap build() {
            return new ReportMap(
                    id,
                    contractId,
                    description,
                    name,
                    latitude,
                    longitude,
                    storeName,
                    storeId,
                    time,
                    distance,
                    uploaded);
        }
    }
}
