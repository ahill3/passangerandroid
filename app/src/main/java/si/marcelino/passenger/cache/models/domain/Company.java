package si.marcelino.passenger.cache.models.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Andraž Hribar on 15.4.2017.
 * andraz.hribar@gmail.com
 */

public class Company extends RealmObject {

    //region Fields

    @PrimaryKey
    private Long id;
    private String name;
    private String description;

    //endregion Fields

    //region Setters&Getters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    //endregion Setters&Getters
}
