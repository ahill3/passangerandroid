package si.marcelino.passenger.cache.models.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Andraž Hribar on 9.4.2017.
 * andraz.hribar@gmail.com
 */

public class Contract extends RealmObject {

    //region Fields

    @PrimaryKey
    private Long id;
    private Long companyId;
    private String companyName;
    private String email;
    private String phone;

    //endregion Fields

    //region Setters&Getters

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    //endregion Setters&Getters
}
