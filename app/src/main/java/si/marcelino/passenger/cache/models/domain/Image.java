package si.marcelino.passenger.cache.models.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Andraž Hribar on 18.4.2017.
 * andraz.hribar@gmail.com
 */

public class Image extends RealmObject {

    @PrimaryKey
    private Long primaryKey;
    private String url;
    private String filePath;
    private Boolean uploaded;
    private Long reportId;

    public Image() {
    }

    public Image(String url, String filePath, Boolean uploaded) {
        this.url = url;
        this.filePath = filePath;
        this.uploaded = uploaded;
    }

    public Long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isUploaded() {
        return uploaded == null ? false : uploaded;
    }

    public Boolean getUploaded() {
        return uploaded;
    }

    public void setUploaded(Boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }
}
