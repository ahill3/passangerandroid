package si.marcelino.passenger.cache;

import android.location.Location;

import java.util.Date;

import si.marcelino.passenger.cache.models.domain.Contract;
import si.marcelino.passenger.cache.models.domain.Report;
import si.marcelino.passenger.cache.models.domain.Store;
import si.marcelino.passenger.cache.models.domain.TrackingLocation;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.cache.models.domain.Workday;
import si.marcelino.passenger.cache.models.mapping.ContractMap;
import si.marcelino.passenger.cache.models.mapping.ReportMap;
import si.marcelino.passenger.cache.models.mapping.StoreMap;
import si.marcelino.passenger.cache.models.mapping.TrackingLocationMap;
import si.marcelino.passenger.cache.models.mapping.UserMap;
import si.marcelino.passenger.cache.models.mapping.WorkdayMap;
import si.marcelino.passenger.rest.models.data.AccountData;
import si.marcelino.passenger.rest.models.data.CompanyData;
import si.marcelino.passenger.rest.models.data.ContractData;
import si.marcelino.passenger.rest.models.data.PointData;
import si.marcelino.passenger.rest.models.data.ReportData;
import si.marcelino.passenger.rest.models.data.StoreData;
import si.marcelino.passenger.rest.models.responses.AccountsResponse;

import static android.location.LocationManager.GPS_PROVIDER;

/**
 * A mapper class that handles mapping between response objects, map objects and realm objects.
 * <p>
 * Created by Andraž Hribar on 26.3.2017.
 * andraz.hribar@gmail.com
 */

public class ModelMapper {
    //region User Mapping Methods

    public static void mapUser(User user, UserMap userMap) {
        user.setId(userMap.getId());
        user.setKeycloakId(userMap.getKeycloakId());
        user.setUsername(userMap.getUsername());
        user.setFirstName(userMap.getFirstName());
        user.setLastName(userMap.getLastName());
        user.setAddress(userMap.getAddress());
        user.setEmail(userMap.getEmail());
        user.setPhone(userMap.getPhone());
        user.setPostCode(userMap.getPostCode());
        user.setPostName(userMap.getPostName());
        if (userMap.getPassword() != null) {
            user.setPassword(userMap.getPassword());
        }
        if (userMap.getCreatedAt() != null) {
            user.setCreatedAt(new Date(userMap.getCreatedAt()));
        }
        if (userMap.getUpdatedAt() != null) {
            user.setUpdatedAt(new Date(userMap.getUpdatedAt()));
        }
    }

    public static UserMap createUserMap(AccountsResponse accountsResponse,
                                        String accessToken,
                                        String refreshToken) {
        return new UserMap.Builder()
                .setAccessToken(accessToken)
                .setRefreshToken(refreshToken)
                .setId(accountsResponse.getId())
                .setKeycloakId(accountsResponse.getKeycloakId())
                .setUsername(accountsResponse.getUsername())
                .setFirstName(accountsResponse.getFirstName())
                .setLastName(accountsResponse.getLastName())
                .setAddress(accountsResponse.getAddress())
                .setEmail(accountsResponse.getEmail())
                .setPhone(accountsResponse.getPhone())
                .setPostCode(accountsResponse.getPostCode())
                .setPostName(accountsResponse.getPostName())
                .setCreatedAt(accountsResponse.getCreatedAt())
                .setUpdatedAt(accountsResponse.getUpdatedAt())
                .build();
    }

    //endregion User Mapping Methods

    //region Workday Mapping Methods

    public static void mapWorkday(Workday workday, WorkdayMap workdayMap) {

    }

    //endregion Workday Mapping Methods

    //region Report Mapping Methods

    public static void mapReport(Report report, ReportMap reportMap) {
        report.setId(reportMap.getId());
        report.setContractId(reportMap.getContractId());
        String meta = report.getDescription();
        if (meta == null || meta.equals("null")) {
            meta = "";
        }
        report.setDescription(reportMap.getDescription() + meta);
        report.setName(reportMap.getName());
        report.setLatitude(reportMap.getLatitude());
        report.setLongitude(reportMap.getLongitude());
        report.setStoreName(reportMap.getStoreName());
        report.setStoreId(reportMap.getStoreId());
        report.setTime(reportMap.getTime());
        report.setDistance(reportMap.getDistance());
        report.setUploaded(reportMap.getUploaded());
    }

    public static ReportMap createReportMap(ReportData reportData,
                                            Long contractId,
                                            boolean uploaded,
                                            Double currentLatitude,
                                            Double currentLongitude) {
        ReportMap.Builder builder = new ReportMap.Builder()
                .setId(reportData.getId())
                .setContractId(contractId)
                .setDescription(reportData.getDescription())
                .setName(reportData.getName())
                .setStoreName(reportData.getStoreName())
                .setUploaded(uploaded);
        PointData point = reportData.getPoint();
        if (point != null) {
            builder.setLatitude(point.getLatitude())
                    .setLongitude(point.getLongitude());
            if (currentLatitude != null && currentLongitude != null) {
                Location locationA = new Location("Location A");
                locationA.setLatitude(currentLatitude);
                locationA.setLongitude(currentLongitude);
                Location locationB = new Location("Location B");
                locationB.setLatitude(point.getLatitude());
                locationB.setLongitude(point.getLongitude());
                builder.setDistance((double) locationA.distanceTo(locationB));
            }
        }

        return builder.build();
    }

    public static void mapTrackingLocation(TrackingLocation trackingLocation,
                                           TrackingLocationMap trackingLocationMap) {
        trackingLocation.setLatitude(trackingLocationMap.getLatitude());
        trackingLocation.setLongitude(trackingLocationMap.getLongitude());
        trackingLocation.setTimestamp(trackingLocationMap.getTimestamp());
        trackingLocation.setGpsLocation(GPS_PROVIDER.equals(trackingLocationMap.getProvider()));
    }

    public static TrackingLocationMap createTrackingLocationMap(Location location, long now) {
        TrackingLocationMap.Builder builder = new TrackingLocationMap.Builder();
        builder.setLatitude(location.getLatitude());
        builder.setLongitude(location.getLongitude());
        builder.setTimestamp(now > 0 ? now : location.getTime());
        builder.setProvider(location.getProvider());
        return builder.build();
    }

    //endregion Report Mapping Methods

    //region Contract Methods

    public static void mapContract(Contract contract, ContractMap contractMap) {
        contract.setCompanyId(contractMap.getCompanyId());
        contract.setCompanyName(contractMap.getCompanyName());
        contract.setEmail(contractMap.getEmail());
        contract.setPhone(contractMap.getPhone());
    }

    public static ContractMap createContractMap(ContractData contractData) {
        ContractMap.Builder builder = new ContractMap.Builder();
        builder.setId(contractData.getId());
        CompanyData company = contractData.getCompany();
        if (company != null) {
            builder.setCompanyId(company.getId());
            builder.setCompanyName(company.getName());
            AccountData account = company.getAccount();
            if (account != null) {
                builder.setEmail(account.getEmail());
                builder.setPhone(account.getPhone());
            }
        }
        return builder.build();
    }

    //endregion Contract Methods

    //region Store Methods

    public static void mapStore(Store store, StoreMap storeMap) {
        store.setName(storeMap.getName());
        store.setPhone(storeMap.getPhone());
        store.setLatitude(storeMap.getLatitude());
        store.setLongitude(storeMap.getLongitude());
        store.setLocal(storeMap.isLocal());
        store.setDistance(storeMap.getDistance());
    }

    public static StoreMap createStoreMap(StoreData storeData, Location location) {
        StoreMap.Builder builder = new StoreMap.Builder();
        builder.setId(storeData.getId());
        builder.setName(storeData.getName());
        builder.setPhone(storeData.getPhone());
        builder.setLocal(false);
        PointData pointData = storeData.getPoint();
        if (pointData != null) {
            builder.setLatitude(pointData.getLatitude());
            builder.setLongitude(pointData.getLongitude());
            if (location != null) {
                Location storeLocation = new Location("store");
                storeLocation.setLongitude(pointData.getLongitude());
                storeLocation.setLatitude(pointData.getLatitude());
                builder.setDistance((double) location.distanceTo(storeLocation));
            }
        }
        return builder.build();
    }

    //endregion Store Methods
}
