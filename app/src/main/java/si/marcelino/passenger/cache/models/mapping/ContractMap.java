package si.marcelino.passenger.cache.models.mapping;

/**
 * Created by Andraž Hribar on 9.4.2017.
 * andraz.hribar@gmail.com
 */

public class ContractMap {

    //region Variables

    private Long id;
    private Long companyId;
    private String companyName;
    private String email;
    private String phone;

    //endregion Variables

    //region Constructors

    public ContractMap(Long id,
                       Long companyId,
                       String companyName,
                       String email,
                       String phone) {
        this.id = id;
        this.companyId = companyId;
        this.companyName = companyName;
        this.email = email;
        this.phone = phone;
    }

    //endregion Constructors

    //region Getters

    public Long getId() {
        return id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    //endregion Getters

    public static class Builder {

        //region Variables

        private Long id;
        private Long companyId;
        private String companyName;
        private String email;
        private String phone;

        //endregion Variables

        //region Setters

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setCompanyId(Long companyId) {
            this.companyId = companyId;
            return this;
        }

        public Builder setCompanyName(String companyName) {
            this.companyName = companyName;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        //endregion Setters

        public ContractMap build() {
            return new ContractMap(id, companyId, companyName, email, phone);
        }
    }
}
