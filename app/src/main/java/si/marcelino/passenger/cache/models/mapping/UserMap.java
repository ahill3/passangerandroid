package si.marcelino.passenger.cache.models.mapping;

/**
 * Created by Andraž Hribar on 26.3.2017.
 * andraz.hribar@gmail.com
 */

public class UserMap {

    //region Variables

    private Long id;
    private String keycloakId;
    private String username;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String phone;
    private Integer postCode;
    private String postName;
    private Long createdAt;
    private Long updatedAt;
    private String accessToken;
    private String refreshToken;
    private String password;

    //endregion Variables

    //region Constructor

    private UserMap(Long id,
                    String keycloakId,
                    String username,
                    String firstName,
                    String lastName,
                    String address,
                    String email,
                    String phone,
                    Integer postCode,
                    String postName,
                    Long createdAt,
                    Long updatedAt,
                    String accessToken,
                    String refreshToken,
                    String password) {
        this.id = id;
        this.keycloakId = keycloakId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.postCode = postCode;
        this.postName = postName;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.password = password;
    }


    //endregion Constructor

    //region Getters

    public Long getId() {
        return id;
    }

    public String getKeycloakId() {
        return keycloakId;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getPostCode() {
        return postCode;
    }

    public String getPostName() {
        return postName;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getPassword() {
        return password;
    }
    //endregion Getters

    public static class Builder {
        //region Variables

        private Long id;
        private String keycloakId;
        private String username;
        private String firstName;
        private String lastName;
        private String address;
        private String email;
        private String phone;
        private Integer postCode;
        private String postName;
        private Long createdAt;
        private Long updatedAt;
        private String accessToken;
        private String refreshToken;
        private String password;

        //endregion Variables

        //region Setters

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setKeycloakId(String keycloakId) {
            this.keycloakId = keycloakId;
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setPostCode(Integer postCode) {
            this.postCode = postCode;
            return this;
        }

        public Builder setPostName(String postName) {
            this.postName = postName;
            return this;
        }

        public Builder setCreatedAt(Long createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder setUpdatedAt(Long updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public Builder setAccessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public Builder setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }
        //endregion Setters

        public UserMap build() {
            return new UserMap(id, keycloakId, username, firstName, lastName, address, email, phone,
                    postCode, postName, createdAt, updatedAt, accessToken, refreshToken, password);
        }
    }
}
