package si.marcelino.passenger.cache.models.domain;

import java.util.Date;

/**
 * Created by Andraž Hribar on 1.4.2017.
 * andraz.hribar@gmail.com
 */

public class Workday {

    //region Fields

    private Long workdayId;
    private Date createdAt;
    private Date updatedAt;
    private Date startedAt;
    private Date endedAt;

    //region Fields

    //region Setters&Getters

    public Long getWorkdayId() {
        return workdayId;
    }

    public void setWorkdayId(Long workdayId) {
        this.workdayId = workdayId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    //endregion Setters&Getters
}
