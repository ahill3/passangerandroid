package si.marcelino.passenger.cache.models.mapping;

/**
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class TrackingLocationMap {
    private Double latitude;
    private Double longitude;
    private Long timestamp;
    private String provider;

    private TrackingLocationMap(Double latitude, Double longitude, Long timestamp, String provider) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
        this.provider = provider;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getProvider() {
        return provider;
    }

    public static class Builder {
        private Double latitude;
        private Double longitude;
        private Long timestamp;
        private String provider;

        public Builder setLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setTimestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder setProvider(String provider) {
            this.provider = provider;
            return this;
        }

        public TrackingLocationMap build() {
            return new TrackingLocationMap(latitude, longitude, timestamp, provider);
        }
    }
}
