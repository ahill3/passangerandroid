package si.marcelino.passenger.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.models.domain.Contract;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Andraž Hribar on 15.4.2017.
 * andraz.hribar@gmail.com
 */

public class ContractsAdapter extends RealmRecyclerViewAdapter<Contract, ContractsAdapter.ContractViewHolder> {

    //region Variables

    @Nullable
    private OrderedRealmCollection<Contract> data;

    //endregion Variables

    //region Constructors

    public ContractsAdapter(@Nullable OrderedRealmCollection<Contract> data) {
        super(data, false);
        this.data = data;
    }

    //endregion Constructors

    //region Implementation Methods

    @Override
    public ContractsAdapter.ContractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_contract, parent, false);
        return new ContractsAdapter.ContractViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContractsAdapter.ContractViewHolder holder, int position) {
        if (data != null && data.isValid()) {
            Contract contract = data.get(position);
            if (contract == null) {
                holder.itemView.setVisibility(GONE);
                return;
            }

            holder.nameText.setText(contract.getCompanyName());

            if (TextUtils.isEmpty(contract.getEmail())) {
                holder.emailButton.setVisibility(GONE);
            } else {
                holder.emailButton.setVisibility(VISIBLE);
                holder.emailButton.setOnClickListener(v -> startEmailIntent(v.getContext(), contract.getEmail()));
            }

            if (TextUtils.isEmpty(contract.getEmail())) {
                holder.messageButton.setVisibility(GONE);
            } else {
                holder.messageButton.setVisibility(VISIBLE);
                holder.messageButton.setOnClickListener(v -> startTextMessageIntent(v.getContext(), contract.getPhone()));
            }

            if (TextUtils.isEmpty(contract.getEmail())) {
                holder.phoneButton.setVisibility(GONE);
            } else {
                holder.phoneButton.setVisibility(VISIBLE);
                holder.phoneButton.setOnClickListener(v -> startDialIntent(v.getContext(), contract.getPhone()));
            }
        }
    }

    @Override
    public long getItemId(int index) {
        //noinspection ConstantConditions
        return index;
    }

    @Override
    public int getItemCount() {
        //noinspection ConstantConditions
        return data != null && data.isValid() ? data.size() : 0;
    }

    @Nullable
    @Override
    public Contract getItem(int index) {
        return data != null && data.isValid() ? data.get(index) : null;
    }

    @Nullable
    public OrderedRealmCollection<Contract> getData() {
        return data;
    }

    //endregion Implementation Methods

    //region Private Methods

    private void startEmailIntent(Context context, String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setType("message/rfc822");
        emailIntent.setData(Uri.fromParts("mailto", email, null));
        context.startActivity(Intent.createChooser(emailIntent, null));
    }

    private void startTextMessageIntent(Context context, String phone) {
        Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.setData(Uri.fromParts("sms", phone, null));
        context.startActivity(Intent.createChooser(smsIntent, null));
    }

    private void startDialIntent(Context context, String phone) {
        Intent emailIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        context.startActivity(Intent.createChooser(emailIntent, null));
    }

    //endregion Private Methods

    class ContractViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text)
        TextView nameText;

        @BindView(R.id.email_button)
        ImageView emailButton;

        @BindView(R.id.message_button)
        ImageView messageButton;

        @BindView(R.id.phone_button)
        ImageView phoneButton;

        ContractViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
