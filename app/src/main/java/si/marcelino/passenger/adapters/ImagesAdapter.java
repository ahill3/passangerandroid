package si.marcelino.passenger.adapters;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.Image;

/**
 * Created by Andraž Hribar on 18.4.2017.
 * andraz.hribar@gmail.com
 */

public class ImagesAdapter extends RealmRecyclerViewAdapter<Image, ImagesAdapter.ImagesViewHolder> {

    //region Variables

    @Nullable
    private OrderedRealmCollection<Image> data;
    private ImageInteractionListener imageInteractionListener;
    private boolean enableEdit = true;

    //endregion Variables

    //region Constructors

    public ImagesAdapter(@Nullable OrderedRealmCollection<Image> data,
                         ImageInteractionListener imageInteractionListener, boolean autoUpdate) {
        super(data, autoUpdate);
        this.data = data;
        this.imageInteractionListener = imageInteractionListener;
    }

    public ImagesAdapter(@Nullable OrderedRealmCollection<Image> data,
                         ImageInteractionListener imageInteractionListener,
                         boolean autoUpdate, boolean enableEdit) {
        this(data, imageInteractionListener, autoUpdate);
        this.enableEdit = enableEdit;
    }

    //endregion Constructors

    //region Implementation Methods

    @Override
    public ImagesAdapter.ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_image, parent, false);
        return new ImagesAdapter.ImagesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImagesAdapter.ImagesViewHolder holder, int position) {
        if (data != null && data.isValid()) {
            Image image = data.get(position);
            if (image == null) {
                return;
            }
            String imgPath = TextUtils.isEmpty(image.getFilePath()) ?
                    image.getUrl() : "file:" + image.getFilePath();

            Picasso.with(holder.imageView.getContext())
                    .load(imgPath)
                    .resize(100, 100)
                    .centerInside()
                    .into(holder.imageView);

            holder.imageView.setOnClickListener(v -> imageInteractionListener.onClick(v, imgPath));

            if (enableEdit) {
                holder.removeButton.setOnClickListener(v -> {
                    DataManager.getInstance().removeImage(data.get(position).getFilePath());
                    notifyDataSetChanged();
                });
            } else {
                holder.removeButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public long getItemId(int index) {
        //noinspection ConstantConditions
        return index;
    }

    @Override
    public int getItemCount() {
        //noinspection ConstantConditions
        return data != null && data.isValid() ? data.size() : 0;
    }

    @Nullable
    @Override
    public Image getItem(int index) {
        return data != null && data.isValid() ? data.get(index) : null;
    }

    @Nullable
    public OrderedRealmCollection<Image> getData() {
        return data;
    }

    //endregion Implementation Methods

    //region Nested Classes

    class ImagesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view)
        ImageView imageView;

        @BindView(R.id.remove_button)
        ImageButton removeButton;

        ImagesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ImageInteractionListener {
        void onClick(View view, String imagePath);
    }

    //endregion Nested Classes
}
