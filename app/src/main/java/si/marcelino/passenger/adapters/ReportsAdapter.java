package si.marcelino.passenger.adapters;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.models.domain.Report;


/**
 * Created by Andraž Hribar on 4.4.2017.
 * andraz.hribar@gmail.com
 */

public class ReportsAdapter extends RealmRecyclerViewAdapter<Report, ReportsAdapter.ReportViewHolder> {

    //region Variables

    @Nullable
    private OrderedRealmCollection<Report> data;
    private DateFormat dateFormat;
    private DateFormat timeFormat;
    private InteractionListener listener;

    //endregion Variables

    //region Constructors

    public ReportsAdapter(Context context, @Nullable OrderedRealmCollection<Report> data,
                          InteractionListener listener) {
        super(data, false);
        this.data = data;
        this.listener = listener;
        dateFormat = android.text.format.DateFormat.getDateFormat(context);
        timeFormat = android.text.format.DateFormat.getTimeFormat(context);
    }

    //endregion Constructors

    //region Implementation Methods

    @Override
    public ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_report, parent, false);
        return new ReportViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReportViewHolder holder, int position) {
        if (data != null && data.isValid()) {
            Report report = data.get(position);
            if (report == null) {
                return;
            }
            holder.titleText.setText(report.getName());
            holder.companyText.setText(report.getStoreName());
            if (report.getTime() != null) {
                holder.dateText.setText(dateFormat.format(new Date(report.getTime()))
                        + " " + timeFormat.format(new Date(report.getTime())));
            }
            if (report.isUploaded() && report.areImagesUploaded()) {
                holder.uploadImage.setClickable(false);
                holder.uploadImage.setImageDrawable(
                        ContextCompat.getDrawable(
                                holder.uploadImage.getContext(),
                                R.drawable.ic_cloud_done));
            } else if (listener != null) {
                holder.uploadImage.setClickable(true);
                holder.uploadImage.setImageDrawable(
                        ContextCompat.getDrawable(
                                holder.uploadImage.getContext(),
                                R.drawable.ic_upload_cloud));
                holder.uploadImage.setOnClickListener(v -> listener.onUpload(report.getId()));
            }

            holder.container.setOnClickListener(v -> listener.onClick(report.getId()));
        }
    }

    @Override
    public long getItemId(int index) {
        //noinspection ConstantConditions
        return index;
    }

    @Override
    public int getItemCount() {
        //noinspection ConstantConditions
        return data != null && data.isValid() ? data.size() : 0;
    }

    @Nullable
    @Override
    public Report getItem(int index) {
        return data != null && data.isValid() ? data.get(index) : null;
    }

    @Nullable
    public OrderedRealmCollection<Report> getData() {
        return data;
    }

    //endregion Implementation Methods

    class ReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        RelativeLayout container;

        @BindView(R.id.title_text)
        TextView titleText;

        @BindView(R.id.company_text)
        TextView companyText;

        @BindView(R.id.date_text)
        TextView dateText;

        @BindView(R.id.upload_image)
        ImageView uploadImage;

        ReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface InteractionListener {
        void onUpload(long reportId);
        void onClick(long reportId);
    }
}
