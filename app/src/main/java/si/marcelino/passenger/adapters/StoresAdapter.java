package si.marcelino.passenger.adapters;

import android.content.Intent;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.models.domain.Store;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static si.marcelino.passenger.utils.Constants.TEL_PREFIX;

/**
 * Created by Andraž Hribar on 15.4.2017.
 * andraz.hribar@gmail.com
 */

public class StoresAdapter extends RealmRecyclerViewAdapter<Store, StoresAdapter.StoreViewHolder> {

    //region Variables

    @Nullable
    private OrderedRealmCollection<Store> data;

    //endregion Variables

    //region Constructors

    public StoresAdapter(@Nullable OrderedRealmCollection<Store> data) {
        super(data, false);
        this.data = data;
    }

    //endregion Constructors

    //region Implementation Methods

    @Override
    public StoresAdapter.StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_store, parent, false);
        return new StoresAdapter.StoreViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StoresAdapter.StoreViewHolder holder, int position) {
        if (data != null && data.isValid()) {
            Store store = data.get(position);
            if (store == null) {
                return;
            }

            holder.nameText.setText(store.getName());

            if (store.getLatitude() == null || store.getLongitude() == null) {
                holder.navigationButton.setVisibility(GONE);
            } else {
                holder.navigationButton.setVisibility(VISIBLE);
                holder.navigationButton.setOnClickListener(v -> {
                    String navUri = "google.navigation:q=" +
                            store.getLatitude() + "," + store.getLongitude();
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(navUri));
                    v.getContext().startActivity(intent);
                });
            }

            if (TextUtils.isEmpty(store.getPhone())) {
                holder.phoneButton.setVisibility(GONE);
            } else {
                holder.phoneButton.setVisibility(VISIBLE);
                holder.phoneButton.setOnClickListener(v -> {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(TEL_PREFIX + store.getPhone()));
                    v.getContext().startActivity(intent);
                });
            }
        }
    }

    @Override
    public long getItemId(int index) {
        //noinspection ConstantConditions
        return index;
    }

    @Override
    public int getItemCount() {
        //noinspection ConstantConditions
        return data != null && data.isValid() ? data.size() : 0;
    }

    @Nullable
    @Override
    public Store getItem(int index) {
        return data != null && data.isValid() ? data.get(index) : null;
    }

    @Nullable
    public OrderedRealmCollection<Store> getData() {
        return data;
    }

    //endregion Implementation Methods

    //region Private Methods

    //endregion Private Methods

    class StoreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text)
        TextView nameText;

        @BindView(R.id.phone_button)
        ImageView phoneButton;

        @BindView(R.id.navigation_button)
        ImageView navigationButton;

        StoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
