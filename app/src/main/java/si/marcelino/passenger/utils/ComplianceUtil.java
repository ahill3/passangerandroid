package si.marcelino.passenger.utils;

import androidx.exifinterface.media.ExifInterface;
import android.os.Build;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ComplianceUtil {

    private static final long TIME_DIFF_THRESHOLD = 24 * 60 * 60 * 1000;
    private static final String TAG = "ComplianceUtil";
    private static final String EXIF_DATE_TIME_PATTERN = "yyyy:MM:dd HH:mm:ss";
    private static final SimpleDateFormat FORMATTER =
            new SimpleDateFormat(EXIF_DATE_TIME_PATTERN, Locale.US);

    public static boolean isImageCompliant(String filePath) {
        Boolean isExifCompliant = isExifCompliant(filePath);
        return isExifCompliant != null ? isExifCompliant : isFileDateCompliant(filePath);
    }

    private static Boolean isExifCompliant(String filePath) {
        Date imageDate;
        String timestamp;
        try {
            ExifInterface exifInterface = new ExifInterface(filePath);
            timestamp = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);
        } catch (IOException e) {
            Log.d(TAG, "EXIF data error");
            e.printStackTrace();
            return null;
        }

        if (timestamp == null) {
            Log.d(TAG, "EXIF data error");
            return null;
        }

        Log.d(TAG, "Parsing EXIF date: " + timestamp);
        try {
            imageDate = FORMATTER.parse(timestamp);
        } catch (ParseException e) {
            Log.d(TAG, "EXIF data error");
            e.printStackTrace();
            return null;
        }

        if (imageDate == null) {
            Log.d(TAG, "EXIF data is null");
            return null;
        }

        return isDateCompliant(imageDate.getTime());
    }

    public static Long getExifDate(String filePath) {
        Date imageDate;
        String timestamp;
        try {
            ExifInterface exifInterface = new ExifInterface(filePath);
            timestamp = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);
        } catch (IOException e) {
            Log.d(TAG, "EXIF data error");
            e.printStackTrace();
            return null;
        }

        if (timestamp == null) {
            Log.d(TAG, "EXIF data error");
            return null;
        }

        Log.d(TAG, "Parsing EXIF date: " + timestamp);
        try {
            imageDate = FORMATTER.parse(timestamp);
        } catch (ParseException e) {
            Log.d(TAG, "EXIF data error");
            e.printStackTrace();
            return null;
        }

        if (imageDate == null) {
            Log.d(TAG, "EXIF data is null");
            return null;
        }

        return imageDate.getTime();
    }

    private static boolean isFileDateCompliant(String filePath) {
        Long fileTime;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fileTime = getCreationDate(filePath);
        } else {
            // API 25 and less cannot access creation date
            fileTime = getLastModifiedDate(filePath);
        }

        if (fileTime == null) {
            Log.d(TAG, "Date data is null");
            return false;
        }

        return isDateCompliant(fileTime);
    }

    @Nullable
    public static Long getLastModifiedDate(String filePath) {
        File file = new File(filePath);
        try {
            return file.lastModified();
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static Long getCreationDate(String filePath) {
        BasicFileAttributes basicFileAttributes;
        try {
            basicFileAttributes = Files.readAttributes(Paths.get(filePath),
                    BasicFileAttributes.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        if (basicFileAttributes == null) return null;

        return basicFileAttributes.creationTime().toMillis();
    }

    private static boolean isDateCompliant(long time) {
        long nowTime = Calendar.getInstance().getTime().getTime();
        return nowTime - time <= TIME_DIFF_THRESHOLD;
    }
}
