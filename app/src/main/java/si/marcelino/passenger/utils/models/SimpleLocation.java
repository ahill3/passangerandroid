package si.marcelino.passenger.utils.models;

/**
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class SimpleLocation {
    private Double mLatitude;
    private Double mLongitude;

    public SimpleLocation() {
    }

    public SimpleLocation(Double latitude, Double longitude) {
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public Double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public Double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }
}
