package si.marcelino.passenger.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static si.marcelino.passenger.utils.Constants.PREFS_FILE;

/**
 * A static class that generically stores and manipulates the app's settings and states, such as
 * current user, current work day status and other preferences.
 * <p>
 * Created by Andraž Hribar on 25.3.2017.
 * andraz.hribar@gmail.com
 */

public class SettingsUtil {

    //region Public Methods

    //region Store Data

    public static void storePreference(Context context, String key, Boolean value) {
        SharedPreferences.Editor editor = editSharedPreferences(context);
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void storePreference(Context context, String key, String value) {
        SharedPreferences.Editor editor = editSharedPreferences(context);
        editor.putString(key, value);
        editor.apply();
    }

    public static void storePreference(Context context, String key, Integer value) {
        SharedPreferences.Editor editor = editSharedPreferences(context);
        editor.putInt(key, value);
        editor.apply();
    }

    public static void storePreference(Context context, String key, Long value) {
        SharedPreferences.Editor editor = editSharedPreferences(context);
        editor.putLong(key, value);
        editor.apply();
    }

    //endregion Store Data

    //region Read Data

    public static Boolean readBooleanPreference(Context context, String key) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.contains(key) ? prefs.getBoolean(key, false) : null;
    }

    public static String readStringPreference(Context context, String key) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.contains(key) ? prefs.getString(key, "") : null;
    }

    public static Integer readIntegerPreference(Context context, String key) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.contains(key) ? prefs.getInt(key, 0) : null;
    }

    public static Long readLongPreference(Context context, String key) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.contains(key) ? prefs.getLong(key, 0) : null;
    }

    public static boolean preferenceExists(Context context, String key) {
        return getSharedPreferences(context).contains(key);
    }

    //endregion Read Data

    public static void clearPreference(Context context, String key) {
        SharedPreferences.Editor editor = editSharedPreferences(context);
        editor.remove(key);
        editor.apply();
    }

    //endregion Public Methods

    //region Private Methods

    private static SharedPreferences.Editor editSharedPreferences(Context context) {
        return getSharedPreferences(context).edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFS_FILE, MODE_PRIVATE);
    }

    //endregion Private Methods
}
