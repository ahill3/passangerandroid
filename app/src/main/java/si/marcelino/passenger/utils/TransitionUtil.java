package si.marcelino.passenger.utils;

import android.app.Activity;

import si.marcelino.passenger.R;
import si.marcelino.passenger.enums.ActivityTransitionType;

/**
 * Utility class that overrides the transition between activities.
 * <p>
 * Created by Andraz Hribar on 21. 03. 2017.
 */

public class TransitionUtil {

    /**
     * Overrides transition animation when starting a new activity.
     *
     * @param activity           Context
     * @param enterAnimationType Animation type
     */
    public static void overrideEnterAnimation(Activity activity, ActivityTransitionType enterAnimationType) {
        switch (enterAnimationType) {
            case NONE:
                activity.overridePendingTransition(0, 0);
                break;
            case SLIDE_BOTTOM:
                activity.overridePendingTransition(R.anim.activity_slide_up_enter, R.anim.activity_slide_up_exit);
                break;
            case SLIDE_SIDE:
                activity.overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
                break;
            case FADE_OUT_ONLY:
                break;
            case FADE_FULL:
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case FADE_IN_ONLY:
                activity.overridePendingTransition(android.R.anim.fade_in, 0);
                break;
        }
    }

    /**
     * Overrides transition animation when finishing an existing activity.
     *
     * @param activity          Context
     * @param exitAnimationType Animation type
     */
    public static void overrideExitAnimation(Activity activity, ActivityTransitionType exitAnimationType) {
        switch (exitAnimationType) {
            case FADE_OUT_ONLY:
                activity.overridePendingTransition(0, android.R.anim.fade_out);
                break;
            case NONE:
                activity.overridePendingTransition(0, 0);
                break;
            case SLIDE_BOTTOM:
                activity.overridePendingTransition(R.anim.activity_slide_down_enter, R.anim.activity_slide_down_exit);
                break;
            case SLIDE_SIDE:
                activity.overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
                break;
            case FADE_FULL:
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case FADE_IN_ONLY:
                break;
        }
    }
}
