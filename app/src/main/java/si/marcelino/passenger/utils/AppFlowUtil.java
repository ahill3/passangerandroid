package si.marcelino.passenger.utils;

import android.content.Context;
import android.util.Log;

import si.marcelino.passenger.activities.HomeActivity;
import si.marcelino.passenger.activities.LoginActivity;
import si.marcelino.passenger.activities.WorkdayActivity;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.User;

import static si.marcelino.passenger.utils.Constants.PARAM_AUTH_EMAIL;
import static si.marcelino.passenger.utils.Constants.PARAM_AUTH_PASSWORD;
import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_ID;

/**
 * A utility class for managing the flow routing of the app.
 * <p>
 * Created by Andraž Hribar on 1.4.2017.
 * andraz.hribar@gmail.com
 */

public class AppFlowUtil {

    //region Public Methods

    public static Class resolveStartupActivity(Context context) {
        User user = DataManager.getInstance().getUser();

        // User not logged in
        if (user == null) {
            return LoginActivity.class;
        } else if (!SettingsUtil.preferenceExists(context, PARAM_OPEN_WORKDAY_ID)) {
            return WorkdayActivity.class;
        } else {
            return HomeActivity.class;
        }
//        DataManager.getInstance().fet
    }

    //region Public Methods

    //region Private Methods
    //endregion Private Methods
}
