package si.marcelino.passenger.utils.models;

import java.io.File;

/**
 * Created by Andraž Hribar on 19. 04. 2018.
 * andraz.hribar@gmail.com
 */
public class Photo {
    private Long imageId;
    private Long reportId;
    private Double latitude;
    private Double longitude;
    private File file;
    private int tryCount = 0;

    public Photo(Long imageId, Long reportId, Double latitude, Double longitude, File file) {
        this.imageId = imageId;
        this.reportId = reportId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.file = file;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getTryCount() {
        return tryCount;
    }

    public void setTryCount(int tryCount) {
        this.tryCount = tryCount;
    }

    public void incrementTryCount() {
        tryCount++;
    }
}
