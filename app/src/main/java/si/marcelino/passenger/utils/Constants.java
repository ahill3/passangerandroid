package si.marcelino.passenger.utils;

/**
 * Created by Andraz on 9.2.2017.
 */

public class Constants {
    public static final String AUTH_TOKEN_ = "Bearer ";
    public static final String BASIC_TOKEN_ = "Basic ";
    public static final String PREFS_FILE = "Marcelino.SharedPrefs";
    public static final String DATE_FORMAT = "yyyy-MM-ddThh:mm:ss";
    public static final String PARAM_OPEN_WORKDAY_ID = "Marcelino.SharedPrefs.OpenWorkdayId";
    public static final String PARAM_OPEN_WORKDAY_CONTRACT_ID = "Marcelino.SharedPrefs.OpenWorkdayContractId";
    public static final String TEL_PREFIX = "tel:";

    public static final double MAP_SLO_LAT = 46.15;
    public static final double MAP_SLO_LNG = 15.00;
    public static final int MAP_SLO_ZOOM = 7;
    public static final int MAP_ZOOM = 12;

    public static final String PARAM_REPORT_ID = "Marcelino.Report.Id";
    public static final String PARAM_STORE_ID = "Marcelino.Store.Id";
    public static final String PARAM_STORE_NAME = "Marcelino.Store.Name";
    public static final String PARAM_STORE_LAT = "Marcelino.Store.Latitude";
    public static final String PARAM_STORE_LNG = "Marcelino.Store.Longitude";
    public static final String PARAM_AUTH_EMAIL = "Marcelino.Auth.Email";
    public static final String PARAM_AUTH_PASSWORD = "Marcelino.Auth.Password";
    public static final String PARAM_URL = "Marcelino.URL";
    public static final String PARAM_TITLE = "Marcelino.Title";
    public static final String PARAM_IMAGE_QUALITY = "Marcelino.ImageQuality";
    public static final String PARAM_LAST_ALERT_SEEN = "Marcelino.LastAlertSeen";
    public static final String PARAM_GEO_ALERT_SEEN = "Marcelino.GeoAlertSeen";

    public static final String URL_PRIVACY_POLICY = "https://passengereye.com/policy.html";
    public static final String URL_HOW_TO = "https://passengereye.com/howtouse/";
    public static final String URL_PASSWORD_RESET = "https://potnikapp.com/auth/realms/PotnikApp/login-actions/reset-credentials";
}
