package si.marcelino.passenger.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import si.marcelino.passenger.enums.ImageQuality;

import static android.content.Context.ACTIVITY_SERVICE;
import static si.marcelino.passenger.utils.Constants.PARAM_IMAGE_QUALITY;

/**
 * Created by Andraž Hribar on 20.4.2017.
 * andraz.hribar@gmail.com
 */

public class ImageUtil {

    //region Constants

    private static final String TAG = "ImageUtil";
    private static final String FOLDER = "rprt_tmp";
    private static final String FILE_ = "rprt_";
    private static final String _EXT = ".jpg";

    //endregion Constants

    //region Public Methods

    public static String saveTempImage(Context context, Bitmap bitmap) {
        // String folderPath = Environment.getExternalStorageDirectory().toString() + FOLDER;
        ContextWrapper cw = new ContextWrapper(context);
        File folder = cw.getDir(FOLDER, Context.MODE_PRIVATE);
        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                Log.e(TAG, "Failed to create folder " + FOLDER);
                return "";
            }
            Log.i(TAG, "Created folder " + FOLDER);
        }

        String imageQualityPref = SettingsUtil.readStringPreference(context, PARAM_IMAGE_QUALITY);
        ImageQuality imageQuality = TextUtils.isEmpty(imageQualityPref) ?
                ImageQuality.AVERAGE : ImageQuality.valueOf(imageQualityPref);

        OutputStream outStream = null;
        File file = new File(folder, FILE_ + System.currentTimeMillis() + _EXT);
        try {
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, imageQuality.getQuality(), outStream);
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file.getAbsolutePath();
    }

    public static boolean clearTempImages(Context context) {
        // String folderPath = Environment.getExternalStorageDirectory() + FOLDER;
        ContextWrapper cw = new ContextWrapper(context);
        File folder = cw.getDir(FOLDER, Context.MODE_PRIVATE);
        return !folder.exists() || folder.delete();
    }

    public static ImageQuality getImageQuality(Context context) {
        try {
            return ImageQuality.valueOf(
                    SettingsUtil.readStringPreference(context, PARAM_IMAGE_QUALITY));
        } catch (IllegalArgumentException | NullPointerException e) {
            return evaluateImageQuality(context);
        }
    }

    public static void setImageQuality(Context context, ImageQuality imageQuality) {
        SettingsUtil.storePreference(context, PARAM_IMAGE_QUALITY, imageQuality.name());
    }

    //endregion Public Methods

    //region Private Methods

    private static long getFreeMemorySize(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        if (activityManager != null) {
            activityManager.getMemoryInfo(memoryInfo);
        }
        return memoryInfo.availMem;
    }

    private static long getTotalMemorySize(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        if (activityManager != null) {
            activityManager.getMemoryInfo(memoryInfo);
        }
        return memoryInfo.totalMem;
    }

    private static ImageQuality evaluateImageQuality(Context context) {
        long totalMemory = getTotalMemorySize(context);
        if (totalMemory <= ImageQuality.POOR.getRequiredMemory()) {
            SettingsUtil.storePreference(context, PARAM_IMAGE_QUALITY, ImageQuality.POOR.name());
            return ImageQuality.POOR;
        } else if (totalMemory <= ImageQuality.AVERAGE.getRequiredMemory()) {
            SettingsUtil.storePreference(context, PARAM_IMAGE_QUALITY, ImageQuality.AVERAGE.name());
            return ImageQuality.AVERAGE;
        } else if (totalMemory <= ImageQuality.BEST.getRequiredMemory()) {
            SettingsUtil.storePreference(context, PARAM_IMAGE_QUALITY, ImageQuality.BEST.name());
            return ImageQuality.BEST;
        }

        // Fallback, in case something goes wrong
        Log.wtf(TAG, String.format(Locale.getDefault(),
                "Falling back to average quality, due to a memory read of %d", totalMemory));
        return ImageQuality.AVERAGE;
    }

    //endregion Private Methods
}
