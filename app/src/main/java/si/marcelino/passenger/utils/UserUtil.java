package si.marcelino.passenger.utils;

import android.text.TextUtils;
import android.widget.TextView;

import si.marcelino.passenger.cache.models.domain.User;

/**
 * Created by Andraž Hribar on 21.5.2017.
 * andraz.hribar@gmail.com
 */

public class UserUtil {
    public static String extractFullName(User user) {
        if (user == null || TextUtils.isEmpty(user.getFirstName()) && TextUtils.isEmpty(user.getLastName())) {
            return "";
        }
        return user.getFirstName() + " " + user.getLastName();
    }

    public static String extractKey(User user) {
        if (user == null || TextUtils.isEmpty(user.getKeycloakId())) {
            return "";
        }
        return user.getKeycloakId().substring(0, 8);
    }
}
