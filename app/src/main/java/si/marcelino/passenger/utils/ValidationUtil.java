package si.marcelino.passenger.utils;

import android.content.Context;
import androidx.annotation.StringRes;
import android.text.TextUtils;

import java.util.Locale;
import java.util.regex.Pattern;

import si.marcelino.passenger.R;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * A validation tool with validation methods.
 * <p>
 * Created by Andraž Hribar on 29. 03. 2017.
 * andraz.hribar@gmail.com
 */

public class ValidationUtil {

    //region Constants

    // Resources
    @StringRes
    private static final int EMPTY_ERROR = R.string.generic_error_empty;
    @StringRes
    private static final int LENGTH_ERROR = R.string.generic_error_length;
    @StringRes
    private static final int FORMAT_ERROR = R.string.generic_error_format;
    @StringRes
    private static final int COMPARE_ERROR = R.string.message_password_mismatch;
    @StringRes
    private static final int FIELD_EMAIL = R.string.label_email;

    // Email
    private static final String EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX, CASE_INSENSITIVE);

    //endregion Constants

    //region Public Methods

    public static Result validateEmail(Context context, String email) {
        if (TextUtils.isEmpty(email)) {
            return new Result(false, formatErrorMessage(context, EMPTY_ERROR, FIELD_EMAIL));
        } else if (!EMAIL_PATTERN.matcher(email).matches()) {
            return new Result(false, formatErrorMessage(context, FORMAT_ERROR, FIELD_EMAIL));
        }
        return new Result(true, "");
    }

    public static Result validateRequired(Context context, String value, @StringRes int field) {
        if (TextUtils.isEmpty(value)) {
            return new Result(false, formatErrorMessage(context, EMPTY_ERROR, field));
        }
        return new Result(true, "");
    }

    public static Result validateLength(Context context, String value, int length, @StringRes int field) {
        if (TextUtils.isEmpty(value)) {
            return new Result(false, formatErrorMessage(context, EMPTY_ERROR, field));
        } else if (value.length() < length) {
            return new Result(false, formatErrorMessage(context, length, LENGTH_ERROR, field));
        }
        return new Result(true, "");
    }

    public static Result validateRetype(Context context, String value, String compare, @StringRes int field) {
        if (TextUtils.isEmpty(value)) {
            return new Result(false, formatErrorMessage(context, EMPTY_ERROR, field));
        } else if (!value.equals(compare)) {
            return new Result(false, context.getString(COMPARE_ERROR));
        }
        return new Result(true, "");
    }

    public static Result validateRequiredNumber(Context context, String value, @StringRes int field) {
        if (TextUtils.isEmpty(value)) {
            return new Result(false, formatErrorMessage(context, EMPTY_ERROR, field));
        } else {
            try {
                int number = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                return new Result(false, formatErrorMessage(context, FORMAT_ERROR, field));
            }
        }
        return new Result(true, "");
    }

    //endregion Public Methods

    //region Private Methods

    private static String formatErrorMessage(Context context,
                                             @StringRes int fieldStringResource,
                                             @StringRes int messageStringResource) {
        String field = context.getString(fieldStringResource);
        String message = context.getString(messageStringResource);
        try {
            return String.format(Locale.getDefault(), field, message);
        } catch (Exception e) {
            return "";
        }
    }

    private static String formatErrorMessage(Context context,
                                             int length,
                                             @StringRes int fieldStringResource,
                                             @StringRes int messageStringResource) {
        String field = context.getString(fieldStringResource);
        String message = context.getString(messageStringResource);
        try {
            return String.format(Locale.getDefault(), field, message, length);
        } catch (Exception e) {
            return "";
        }
    }

    //endregion Private Methods

    //region Nested Classes

    public static class Result {
        private boolean valid;
        private String message;

        public Result(boolean valid, String message) {
            this.valid = valid;
            this.message = message;
        }

        public boolean isValid() {
            return valid;
        }

        public String getMessage() {
            return message;
        }
    }

    //endregion Nested Classes
}