package si.marcelino.passenger.utils.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * A cluster item that represents a store marker on a Google Map.
 * <p>
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class StoreClusterItem implements ClusterItem {

    //region Variables

    private final LatLng position;
    private final String title;
    private final Long storeId;

    //endregion Variables

    //region Constructors

    public StoreClusterItem(double latitude, double longitude, String title, Long storeId) {
        this.position = new LatLng(latitude, longitude);
        this.title = title;
        this.storeId = storeId;
    }

    //endregion Constructors

    //region Implementation Methods

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    //endregion Implementation Methods

    //region Getters

    public Long getStoreId() {
        return storeId;
    }

    //endregion Getters
}
