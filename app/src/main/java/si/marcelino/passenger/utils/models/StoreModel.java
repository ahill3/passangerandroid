package si.marcelino.passenger.utils.models;

/**
 * Created by Andraž Hribar on 7.5.2017.
 * andraz.hribar@gmail.com
 */

public class StoreModel {
    private Long id;
    private String name;
    private double latitude;
    private double longitude;

    public StoreModel(Long id, String name, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
