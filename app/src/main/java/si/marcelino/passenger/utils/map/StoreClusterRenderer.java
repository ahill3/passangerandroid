package si.marcelino.passenger.utils.map;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import si.marcelino.passenger.R;

/**
 * A cluster renderer extension used for rendering custom map marker icons.
 * <p>
 * Created by Andraž Hribar on 21.4.2017.
 * andraz.hribar@gmail.com
 */

public class StoreClusterRenderer extends DefaultClusterRenderer<StoreClusterItem> {

    //region Constructor

    public StoreClusterRenderer(Context context, GoogleMap map, ClusterManager<StoreClusterItem> clusterManager) {
        super(context, map, clusterManager);
    }

    //endregion Constructor

    //region Overridden Methods


    @Override
    protected void onBeforeClusterItemRendered(StoreClusterItem item,
                                               MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place));
    }


    @Override
    protected void onClusterItemRendered(StoreClusterItem clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);
    }

    //endregion Overridden Methods
}
