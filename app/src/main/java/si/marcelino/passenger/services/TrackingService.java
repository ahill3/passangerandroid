package si.marcelino.passenger.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.ModelMapper;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.utils.NetworkUtils;

/**
 * A service for logging the device's location.
 * <p>
 * Created by Andraž Hribar on 21. 04. 2017.
 */

public class TrackingService extends Service {

    //region Constants

    private static final String TAG = "TrackingService";
    private static final String COMMON_PROVIDER = "CommonProvider";
    private static final long LOCATION_INTERVAL = 30000L;
    private static final float LOCATION_DISTANCE = 0f;
    private static final int GPS_LOCATION = 0;
    private static final int NETWORK_LOCATION = 1;

    //endregion Constants

    //region Variables

    //    private boolean mGpsTrackingAvailable = true;
//    private boolean mNetworkTrackingAvailable = true;
    private Location mLatestLocation;
    private LocationManager mLocationManager = null;

    private LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    //endregion Variables

    //region Lifecycle Methods

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[NETWORK_LOCATION]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
//            mNetworkTrackingAvailable = false;
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
//            mNetworkTrackingAvailable = false;
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[GPS_LOCATION]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
//            mGpsTrackingAvailable = false;
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
//            mGpsTrackingAvailable = false;
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (LocationListener mLocationListener : mLocationListeners) {
                try {
                    mLocationManager.removeUpdates(mLocationListener);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listeners, ignore", ex);
                }
            }
        }
    }

    //endregion Lifecycle Methods

    //region Private Methods

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    //endregion Private Methods

    //region Nested Classes

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;
//        List<SimpleLocation> mLocationList = new ArrayList<>();

        LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
            if (mLatestLocation == null) {
                mLatestLocation = new Location(COMMON_PROVIDER);
            }
        }

        @Override
        public void onLocationChanged(Location location) {
            if (location.getAccuracy() < 50.0) {
                mLastLocation.set(location);
                mLatestLocation.set(location);
                DataManager.getInstance().storeLocation(
                        ModelMapper.createTrackingLocationMap(location,
                                System.currentTimeMillis()));
                if (NetworkUtils.hasInternetConnection(getBaseContext())) {
                    PassengerApiManager.addLocations(getBaseContext(), true, null);
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    //endregion Nested Classes
}