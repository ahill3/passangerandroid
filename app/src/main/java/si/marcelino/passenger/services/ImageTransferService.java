package si.marcelino.passenger.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import android.util.Log;

import java.io.File;
import java.util.List;

import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.Image;
import si.marcelino.passenger.cache.models.domain.Report;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.utils.NetworkUtils;
import si.marcelino.passenger.utils.models.Photo;

/**
 * A service for continuous retrying to push images to server.
 * <p>
 * Created by Andraž Hribar on 15. 04. 2018.
 * andraz.hribar@gmail.com
 */
public class ImageTransferService extends Service {

    //region Variables

    private static final long UPDATE_INTERVAL = 300000L; // 5 minute update
    private static final String TAG = "ImageTransferService";
    private static final int MAX_TRIES = 5;

    private LongSparseArray<Photo> photoQueue = new LongSparseArray<>();

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            syncImages();
            mHandler.postDelayed(mRunnable, UPDATE_INTERVAL);
        }
    };

    //endregion Variables

    //region Overridden Methods

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler.post(mRunnable);
        Log.i(TAG, TAG + " started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mRunnable);
        mRunnable = null;
        mHandler = null;
        Log.i(TAG, TAG + " stopped");
    }

    //endregion Overridden Methods

    //region Private Methods

    private void syncImages() {
        if (!NetworkUtils.hasInternetConnection(this)) {
            Log.d(TAG, "no network");
            return;
        }
        List<Image> images = DataManager.getInstance().getUnuploadedImages();
        if (images.isEmpty() && photoQueue.size() == 0) {
            Log.d(TAG, "no images");
            return;
        }
        for (final Image image : images) {
            Log.d(TAG, "checking image " + image.getFilePath());
            final Report report = DataManager.getInstance().getReport(image.getReportId());
            if (report == null) {
                continue;
            }
            Photo photo = new Photo(image.getPrimaryKey(), image.getReportId(),
                    report.getLatitude(), report.getLongitude(), new File(image.getFilePath()));
            photoQueue.put(image.getPrimaryKey(), photo);
        }

        uploadImages();
    }

    private void uploadImages() {
        if (photoQueue.size() == 0 || !NetworkUtils.hasInternetConnection(this)) {
            return;
        }
        final Long imageId = photoQueue.keyAt(0);
        final Photo p = photoQueue.get(imageId);
        PassengerApiManager.addPhoto(this, p.getLatitude(), p.getLongitude(),
                p.getReportId(), p.getFile(), new ResponseListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Image " + imageId + " successfully uploaded");
                        DataManager.getInstance().setImageUploaded(imageId, true);
                        photoQueue.remove(imageId);
                        uploadImages();
                    }

                    @Override
                    public void onLoadFailure(FailureType failureType) {
                        Log.w(TAG, "Image " + imageId + " not uploaded");
                        p.incrementTryCount();
                        if (p.getTryCount() > MAX_TRIES) {
                            photoQueue.remove(imageId); // Will retry on next sync
                        } else {
                            uploadImages();
                        }
                    }
                });
    }

    //endregion Private Methods
}
