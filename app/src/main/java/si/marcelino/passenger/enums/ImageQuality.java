package si.marcelino.passenger.enums;

/**
 * Enumerated values representing the quality of the images taken for the report.
 * Made to prevent running out of memory.
 * <p>
 * Created by Andraž Hribar on 9.7.2017.
 * andraz.hribar@gmail.com
 */

public enum ImageQuality {
    POOR(1000, 50, 1500000000L),
    AVERAGE(1500, 66, 2500000000L),
    BEST(3000, 80, Long.MAX_VALUE);

    private int maxSize;
    private int quality;
    private long requiredMemory;

    ImageQuality(int maxSize, int quality, long requiredMemory) {
        this.maxSize = maxSize;
        this.quality = quality;
        this.requiredMemory = requiredMemory;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public int getQuality() {
        return quality;
    }

    public long getRequiredMemory() {
        return requiredMemory;
    }
}
