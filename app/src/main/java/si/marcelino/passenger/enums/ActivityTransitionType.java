package si.marcelino.passenger.enums;

/**
 * Types of transition between activities.
 * <p>
 * Created by Andraz Hribar on 21. 03. 2017.
 */

public enum ActivityTransitionType {
    SLIDE_SIDE,
    SLIDE_BOTTOM,
    FADE_OUT_ONLY,
    FADE_FULL,
    NONE,
    FADE_IN_ONLY
}
