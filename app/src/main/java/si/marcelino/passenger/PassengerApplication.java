package si.marcelino.passenger;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;

//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.core.CrashlyticsCore;

//import io.fabric.sdk.android.Fabric;
import androidx.multidex.MultiDexApplication;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import si.marcelino.passenger.rest.clients.ApiClient;

/**
 * Created by Andraž Hribar on 9. 02. 2017.
 * andraz.hribar@gmail.com
 */

public class PassengerApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder()
//                        .disabled(BuildConfig.DEBUG)
//                        .build())
//                .build());

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(2)
                .allowWritesOnUiThread(true)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        ApiClient.initiate(this);
    }

    public boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
