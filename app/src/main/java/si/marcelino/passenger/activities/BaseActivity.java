package si.marcelino.passenger.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import si.marcelino.passenger.R;
import si.marcelino.passenger.enums.ActivityTransitionType;
import si.marcelino.passenger.rest.utils.NetworkUtils;
import si.marcelino.passenger.utils.TransitionUtil;

import static si.marcelino.passenger.enums.ActivityTransitionType.SLIDE_BOTTOM;

/**
 * An abstract activity class which holds basic common configuration and methods for most of the
 * used activities.
 * <p>
 * Created by Andraž Hribar on 9.2.2017.
 * andraz.hribar@gmail.com
 */

public abstract class BaseActivity extends AppCompatActivity {

    //region Variables

    protected String mTag = "BaseActivity";
    protected boolean mShowToolbar = true;
    protected boolean mActivityLive = false;
    protected Unbinder mUnbinder;

    private ProgressDialog mProgressDialog;

    //endregion Variables

    //region Lifecycle Methods

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityLive = true;
        setupTag();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mActivityLive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mActivityLive = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivityLive = false;
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    //endregion Lifecycle Methods

    //region Overridden Methods

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isFinishing()) {
            TransitionUtil.overrideExitAnimation(this, SLIDE_BOTTOM);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(SLIDE_BOTTOM);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //endregion Overridden Methods

    //region Protected Methods

    /**
     * Initializes basic common activity functionalities.
     *
     * @param target      Activity context.
     * @param layoutResID Resource ID to be inflated.
     */
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        setContentView(layoutResID);
        mUnbinder = ButterKnife.bind(target);

        // Setup action bar
        if (mShowToolbar) {
            if (NavUtils.getParentActivityIntent(this) != null) {
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null)
                    actionBar.setDisplayHomeAsUpEnabled(true);
            }
        } else {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null)
                actionBar.hide();
        }
    }

    /**
     * Same as {@link AppCompatActivity#startActivity(Intent)} but with a custom transition.
     *
     * @param intent             {@link Intent} object with extras and flags for a new activity.
     * @param enterAnimationType {@link ActivityTransitionType} enum that defines the transition to
     *                           the next activity
     */
    protected void startActivity(Intent intent, ActivityTransitionType enterAnimationType) {
        startActivity(intent);
        TransitionUtil.overrideEnterAnimation(this, enterAnimationType);
    }

    /**
     * Same as {@link AppCompatActivity#startActivityForResult(Intent, int)} but with a custom transition.
     *
     * @param intent             {@link Intent} object with extras and flags for a new activity.
     * @param requestCode        If >= 0, this code will be returned in onActivityResult() when the activity exits.
     * @param enterAnimationType {@link ActivityTransitionType} enum that defines the transition to
     *                           the next activity
     */
    protected void startActivityForResult(Intent intent, int requestCode, ActivityTransitionType enterAnimationType) {
        startActivityForResult(intent, requestCode);
        TransitionUtil.overrideEnterAnimation(this, enterAnimationType);
    }

    /**
     * Same as {@link AppCompatActivity#finish()} but applies animation.
     *
     * @param exitAnimationType {@link ActivityTransitionType} enum that defines the transition to
     *                          the previous activity
     */
    protected void finish(ActivityTransitionType exitAnimationType) {
        finish();
        TransitionUtil.overrideExitAnimation(this, exitAnimationType);
    }

    /**
     * Sets the visibility of a target {@link View}.
     *
     * @param target     The targeted {@link View}
     * @param visibility One of {@link View#VISIBLE} or {@link View#GONE}.
     */
    protected void fadeVisibility(final View target, final int visibility) {
        boolean show;
        if (visibility == View.VISIBLE)
            show = true;
        else if (visibility == View.GONE)
            show = false;
        else return;

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        target.setVisibility(visibility);
        target.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        target.setVisibility(visibility);
                    }
                });
    }

    /**
     * Creates a new intent and clears the back stack if selected.
     *
     * @param activityClass The next activity
     * @param clearStack    Clears the activity back stack if set to {@code false}
     * @return A new intent with a selected activity.
     */
    protected Intent createIntent(Class activityClass, boolean clearStack) {
        Intent intent = new Intent(this, activityClass);
        if (clearStack)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    /**
     * Same as {@link BaseActivity#createIntent(Class, boolean)}.
     *
     * @param activityClass The next activity
     * @return A new intent with a selected activity.
     */
    protected Intent createIntent(Class activityClass) {
        return createIntent(activityClass, false);
    }

    protected void showLoadingDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void showLoadingDialog(@StringRes int stringResource) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage(getString(stringResource));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void showLoadingDialog() {
        showLoadingDialog(R.string.general_loading);
    }

    protected void hideLoadingDialog() {
        if (mProgressDialog == null)
            return;
        mProgressDialog.dismiss();
    }

    protected boolean loadFromCache() {
        return !NetworkUtils.hasInternetConnection(this);
    }

    protected void setToolbarTitle(@StringRes int titleStringRes) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(titleStringRes);
        }
    }

    protected void setToolbarTitle(String titleString) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(titleString);
        }
    }

    // Logging
    protected void log(String message) {
        Log.d(mTag, message);
    }

    protected void setTag(String tag) {
        mTag = tag.replace("Activity", "");
    }

    //endregion Protected Methods

    //region Event Methods
    //endregion Event Methods

    //region Abstract Methods

    abstract void setupTag();

    //endregion Abstract Methods
}
