package si.marcelino.passenger.activities;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.TextView;

import butterknife.BindView;
import si.marcelino.passenger.R;
import si.marcelino.passenger.adapters.ContractsAdapter;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.enums.ImageQuality;
import si.marcelino.passenger.utils.ImageUtil;
import si.marcelino.passenger.utils.UserUtil;

/**
 * Created by Andraž Hribar on 31.5.2017.
 * andraz.hribar@gmail.com
 */

public class UserProfileActivity extends BaseActivity {

    //region UI Elements

    @BindView(R.id.user_name_text)
    TextView mUserNameText;
    @BindView(R.id.phone_text)
    TextView mPhoneText;
    @BindView(R.id.email_text)
    TextView mEmailText;
    @BindView(R.id.address_text)
    TextView mAddressText;
    @BindView(R.id.user_key_text)
    TextView mUserKeyText;
    @BindView(R.id.contractors_recycler)
    RecyclerView mContractorsRecycler;

    //endregion UI Elements

    //region Variables
    //endregion Variables

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_user_profile);
        loadUserProfile();
        loadUserPreferences();
    }

    //endregion Lifecycle Methods

    //region Overridden Methods

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);

        // Setup RecyclerView
        mContractorsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mContractorsRecycler.setHasFixedSize(true);
        mContractorsRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }


    //endregion Overridden Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods

    //region Private Methods

    private void loadUserProfile() {
        User user = DataManager.getInstance().getUser();
        if (user != null) {
            String username = UserUtil.extractFullName(user);
            mUserNameText.setText(TextUtils.isEmpty(username) ? getString(R.string.user_profile_me) : username);
            mPhoneText.setText(user.getPhone());
            mEmailText.setText(user.getEmail());
            mAddressText.setText(user.getAddress());
            mUserKeyText.setText(UserUtil.extractKey(user));
        }

        ContractsAdapter adapter = new ContractsAdapter(DataManager.getInstance().getLiveContracts());
        mContractorsRecycler.setAdapter(adapter);
    }

    private void loadUserPreferences() {
        ImageQuality imageQuality = ImageUtil.getImageQuality(this);
        // TODO: Set the slider to the setting.
    }

    //endregion Private Methods
}
