package si.marcelino.passenger.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.BindView;
import si.marcelino.passenger.R;
import si.marcelino.passenger.adapters.ReportsAdapter;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.enums.ActivityTransitionType;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.utils.SettingsUtil;

import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_CONTRACT_ID;
import static si.marcelino.passenger.utils.Constants.PARAM_REPORT_ID;

/**
 * The inventory list activity, containing all inventories made by the user.
 * <p>
 * Created by Andraž Hribar on 21.3.2017.
 * andraz.hribar@gmail.com
 */

public class ReportsActivity extends BaseActivity implements ReportsAdapter.InteractionListener {

    //region UI Element

    @BindView(R.id.reports_recycler)
    RecyclerView reportsRecycler;

    //endregion UI Element

    //region Variables

    private ReportsAdapter mAdapter;

    //endregion Variables

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_reports);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadReports();
    }

    //endregion Lifecycle Methods

    //region Overridden Methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_reports, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add_report) {
            openReportForm();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);

        // Setup RecyclerView
        reportsRecycler.setLayoutManager(new LinearLayoutManager(this));
        reportsRecycler.setHasFixedSize(true);
        reportsRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

//        TouchHelperCallback touchHelperCallback = new TouchHelperCallback();
//        ItemTouchHelper touchHelper = new ItemTouchHelper(touchHelperCallback);
//        touchHelper.attachToRecyclerView(reportsRecycler);
    }

    //endregion Overridden Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    // ReportsAdapter.InteractionListener

    @Override
    public void onUpload(long reportId) {
        showLoadingDialog();
        PassengerApiManager.addReport(this, reportId, new ResponseListener() {
            @Override
            public void onSuccess() {
                hideLoadingDialog();
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(ReportsActivity.this, R.string.general_error_title, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(long reportId) {
        Intent intent = new Intent(this, ReportDetailActivity.class);
        intent.putExtra(PARAM_REPORT_ID, reportId);
        startActivity(intent, ActivityTransitionType.SLIDE_BOTTOM);
    }

    //endregion Implementation Methods

    //region Private Methods

    private void loadReports() {
        showLoadingDialog(R.string.general_loading);
        Long contractId = SettingsUtil.readLongPreference(this, PARAM_OPEN_WORKDAY_CONTRACT_ID);
        if (contractId == null) {
            Toast.makeText(this, R.string.message_report_load_error, Toast.LENGTH_SHORT).show();
            return;
        }

        mAdapter = new ReportsAdapter(getApplicationContext(),
                DataManager.getInstance().getReports(contractId), this);
        reportsRecycler.setAdapter(mAdapter);
        hideLoadingDialog();
    }

    private void openReportForm() {
        Intent intent = new Intent(this, AddReportActivity.class);
        startActivity(intent, ActivityTransitionType.SLIDE_BOTTOM);
    }

    //endregion Private Methods
}
