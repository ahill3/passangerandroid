package si.marcelino.passenger.activities;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import butterknife.BindView;
import si.marcelino.passenger.R;

import static android.view.View.GONE;
import static si.marcelino.passenger.utils.Constants.PARAM_TITLE;
import static si.marcelino.passenger.utils.Constants.PARAM_URL;

/**
 * Created by Andraž Hribar on 4.5.2017.
 * andraz.hribar@gmail.com
 */

public class WebViewActivity extends BaseActivity {

    //region Variables

    private String mUrl;

    //endregion Variables

    //region UI Elements

    @BindView(R.id.web_view)
    WebView mWebView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    //endregion UI Elements

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_web_view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(mUrl)) {
            finish();
            return;
        }

        mWebView.loadUrl(mUrl);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(GONE);
            }
        });
    }

    //endregion Lifecycle Methods

    //region Overridden Methods

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);
        mUrl = getIntent().getStringExtra(PARAM_URL);
        int title = getIntent().getIntExtra(PARAM_TITLE, 0);
        if (title == 0) {
            String titleText = getIntent().getStringExtra(PARAM_TITLE);
            if (TextUtils.isEmpty(titleText)) {
                setTitle(R.string.app_title);
            } else {
                setTitle(titleText);
            }
        } else {
            setTitle(title);
        }
    }


    //endregion Overridden Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods
}
