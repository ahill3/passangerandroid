package si.marcelino.passenger.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import si.marcelino.passenger.R;
import si.marcelino.passenger.adapters.ImagesAdapter;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.Report;
import si.marcelino.passenger.cache.models.domain.Store;
import si.marcelino.passenger.fragments.NestedMapFragment;
import si.marcelino.passenger.views.DetailRow;

import static si.marcelino.passenger.utils.Constants.MAP_ZOOM;
import static si.marcelino.passenger.utils.Constants.PARAM_REPORT_ID;

/**
 * Created by Andraž Hribar on 21. 04. 2017.
 * andraz.hribar@gmail.com
 */

public class ReportDetailActivity extends BaseActivity implements
        OnMapReadyCallback,
        ImagesAdapter.ImageInteractionListener {

    //region Variables

    private Report mReport;
    private Store mStore;

    private GoogleMap mMap;
    private ImagesAdapter mImagesAdapter;

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    //endregion Variables

    //region UI Elements

    @BindView(R.id.container)
    RelativeLayout container;

    @BindView(R.id.main_scroll)
    ScrollView mMainScroll;

    @BindView(R.id.expanded_image)
    ImageView mExpandedImageView;

    @BindView(R.id.name_detail)
    DetailRow mNameDetail;

    @BindView(R.id.description_detail)
    DetailRow mDescriptionDetail;

    @BindView(R.id.location_detail)
    DetailRow mLocationDetail;

    @BindView(R.id.images_recycler)
    RecyclerView mImagesRecycler;

    //endregion UI Elements

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_report_details);
    }

    //endregion Lifecycle Methods

    //region Private Methods

    private void zoomImageFromThumb(final View thumbView, final String imagePath) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        Picasso.with(this)
                .load(imagePath)
                .into(mExpandedImageView);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        container.getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        mExpandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        mExpandedImageView.setPivotX(0f);
        mExpandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(mExpandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(mExpandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(mExpandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(mExpandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        mExpandedImageView.setOnClickListener(view -> {
            if (mCurrentAnimator != null) {
                mCurrentAnimator.cancel();
            }

            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            AnimatorSet set1 = new AnimatorSet();
            set1.play(ObjectAnimator
                    .ofFloat(mExpandedImageView, View.X, startBounds.left))
                    .with(ObjectAnimator
                            .ofFloat(mExpandedImageView,
                                    View.Y, startBounds.top))
                    .with(ObjectAnimator
                            .ofFloat(mExpandedImageView,
                                    View.SCALE_X, startScaleFinal))
                    .with(ObjectAnimator
                            .ofFloat(mExpandedImageView,
                                    View.SCALE_Y, startScaleFinal));
            set1.setDuration(mShortAnimationDuration);
            set1.setInterpolator(new DecelerateInterpolator());
            set1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    thumbView.setAlpha(1f);
                    mExpandedImageView.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    thumbView.setAlpha(1f);
                    mExpandedImageView.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }
            });
            set1.start();
            mCurrentAnimator = set1;
        });
    }

    private void prepareGallery() {
        mImagesAdapter = new ImagesAdapter(
                DataManager.getInstance().getImages(mReport.getId()), this, true, false);
        mImagesRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImagesRecycler.setHasFixedSize(true);
        mImagesRecycler.setAdapter(mImagesAdapter);
    }

    //endregion Private Methods

    //region Overridden Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        long reportId = getIntent().getLongExtra(PARAM_REPORT_ID, -1);
        if (reportId < 0) {
            finish();
            return;
        }

        DataManager dataManager = DataManager.getInstance();
        mReport = dataManager.getReport(reportId, true);
        if (mReport == null) {
            finish();
            return;
        }
        Long storeId = mReport.getStoreId();
        if (storeId == null) {
            finish();
            return;
        }
        mStore = dataManager.getStore(storeId, true);
        if (mStore == null) {
            finish();
            return;
        }

        mNameDetail.setValueText(mReport.getName());

        String description = mReport.getDescription();
        String[] split = description.split("\n[*]{3}\n");
        if (split.length > 0) {
            description = split[0];
        }

        mDescriptionDetail.setValueText(description);
        mLocationDetail.setValueText(mReport.getStoreName());

        prepareGallery();

        NestedMapFragment mapFragment = (NestedMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.setListener(() -> mMainScroll.requestDisallowInterceptTouchEvent(true));
        mapFragment.getMapAsync(this);
    }

    //endregion Overridden Methods

    //region Implementation Methods

    // OnMapReadyCallback

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(mStore.getLatitude(), mStore.getLongitude());
        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM));
    }

    // ImagesAdapter.ImageInteractionListener

    @Override
    public void onClick(View view, String imagePath) {
        zoomImageFromThumb(view, imagePath);
    }

    //endregion Implementation Methods
}
