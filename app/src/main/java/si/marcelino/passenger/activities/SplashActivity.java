package si.marcelino.passenger.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.LayoutRes;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import si.marcelino.passenger.R;
import si.marcelino.passenger.utils.AppFlowUtil;

/**
 * The activity and view implementation for the splash component.
 * <p>
 * Created by Andraž Hribar on 09. 02. 2017.
 * andraz.hribar@gmail.com
 */

public class SplashActivity extends BaseActivity {

    //region Constants

    //endregion Constants


    //region Variables

    //endregion Variables


    //region UI Elements

    //    @BindView(R.id.logo_layout)
//    LinearLayout mLogoLayout;
    @BindView(R.id.copyright_text)
    TextView mCopyrightText;

    //endregion UI Elements

    public SplashActivity() {
        mShowToolbar = false;
    }

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(() -> startActivity(createIntent(AppFlowUtil.resolveStartupActivity(this), true)), 2000L);
    }

    //endregion Lifecycle Methods


    //region Overridden Methods

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);
        mCopyrightText.setText(formatCopyright());
    }

    //endregion Overridden Methods

    //region Public Methods

    //endregion Public Methods


    //region Private Methods

    private String formatCopyright() {
        String line = getString(R.string.splash_copyright);
        return String.format(Locale.getDefault(), line, Calendar.getInstance().get(Calendar.YEAR));
    }


    //endregion Private Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods
}
