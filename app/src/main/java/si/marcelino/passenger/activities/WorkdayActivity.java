package si.marcelino.passenger.activities;

import static si.marcelino.passenger.enums.ActivityTransitionType.FADE_FULL;
import static si.marcelino.passenger.enums.ActivityTransitionType.SLIDE_BOTTOM;
import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_CONTRACT_ID;
import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_ID;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.Contract;
import si.marcelino.passenger.cache.models.domain.User;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.IdResponseListener;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.KeycloakApiManager;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.utils.NetworkUtils;
import si.marcelino.passenger.services.ImageTransferService;
import si.marcelino.passenger.services.TrackingService;
import si.marcelino.passenger.utils.SettingsUtil;
import si.marcelino.passenger.utils.UserUtil;

/**
 * Created by Andraž Hribar on 27.3.2017.
 * andraz.hribar@gmail.com
 */

public class WorkdayActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    //region Constants

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    //endregion Constants

    //region Variables

    private long mSelectedContractId = 0L;
    private List<ContractModel> mContractModelList;
    private boolean mGpsDialogShown = false;

    //endregion Variables

    //region UI Elements

    @BindView(R.id.start_workday_button)
    Button mStartWorkdayButton;

    @BindView(R.id.select_contract_spinner)
    AppCompatSpinner mSelectCompanySpinner;

    @BindView(R.id.user_key_text)
    TextView mUserKeyText;

    //endregion UI Elements

    //region Lifecycle Methods

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_workday);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLocationPermission();
    }

    //endregion Lifecycle Methods

    //region Overridden Methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_workday, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);
        User user = DataManager.getInstance().getUser();
        if (user == null) {
            DataManager.getInstance().clearUser();
            startActivity(createIntent(LoginActivity.class, true), FADE_FULL);
            return;
        }
        mUserKeyText.setText(UserUtil.extractKey(user));
        populateContractDropdown();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mStartWorkdayButton.setEnabled(true);
            } else {
                mStartWorkdayButton.setEnabled(false);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //endregion Overridden Methods

    //region Event Methods

    @OnClick(R.id.start_workday_button)
    void onStartWorkdayClick() {
        if (!checkLocationPermission()) {
            return;
        }
        if (mSelectedContractId < 1) {
            return;
        }
        showLoadingDialog(R.string.general_loading);
        addWorkday();
    }

    @OnClick(R.id.add_contract_button)
    void onAddContractClick() {
        new MaterialDialog.Builder(this)
                .title(R.string.add_contract_title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(getString(R.string.add_contract_insert_pin_title), null, (dialog, input) -> {
                    if (!TextUtils.isEmpty(input)) {
                        confirmPin(input.toString());
                    }
                })
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .show();
    }

    //endregion Event Methods

    //region Private Methods

    private void populateContractDropdown() {
        mContractModelList = new ArrayList<>();
        List<Contract> contractList = DataManager.getInstance().getContracts();
        String[] contracts = new String[contractList.size() + 1];
        contracts[0] = getString(R.string.add_report_dropdown_placeholder);
        for (int i = 0; i < contractList.size(); i++) {
            Contract contract = contractList.get(i);
            mContractModelList.add(new ContractModel(contract.getId(), contract.getCompanyName()));
            contracts[i + 1] = contract.getCompanyName();
        }
        final ArrayAdapter<String> contractAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, contracts);
        mSelectCompanySpinner.setAdapter(contractAdapter);
        mSelectCompanySpinner.setOnItemSelectedListener(this);
    }

    private void logout() {
        if (NetworkUtils.hasInternetConnection(this)) {
            KeycloakApiManager.logout(this, new ResponseListener() {
                @Override
                public void onSuccess() {
                    startActivity(createIntent(LoginActivity.class, true), FADE_FULL);
                }

                @Override
                public void onLoadFailure(FailureType failureType) {
                    hideLoadingDialog();
                    Toast.makeText(WorkdayActivity.this, failureType.getErrorMessageResId(),
                            Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            DataManager.getInstance().clearUser();
            startActivity(createIntent(LoginActivity.class, true), FADE_FULL);
        }
    }

    private void confirmPin(String pin) {
        showLoadingDialog(R.string.general_loading);
        PassengerApiManager.activateContract(this, pin, new ResponseListener() {
            @Override
            public void onSuccess() {
                loadContracts();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(WorkdayActivity.this, R.string.message_pin_confirmation_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadContracts() {
        PassengerApiManager.getContracts(this, new ResponseListener() {
            @Override
            public void onSuccess() {
                populateContractDropdown();
                hideLoadingDialog();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(WorkdayActivity.this, R.string.message_contracts_load_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addWorkday() {
        PassengerApiManager.addWorkday(this, mSelectedContractId, new IdResponseListener() {
            @Override
            public void onSuccess(Long id) {
                SettingsUtil.storePreference(WorkdayActivity.this, PARAM_OPEN_WORKDAY_ID, id);
                fetchStores();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(WorkdayActivity.this, R.string.message_workday_start_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchStores() {
        Contract contract = DataManager.getInstance().getContract(mSelectedContractId);
        if (contract == null || contract.getCompanyId() == null) {
            hideLoadingDialog();
            Toast.makeText(WorkdayActivity.this, R.string.message_contract_valid_error, Toast.LENGTH_SHORT).show();
            return;
        }

        PassengerApiManager.getStores(this, contract.getCompanyId(), new ResponseListener() {
            @Override
            public void onSuccess() {
                SettingsUtil.storePreference(WorkdayActivity.this, PARAM_OPEN_WORKDAY_CONTRACT_ID, mSelectedContractId);
                startService(new Intent(WorkdayActivity.this, TrackingService.class));
                startService(new Intent(WorkdayActivity.this, ImageTransferService.class));
                //fetchReports();
                hideLoadingDialog();
                startActivity(createIntent(HomeActivity.class, true), SLIDE_BOTTOM);
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(WorkdayActivity.this, R.string.message_location_load_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchReports() {
        PassengerApiManager.getReports(this, new ResponseListener() {
            @Override
            public void onSuccess() {
                hideLoadingDialog();
                startActivity(createIntent(HomeActivity.class, true), SLIDE_BOTTOM);
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(WorkdayActivity.this, R.string.message_report_load_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkLocationPermission() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Toast.makeText(this, R.string.message_no_gps_enabled, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && !mGpsDialogShown) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.workday_gps_dialog_title)
                        .setMessage(R.string.workday_gps_dialog_message)
                        .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                            checkLocationPermission();
                        })
                        .setNegativeButton(android.R.string.no, (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        })
                        .create()
                        .show();
                mGpsDialogShown = true;
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    //endregion Private Methods

    //region Implementation Methods

    // BaseActivity

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    // AdapterView.OnItemSelectedListener

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (mContractModelList != null && position <= mContractModelList.size() && position > 0) {
            mStartWorkdayButton.setEnabled(true);
            mSelectedContractId = mContractModelList.get(position - 1).id;
        } else {
            mStartWorkdayButton.setEnabled(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Do nothing
    }

    //endregion Implementation Methods

    //region Nested Classes

    private static class ContractModel {
        long id;
        String name;

        ContractModel(long id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    //endregion Nested Classes
}
