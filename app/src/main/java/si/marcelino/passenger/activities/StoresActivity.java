package si.marcelino.passenger.activities;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import si.marcelino.passenger.R;
import si.marcelino.passenger.adapters.StoresAdapter;
import si.marcelino.passenger.cache.DataManager;

/**
 * The location list activity, containing all the locations the user has added.
 * <p>
 * Created by Andraž Hribar on 20.3.2017.
 * andraz.hribar@gmail.com
 */

public class StoresActivity extends BaseActivity {

    //region UI elements

    @BindView(R.id.stores_recycler)
    RecyclerView storesRecycler;

    //endregion UI elements

    //region Lifecycle Activities

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_stores);
        loadStores();
    }

    //endregion Lifecycle Activities

    //region Overridden Methods

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);

        // Setup RecyclerView
        storesRecycler.setLayoutManager(new LinearLayoutManager(this));
        storesRecycler.setHasFixedSize(true);
        storesRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    //endregion Overridden Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods

    //region Private Methods

    private void loadStores() {
//        showLoadingDialog(R.string.general_loading);
        StoresAdapter adapter = new StoresAdapter(DataManager.getInstance().getStores(false));
        storesRecycler.setAdapter(adapter);
//        hideLoadingDialog();
    }

    //endregion Private Methods
}
