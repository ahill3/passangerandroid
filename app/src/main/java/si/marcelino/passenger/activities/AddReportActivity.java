package si.marcelino.passenger.activities;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static si.marcelino.passenger.utils.Constants.MAP_SLO_ZOOM;
import static si.marcelino.passenger.utils.Constants.MAP_ZOOM;
import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_CONTRACT_ID;
import static si.marcelino.passenger.utils.Constants.PARAM_STORE_ID;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.util.LongSparseArray;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputLayout;
import com.google.maps.android.clustering.ClusterManager;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import si.marcelino.passenger.R;
import si.marcelino.passenger.adapters.ImagesAdapter;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.Store;
import si.marcelino.passenger.cache.models.mapping.ReportMap;
import si.marcelino.passenger.enums.ActivityTransitionType;
import si.marcelino.passenger.fragments.NestedMapFragment;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.utils.NetworkUtils;
import si.marcelino.passenger.utils.ComplianceUtil;
import si.marcelino.passenger.utils.ImageUtil;
import si.marcelino.passenger.utils.SettingsUtil;
import si.marcelino.passenger.utils.TrackingUtil;
import si.marcelino.passenger.utils.ValidationUtil;
import si.marcelino.passenger.utils.map.StoreClusterItem;
import si.marcelino.passenger.utils.map.StoreClusterRenderer;
import si.marcelino.passenger.utils.models.StoreModel;

/**
 * Created by Andraž Hribar on 8.4.2017.
 * andraz.hribar@gmail.com
 */

public class AddReportActivity extends BaseActivity implements
        OnMapReadyCallback,
        AdapterView.OnItemSelectedListener,
        IPickResult,
        ImagesAdapter.ImageInteractionListener {

    //region Constants

    private static final int PARAM_STORE_REQUEST_CODE = 100;
    private static final int IMAGE_MAX_SIZE = 3000;

    //endregion Constants

    //region Variables

    private long mReportId;
    private long mContractId;
    private long mAdHocId;
    private int mSelectedPosition = 0;
    private StoreModel mSelectedStoreModel;

    private List<Long> mSortedIdList;
    private LongSparseArray<StoreModel> mStoreModelSparseArray = new LongSparseArray<>();
    private ImagesAdapter mImagesAdapter;
    private GoogleMap mMap;
    private ClusterManager<StoreClusterItem> mClusterManager;

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    private PickSetup setup;

    //endregion Variables

    //region UI Element

    @BindView(R.id.container)
    RelativeLayout container;

    @BindView(R.id.expanded_image)
    ImageView mExpandedImageView;

    @BindView(R.id.main_scroll)
    ScrollView mMainScroll;

    @BindView(R.id.report_title_layout)
    TextInputLayout mTitleLayout;

    @BindView(R.id.report_title_input)
    AppCompatEditText mTitleInput;

    @BindView(R.id.select_contract_spinner)
    AppCompatSpinner mSelectedContractSpinner;

    @BindView(R.id.report_description_input)
    AppCompatEditText mDescriptionInput;

    @BindView(R.id.images_recycler)
    RecyclerView mImagesRecycler;

    //endregion UI Element

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_add_report);
        if (!loadVariables()) {
            Toast.makeText(this, R.string.message_contract_load_error, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        addTemplateReport();
        mImagesAdapter = new ImagesAdapter(
                DataManager.getInstance().getImages(mReportId), this, true);

        setup = new PickSetup()
                .setMaxSize(IMAGE_MAX_SIZE)
                .setCameraButtonText(getString(R.string.image_alert_action_camera))
                .setGalleryButtonText(getString(R.string.image_alert_action_photo_library))
                .setButtonOrientation(LinearLayout.HORIZONTAL);
        setup.setIconGravity(Gravity.TOP);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStores();
        prepareGallery();
        if (mSelectedStoreModel != null && mSelectedContractSpinner.getAdapter().getCount() > 0) {
            mSelectedContractSpinner.setSelection(mSelectedPosition);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            clearTemplateReport();
        }
    }

    //endregion Lifecycle Methods

    //region Event Methods

    @OnClick(R.id.add_store_button)
    void onAddStoreClick() {
        Intent intent = new Intent(this, AddStoreActivity.class);
        startActivityForResult(intent, PARAM_STORE_REQUEST_CODE, ActivityTransitionType.SLIDE_BOTTOM);
    }

    @OnClick(R.id.submit_button)
    void onSubmitClick() {
        attemptReportSubmission();
    }

    @OnClick(R.id.photo_button)
    void onSelectImageClick() {
        PickImageDialog
                .build(setup)
                .setOnPickCancel(() -> {
                    // Do nothing... Library null ptr fix...
                })
                .show(this);
    }

    @OnTextChanged(R.id.report_title_input)
    void onReportTitleInput() {
        mTitleInput.setError(null);
    }

    //endregion Event Methods

    //region Overridden Methods

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);
        NestedMapFragment mapFragment = (NestedMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
        mapFragment.setListener(() -> mMainScroll.requestDisallowInterceptTouchEvent(true));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PARAM_STORE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                mAdHocId = data.getLongExtra(PARAM_STORE_ID, 0);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    //endregion Overridden Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    // OnMapReadyCallback

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (!mMap.isMyLocationEnabled() &&
                ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

            Location location = TrackingUtil.getLastKnownLocation(this);
            if (location != null) {
                final double currentLatitude = location.getLatitude();
                final double currentLongitude = location.getLongitude();
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(currentLatitude, currentLongitude), MAP_SLO_ZOOM));
            }
        }

        mClusterManager = new ClusterManager<>(this, mMap);

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);

        mClusterManager.setOnClusterItemClickListener(storeClusterItem -> {
            long storeId = storeClusterItem.getStoreId();
            StoreModel storeModel = mStoreModelSparseArray.get(storeId);
            if (storeModel != null) {
                for (int i = 0; i < mSortedIdList.size(); i++) {
                    if (mSortedIdList.get(i) == storeId && i + 1 <= mSortedIdList.size()) {
                        mSelectedContractSpinner.setSelection(i + 1);
                        mSelectedStoreModel = mStoreModelSparseArray.get(storeId);
                        break;
                    }
                }
                return true;
            }
            return false;
        });

        mClusterManager.setOnClusterClickListener(cluster -> {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(),
                    mMap.getCameraPosition().zoom + 1.0f));
            return true;
        });

        mClusterManager.setRenderer(new StoreClusterRenderer(this, mMap, mClusterManager));

        for (int i = 0; i < mStoreModelSparseArray.size(); i++) {
            StoreModel storeModel = mStoreModelSparseArray.valueAt(i);
            mClusterManager.addItem(new StoreClusterItem(storeModel.getLatitude(),
                    storeModel.getLongitude(), storeModel.getName(),
                    mStoreModelSparseArray.keyAt(i)));
        }
        mClusterManager.cluster();
    }

    // AdapterView.OnItemSelectedListener

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mSelectedPosition = position;
        if (mStoreModelSparseArray != null && position < mStoreModelSparseArray.size()) {
            if (position > 0) {
                mSelectedStoreModel = mStoreModelSparseArray.get(mSortedIdList.get(position - 1));
            } else {
                mSelectedStoreModel = null;
            }
        }

        if (mMap != null && mSelectedStoreModel != null) {
            LatLng latLng = new LatLng(mSelectedStoreModel.getLatitude(),
                    mSelectedStoreModel.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Do nothing
    }

    // IPickResult

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            String imageFileName = ImageUtil.saveTempImage(this, pickResult.getBitmap());
            if (!imageFileName.isEmpty()) {
//                if (ComplianceUtil.isImageCompliant(pickResult.getPath())) {
                    addImage(imageFileName, pickResult.getPath());
//                } else {
//                    Toast.makeText(this, R.string.add_report_compliance_alert,
//                            Toast.LENGTH_SHORT).show();
//                }
            }
        } else {
            Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // ImagesAdapter.ImageInteractionListener

    @Override
    public void onClick(View view, String imagePath) {
        zoomImageFromThumb(view, imagePath);
    }

    //endregion Implementation Methods

    //region Private Methods

    private boolean loadVariables() {
        Long contactId = SettingsUtil.readLongPreference(this, PARAM_OPEN_WORKDAY_CONTRACT_ID);
        if (contactId == null || contactId <= 0) {
            return false;
        }
        mContractId = contactId;
        mReportId = System.currentTimeMillis();
        return true;
    }

    private void loadStores() {
        mStoreModelSparseArray.clear();
        mSortedIdList = new ArrayList<>();
        int selectedIdx = 0;
        List<Store> storeList = DataManager.getInstance().getStores(true);
        String[] stores = new String[storeList.size() + 1];
        stores[0] = getString(R.string.add_report_dropdown_placeholder);
        for (int i = 0; i < storeList.size(); i++) {
            Store store = storeList.get(i);
            mStoreModelSparseArray.append(store.getId(),
                    new StoreModel(store.getId(), store.getName(), store.getLatitude(), store.getLongitude()));
            stores[i + 1] = store.getName();
            mSortedIdList.add(store.getId());
            if (mAdHocId == store.getId()) {
                selectedIdx = i + 1;
            }
        }
        final ArrayAdapter<String> contractAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, stores);
        mSelectedContractSpinner.setAdapter(contractAdapter);
        mSelectedContractSpinner.setOnItemSelectedListener(this);

        if (mMap != null && mClusterManager != null) {
            mClusterManager.clearItems();
            for (int i = 0; i < mStoreModelSparseArray.size(); i++) {
                StoreModel storeModel = mStoreModelSparseArray.valueAt(i);
                mClusterManager.addItem(new StoreClusterItem(storeModel.getLatitude(),
                        storeModel.getLongitude(), storeModel.getName(),
                        mStoreModelSparseArray.keyAt(i)));
            }
            mClusterManager.cluster();
        }

        if (mAdHocId > 0) {
            mSelectedContractSpinner.setSelection(selectedIdx);
        }
    }

    private void prepareGallery() {
        mImagesRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImagesRecycler.setHasFixedSize(true);
        mImagesRecycler.setAdapter(mImagesAdapter);
    }

    private boolean addTemplateReport() {
        ReportMap.Builder builder = new ReportMap.Builder()
                .setId(mReportId)
                .setContractId(mContractId)
                .setUploaded(false);
        DataManager.getInstance().storeReport(builder.build());
        return true;
    }

    private void clearTemplateReport() {
        DataManager.getInstance().removeReport(mReportId);
        ImageUtil.clearTempImages(this);
    }

    private void addImage(String imagePath, String originalImagePath) {
        DataManager.getInstance().addImage(mReportId, imagePath, originalImagePath);
        mImagesAdapter.updateData(DataManager.getInstance().getImages(mReportId));
    }

    private void attemptReportSubmission() {
        boolean formValid = true;
        View focusView = null;

        ValidationUtil.Result descriptionValidationResult = ValidationUtil.validateRequired(this,
                mTitleInput.getText().toString(), R.string.add_report_name_title);
        if (!descriptionValidationResult.isValid()) {
            mTitleLayout.setError(descriptionValidationResult.getMessage());
            formValid = false;
            focusView = mTitleInput;
        }

        if (mSelectedStoreModel == null) {
            Toast.makeText(this, R.string.add_report_location_title, Toast.LENGTH_SHORT).show();
            formValid = false;
        }

        if (formValid) {
            showLoadingDialog(R.string.general_loading);
            submitReport();
        } else if (focusView != null) {
            focusView.requestFocus();
        }
    }

    private void submitReport() {
        ReportMap.Builder builder = new ReportMap.Builder()
                .setId(mReportId)
                .setContractId(mContractId)
                .setName(mTitleInput.getText().toString())
                .setDescription(mDescriptionInput.getText().toString())
                .setLatitude(mSelectedStoreModel.getLatitude())
                .setLongitude(mSelectedStoreModel.getLongitude())
                .setStoreName(mSelectedStoreModel.getName())
                .setStoreId(mSelectedStoreModel.getId())
                .setTime(System.currentTimeMillis())
                .setUploaded(false);
        DataManager.getInstance().storeReport(builder.build());

        if (NetworkUtils.hasInternetConnection(this)) {
            PassengerApiManager.addReport(this, mReportId, new ResponseListener() {
                @Override
                public void onSuccess() {
                    mReportId = -1;
                    hideLoadingDialog();
                    Toast.makeText(AddReportActivity.this, R.string.message_report_uploaded, Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onLoadFailure(FailureType failureType) {
                    mReportId = -1;
                    hideLoadingDialog();
                    finish();
                }
            });
        } else {
            mReportId = -1;
            hideLoadingDialog();
            Toast.makeText(AddReportActivity.this, R.string.message_report_saved, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void zoomImageFromThumb(final View thumbView, final String imagePath) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        Picasso.with(this)
                .load(imagePath)
                .into(mExpandedImageView);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        container.getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        mExpandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        mExpandedImageView.setPivotX(0f);
        mExpandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(mExpandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(mExpandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(mExpandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(mExpandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        mExpandedImageView.setOnClickListener(view -> {
            if (mCurrentAnimator != null) {
                mCurrentAnimator.cancel();
            }

            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            AnimatorSet set1 = new AnimatorSet();
            set1.play(ObjectAnimator
                    .ofFloat(mExpandedImageView, View.X, startBounds.left))
                    .with(ObjectAnimator
                            .ofFloat(mExpandedImageView,
                                    View.Y, startBounds.top))
                    .with(ObjectAnimator
                            .ofFloat(mExpandedImageView,
                                    View.SCALE_X, startScaleFinal))
                    .with(ObjectAnimator
                            .ofFloat(mExpandedImageView,
                                    View.SCALE_Y, startScaleFinal));
            set1.setDuration(mShortAnimationDuration);
            set1.setInterpolator(new DecelerateInterpolator());
            set1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    thumbView.setAlpha(1f);
                    mExpandedImageView.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    thumbView.setAlpha(1f);
                    mExpandedImageView.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }
            });
            set1.start();
            mCurrentAnimator = set1;
        });
    }

    //endregion Private Methods
}
