package si.marcelino.passenger.activities;

import static si.marcelino.passenger.enums.ActivityTransitionType.SLIDE_BOTTOM;
import static si.marcelino.passenger.utils.Constants.PARAM_AUTH_EMAIL;
import static si.marcelino.passenger.utils.Constants.PARAM_AUTH_PASSWORD;
import static si.marcelino.passenger.utils.Constants.PARAM_GEO_ALERT_SEEN;
import static si.marcelino.passenger.utils.Constants.PARAM_LAST_ALERT_SEEN;
import static si.marcelino.passenger.utils.Constants.PARAM_TITLE;
import static si.marcelino.passenger.utils.Constants.PARAM_URL;
import static si.marcelino.passenger.utils.Constants.URL_HOW_TO;
import static si.marcelino.passenger.utils.Constants.URL_PASSWORD_RESET;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import si.marcelino.passenger.BuildConfig;
import si.marcelino.passenger.R;
import si.marcelino.passenger.enums.ActivityTransitionType;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.KeycloakApiManager;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.utils.NetworkUtils;
import si.marcelino.passenger.utils.SettingsUtil;
import si.marcelino.passenger.utils.ValidationUtil;

/**
 * A login screen that offers login via email/password.
 * <p>
 * Created by Andraž Hribar on 9.2.2017.
 * andraz.hribar@gmail.com
 */
public class LoginActivity extends BaseActivity {

    //region Constants

    private static final int PARAM_REGISTRATION_REQUEST_CODE = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 990;

    //endregion Constants

    //region Variables
    //endregion Variables

    //region UI Elements

    @BindView(R.id.email_input_layout)
    TextInputLayout mEmailLayout;

    @BindView(R.id.email_input)
    AppCompatEditText mEmailInput;

    @BindView(R.id.password_input_layout)
    TextInputLayout mPasswordLayout;

    @BindView(R.id.password_input)
    AppCompatEditText mPasswordInput;

    @BindView(R.id.email_sign_in_button)
    Button mLoginButton;

    //endregion UI Elements

    public LoginActivity() {
        mShowToolbar = false;
    }

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_login);

        mPasswordInput.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE) {
                attemptLogin();
                return true;
            }
            return false;
        });

        notifyGeoUsageNecessary();

        if (BuildConfig.DEBUG) {
            devPreset();
            notifyUpdateIfNecessary();
        } else {
            String email = SettingsUtil.readStringPreference(this, PARAM_AUTH_EMAIL);
            String password = SettingsUtil.readStringPreference(this, PARAM_AUTH_PASSWORD);
            if (!TextUtils.isEmpty(email)) {
                mEmailInput.setText(email);
                if (!TextUtils.isEmpty(password)) {
                    mPasswordInput.setText(password);
                }
                notifyUpdateIfNecessary();
            }
        }
    }

    //endregion Lifecycle Methods

    //region Private Methods

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (!NetworkUtils.hasInternetConnection(this)) {
            Toast.makeText(this, R.string.general_no_network, Toast.LENGTH_LONG).show();
            return;
        }

        // Reset errors.
        mEmailLayout.setError(null);
        mPasswordLayout.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailInput.getText().toString();
        String password = mPasswordInput.getText().toString();

        boolean formValid = true;
        View focusView = null;

        ValidationUtil.Result emailValidationResult = ValidationUtil.validateEmail(this, email);
        if (!emailValidationResult.isValid()) {
            mEmailLayout.setError(emailValidationResult.getMessage());
            formValid = false;
            focusView = mEmailInput;
        }

        if (formValid) {
            SettingsUtil.storePreference(this, PARAM_AUTH_EMAIL, email);
            SettingsUtil.storePreference(this, PARAM_AUTH_PASSWORD, password);
            login(email, password);
        } else {
            focusView.requestFocus();
        }
    }

    private void login(String email, String password) {
        showLoadingDialog(R.string.general_loading);
        KeycloakApiManager.token(this, email, password, new ResponseListener() {
            @Override
            public void onSuccess() {
                loadUser();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                Toast.makeText(LoginActivity.this, R.string.general_login_error, Toast.LENGTH_SHORT).show();
                hideLoadingDialog();
            }
        });
    }

    private void loadUser() {
        PassengerApiManager.profile(this, new ResponseListener() {
            @Override
            public void onSuccess() {
                loadContracts();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
            }
        });
    }

    private void loadContracts() {
        PassengerApiManager.getContracts(this, new ResponseListener() {
            @Override
            public void onSuccess() {
                startActivity(createIntent(WorkdayActivity.class, true), SLIDE_BOTTOM);
                hideLoadingDialog();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
            }
        });
    }

    private void notifyUpdateIfNecessary() {
        boolean isFirstInstall = true;
        for (int i = 0; i <= BuildConfig.VERSION_CODE; i++) {
            Boolean las = SettingsUtil.readBooleanPreference(this, PARAM_LAST_ALERT_SEEN + i);
            if (las != null && las) {
                isFirstInstall = false;
                break;
            }
        }
        if (isFirstInstall) {
            SettingsUtil.storePreference(
                    LoginActivity.this,
                    PARAM_LAST_ALERT_SEEN + BuildConfig.VERSION_CODE,
                    true);
            return;
        }

        Boolean lastAlertSeen = SettingsUtil.readBooleanPreference(this, PARAM_LAST_ALERT_SEEN + BuildConfig.VERSION_CODE);
        if (lastAlertSeen != null && lastAlertSeen) {
            return;
        }
        new MaterialDialog.Builder(LoginActivity.this)
                .title(android.R.string.dialog_alert_title)
                .content(R.string.message_update_logout)
                .neutralText(android.R.string.ok)
                .onNeutral((dialog, which) -> SettingsUtil.storePreference(
                        LoginActivity.this,
                        PARAM_LAST_ALERT_SEEN + BuildConfig.VERSION_CODE,
                        true)
                )
                .show();
    }

    private void notifyGeoUsageNecessary() {
        Boolean geoAlertSeen = SettingsUtil.readBooleanPreference(this, PARAM_GEO_ALERT_SEEN);
        if (geoAlertSeen != null && geoAlertSeen) {
            return;
        }
        new MaterialDialog.Builder(LoginActivity.this)
                .title(R.string.dialog_geo_alert_title)
                .content(R.string.dialog_geo_alert_message)
                .positiveText(R.string.dialog_allow)
                .onPositive((dialog, which) -> {
                    SettingsUtil.storePreference(LoginActivity.this, PARAM_GEO_ALERT_SEEN, true);
                    checkLocationPermission();
                })
                .negativeText(R.string.dialog_decline)
                .onNegative((dialog, which) -> SettingsUtil.storePreference(
                        LoginActivity.this, PARAM_GEO_ALERT_SEEN, false)
                )
                .show();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.workday_gps_dialog_title)
                        .setMessage(R.string.workday_gps_dialog_message)
                        .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                            checkLocationPermission();
                        })
                        .setNegativeButton(android.R.string.no, (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    //endregion Private Methods

    //region Event Methods

    @OnClick(R.id.email_sign_in_button)
    void onSingInClicked() {
        attemptLogin();
    }

    @OnClick(R.id.email_sign_up_button)
    void onSingUpClicked() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivityForResult(intent, PARAM_REGISTRATION_REQUEST_CODE, SLIDE_BOTTOM);
    }

    @OnClick(R.id.forgotten_password_button)
    void onForgottenPasswordClick() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(PARAM_URL, URL_PASSWORD_RESET);
        intent.putExtra(PARAM_TITLE, R.string.login_label_forgotten_password);
        startActivity(intent, ActivityTransitionType.SLIDE_BOTTOM);
    }

    @OnClick(R.id.how_to_button)
    void onHowToClicked() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(PARAM_URL, URL_HOW_TO);
        intent.putExtra(PARAM_TITLE, R.string.toolbar_title_how_to);
        startActivity(intent, ActivityTransitionType.SLIDE_BOTTOM);
    }

    @OnTextChanged(R.id.email_input)
    void onEmailInput() {
        mEmailInput.setError(null);
    }

    @OnTextChanged(R.id.password_input)
    void onPasswordInput() {
        mPasswordInput.setError(null);
    }

    //endregion Event Methods

    //region Overridden Methods

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PARAM_REGISTRATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                mEmailInput.setText(data.getStringExtra(PARAM_AUTH_EMAIL));
                mPasswordInput.setText(data.getStringExtra(PARAM_AUTH_PASSWORD));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    //endregion Overridden Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods

    //region Dev Methods

    private void devPreset() {
        mEmailInput.setText("test@marcelino.si");
        mPasswordInput.setText("TestTest123.");
    }

    //endregion Dev Methods
}

