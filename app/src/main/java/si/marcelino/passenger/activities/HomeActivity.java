package si.marcelino.passenger.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.OnClick;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.domain.Report;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.rest.models.data.LocationData;
import si.marcelino.passenger.rest.utils.NetworkUtils;
import si.marcelino.passenger.rest.utils.WorkdayUtils;
import si.marcelino.passenger.services.ImageTransferService;
import si.marcelino.passenger.services.TrackingService;
import si.marcelino.passenger.utils.SettingsUtil;

import static si.marcelino.passenger.enums.ActivityTransitionType.SLIDE_BOTTOM;
import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_CONTRACT_ID;
import static si.marcelino.passenger.utils.Constants.PARAM_OPEN_WORKDAY_ID;

/**
 * The main menu activity that represents the user's main entry point.
 * <p>
 * Created by Andraž Hribar on 20.3.2017.
 * andraz.hribar@gmail.com
 */

public class HomeActivity extends BaseActivity {

    //region UI Elements
    //endregion UI Elements

    //region Variables

    private boolean mUploadError = false;

    //endregion Variables

    //region Lifecycle Methods

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_home);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DataManager.getInstance().updateStoreDistance(this);
    }

    //endregion Lifecycle Methods

    //region Overridden Methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_profile) {
            startActivity(createIntent(UserProfileActivity.class), SLIDE_BOTTOM);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initializeActivity(Activity target, @LayoutRes int layoutResID) {
        super.initializeActivity(target, layoutResID);
    }

    @Override
    protected void hideLoadingDialog() {
        super.hideLoadingDialog();
        if (mUploadError) {
            Toast.makeText(this, R.string.message_reports_upload_error, Toast.LENGTH_SHORT).show();
            mUploadError = false;
        }
    }

    //endregion Overridden Methods

    //region Event Methods

    @OnClick(R.id.add_report_button)
    void onAddReportClick() {
        startActivity(new Intent(this, AddReportActivity.class), SLIDE_BOTTOM);
    }

    @OnClick(R.id.reports_button)
    void onReportsClick() {
        startActivity(createIntent(ReportsActivity.class), SLIDE_BOTTOM);
    }

    @OnClick(R.id.stores_button)
    void onStoresClick() {
        startActivity(createIntent(StoresActivity.class), SLIDE_BOTTOM);
    }

    @OnClick(R.id.export_button)
    void onExportClick() {
        if (!NetworkUtils.hasInternetConnection(this)) {
            Toast.makeText(this, R.string.message_report_offline, Toast.LENGTH_LONG).show();
            return;
        }
        Long workdayId = SettingsUtil.readLongPreference(this, PARAM_OPEN_WORKDAY_CONTRACT_ID);
        if (workdayId == null) {
            Toast.makeText(this, R.string.message_report_load_error, Toast.LENGTH_SHORT).show();
            return;
        }

        List<Report> reportList = DataManager.getInstance().getPartialReports(workdayId);
//        if (reportList.isEmpty()) {
//            reportList = DataManager.getInstance().getPartialReports(workdayId);
//        }
        if (reportList.isEmpty()) {
            Toast.makeText(this, R.string.message_reports_uploaded, Toast.LENGTH_SHORT).show();
            return;
        }

        showLoadingDialog();
        Set<Long> reportIdSet = new HashSet<>(reportList.size());
        for (Report report : reportList) {
            if (!report.isValid()) {
                continue;
            }
            final Long reportId = report.getId();
            if (reportId != null) {
                reportIdSet.add(reportId);
                PassengerApiManager.addReport(this, reportId, new ResponseListener() {
                    @Override
                    public void onSuccess() {
                        reportIdSet.remove(reportId);
                        if (reportIdSet.isEmpty()) {
                            hideLoadingDialog();
                        }
                    }

                    @Override
                    public void onLoadFailure(FailureType failureType) {
                        mUploadError = true;
                        reportIdSet.remove(reportId);
                        if (reportIdSet.isEmpty()) {
                            hideLoadingDialog();
                        }
                    }
                });
            }
        }
    }

    @OnClick(R.id.logout_button)
    void onLogoutClick() {
        if (!NetworkUtils.hasInternetConnection(this)) {
            new MaterialDialog.Builder(HomeActivity.this)
                    .title(android.R.string.dialog_alert_title)
                    .content(R.string.message_offline_end_workday)
                    .positiveText(android.R.string.yes)
                    .negativeText(android.R.string.no)
                    .onPositive((dialog, which) -> clearWorkday())
                    .show();
        } else {
            logWorkday();
        }
    }

    //endregion Event Methods

    //region Private Methods

    private void logWorkday() {
        showLoadingDialog(R.string.workday_label_ending);
        PassengerApiManager.addLocations(this, false, new ResponseListener() {
            @Override
            public void onSuccess() {
                uploadReports();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                List<LocationData> locations = DataManager.getInstance().getLocations(HomeActivity.this);
                if (locations == null || locations.isEmpty()) {
                    if (DataManager.getInstance().getLocationsCount() > 0) {
                        uploadReports();
                        return;
                    }

                    if (!mActivityLive) {
                        return;
                    }

                    new MaterialDialog.Builder(HomeActivity.this)
                            .title(android.R.string.dialog_alert_title)
                            .content(R.string.message_no_locations)
                            .positiveText(android.R.string.yes)
                            .negativeText(android.R.string.no)
                            .onPositive((dialog, which) -> uploadReports())
                            .show();
                }
            }
        });
    }

    private void uploadReports() {
        showLoadingDialog(R.string.workday_label_ending);
        Long workdayId = SettingsUtil.readLongPreference(this, PARAM_OPEN_WORKDAY_CONTRACT_ID);
        if (workdayId == null) {
            hideLoadingDialog();
            if (!mActivityLive) {
                return;
            }
            new MaterialDialog.Builder(HomeActivity.this)
                    .title(android.R.string.dialog_alert_title)
                    .content(R.string.message_reports_upload_end_workday_error)
                    .positiveText(android.R.string.yes)
                    .negativeText(android.R.string.no)
                    .onPositive((dialog, which) -> endWorkday())
                    .show();
            return;
        }

        List<Report> reportList = DataManager.getInstance().getLocalReports(workdayId);
        if (reportList.isEmpty()) {
            endWorkday();
            return;
        }
        Set<Long> reportIdSet = new HashSet<>(reportList.size());
        List<Long> errorIdList = new ArrayList<>();
        for (Report report : reportList) {
            if (report.isValid() && report.getId() != null) {
                final long reportId = report.getId();
                reportIdSet.add(reportId);
                PassengerApiManager.addReport(this, reportId, new ResponseListener() {
                    @Override
                    public void onSuccess() {
                        reportIdSet.remove(reportId);
                        if (reportIdSet.isEmpty()) {
                            if (!errorIdList.isEmpty()) {
                                hideLoadingDialog();
                                if (!mActivityLive) {
                                    return;
                                }
                                new MaterialDialog.Builder(HomeActivity.this)
                                        .title(android.R.string.dialog_alert_title)
                                        .content(R.string.message_reports_upload_end_workday_error)
                                        .positiveText(android.R.string.yes)
                                        .negativeText(android.R.string.no)
                                        .onPositive((dialog, which) -> endWorkday())
                                        .show();
                            } else {
                                endWorkday();
                            }
                        }
                    }

                    @Override
                    public void onLoadFailure(FailureType failureType) {
                        errorIdList.add(reportId);
                        reportIdSet.remove(reportId);
                        if (reportIdSet.isEmpty()) {
                            hideLoadingDialog();
                            if (!mActivityLive) {
                                return;
                            }
                            new MaterialDialog.Builder(HomeActivity.this)
                                    .title(android.R.string.dialog_alert_title)
                                    .content(R.string.message_reports_upload_end_workday_error)
                                    .positiveText(android.R.string.yes)
                                    .negativeText(android.R.string.no)
                                    .onPositive((dialog, which) -> endWorkday())
                                    .show();
                        }
                    }
                });
            }
        }
    }

    private void endWorkday() {
        showLoadingDialog(R.string.workday_label_ending);
        new WorkdayUtils(new ResponseListener() {
            @Override
            public void onSuccess() {
                clearWorkday();
                hideLoadingDialog();
            }

            @Override
            public void onLoadFailure(FailureType failureType) {
                hideLoadingDialog();
                Toast.makeText(HomeActivity.this, R.string.message_workday_end_error, Toast.LENGTH_SHORT).show();
            }
        }).fetchAndCloseOpenedWorkdays(this);
    }

    private void clearWorkday() {
        SettingsUtil.clearPreference(HomeActivity.this, PARAM_OPEN_WORKDAY_ID);
        SettingsUtil.clearPreference(HomeActivity.this, PARAM_OPEN_WORKDAY_CONTRACT_ID);
        stopService(new Intent(HomeActivity.this, TrackingService.class));
        stopService(new Intent(HomeActivity.this, ImageTransferService.class));
        DataManager.getInstance().clearCache();
        startActivity(createIntent(WorkdayActivity.class, true), SLIDE_BOTTOM);
    }

    //endregion Private Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods
}
