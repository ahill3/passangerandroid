package si.marcelino.passenger.activities;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.widget.AppCompatEditText;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import si.marcelino.passenger.R;
import si.marcelino.passenger.enums.ActivityTransitionType;
import si.marcelino.passenger.rest.enums.FailureType;
import si.marcelino.passenger.rest.interfaces.listeners.ResponseListener;
import si.marcelino.passenger.rest.managers.PassengerApiManager;
import si.marcelino.passenger.utils.ValidationUtil;

import static si.marcelino.passenger.utils.Constants.PARAM_AUTH_EMAIL;
import static si.marcelino.passenger.utils.Constants.PARAM_AUTH_PASSWORD;
import static si.marcelino.passenger.utils.Constants.PARAM_TITLE;
import static si.marcelino.passenger.utils.Constants.PARAM_URL;
import static si.marcelino.passenger.utils.Constants.URL_PRIVACY_POLICY;

/**
 * Created by Andraž Hribar on 25. 04. 2017.
 * andraz.hribar@gmail.com
 */
public class RegistrationActivity extends BaseActivity {

    //region UI Elements

    @BindView(R.id.email_input_layout)
    TextInputLayout mEmailInputLayout;

    @BindView(R.id.password_input_layout)
    TextInputLayout mPasswordInputLayout;

    @BindView(R.id.re_password_input_layout)
    TextInputLayout mRePasswordInputLayout;

    @BindView(R.id.first_name_input_layout)
    TextInputLayout mFirstNameInputLayout;

    @BindView(R.id.last_name_input_layout)
    TextInputLayout mLastNameInputLayout;

    @BindView(R.id.phone_input_layout)
    TextInputLayout mPhoneInputLayout;

    @BindView(R.id.address_input_layout)
    TextInputLayout mAddressInputLayout;

    @BindView(R.id.zip_input_layout)
    TextInputLayout mZipInputLayout;

    @BindView(R.id.city_input_layout)
    TextInputLayout mCityInputLayout;

    @BindView(R.id.email_input)
    AppCompatEditText mEmailInput;

    @BindView(R.id.password_input)
    AppCompatEditText mPasswordInput;

    @BindView(R.id.re_password_input)
    AppCompatEditText mRePasswordInput;

    @BindView(R.id.first_name_input)
    AppCompatEditText mFirstNameInput;

    @BindView(R.id.last_name_input)
    AppCompatEditText mLastNameInput;

    @BindView(R.id.phone_input)
    AppCompatEditText mPhoneInput;

    @BindView(R.id.address_input)
    AppCompatEditText mAddressInput;

    @BindView(R.id.zip_input)
    AppCompatEditText mZipInput;

    @BindView(R.id.city_input)
    AppCompatEditText mCityInput;

    //endregion UI Elements

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_registration);
    }

    //endregion Lifecycle Methods

    //region Private Methods

    private void resetErrors() {
        mEmailInputLayout.setError(null);
        mPasswordInputLayout.setError(null);
        mRePasswordInputLayout.setError(null);
        mFirstNameInput.setError(null);
        mLastNameInputLayout.setError(null);
        mPhoneInputLayout.setError(null);
        mAddressInputLayout.setError(null);
        mZipInputLayout.setError(null);
        mCityInputLayout.setError(null);
    }

    private void attemptRegistration() {
        resetErrors();

        // Store values at the time of the login attempt.
        String email = mEmailInput.getText().toString();
        String password = mPasswordInput.getText().toString();
        String rePassword = mRePasswordInput.getText().toString();
        String firstName = mFirstNameInput.getText().toString();
        String lastName = mLastNameInput.getText().toString();
        String phone = mPhoneInput.getText().toString();
        String address = mAddressInput.getText().toString();
        String zip = mZipInput.getText().toString();
        int zipCode = 0;
        String city = mCityInput.getText().toString();

        boolean formValid = true;
        View focusView = null;

        //region Validations

        ValidationUtil.Result cityValidationResult = ValidationUtil.validateRequired(this, city, R.string.label_city);
        if (!cityValidationResult.isValid()) {
            mCityInputLayout.setError(cityValidationResult.getMessage());
            formValid = false;
            focusView = mCityInput;
        }

        ValidationUtil.Result zipValidationResult = ValidationUtil.validateRequiredNumber(this, zip, R.string.label_zip);
        if (!zipValidationResult.isValid()) {
            mZipInputLayout.setError(zipValidationResult.getMessage());
            formValid = false;
            focusView = mZipInput;
        } else {
            try {
                zipCode = Integer.parseInt(zip);
            } catch (NumberFormatException e) {
                mZipInputLayout.setError(zipValidationResult.getMessage());
                formValid = false;
                focusView = mZipInput;
            }
        }

        ValidationUtil.Result addressValidationResult = ValidationUtil.validateRequired(this, address, R.string.label_address);
        if (!addressValidationResult.isValid()) {
            mAddressInputLayout.setError(addressValidationResult.getMessage());
            formValid = false;
            focusView = mAddressInput;
        }

        ValidationUtil.Result phoneValidationResult = ValidationUtil.validateRequired(this, phone, R.string.label_phone);
        if (!phoneValidationResult.isValid()) {
            mPhoneInputLayout.setError(phoneValidationResult.getMessage());
            formValid = false;
            focusView = mPhoneInput;
        }

        ValidationUtil.Result lastNameValidationResult = ValidationUtil.validateRequired(this, lastName, R.string.label_last_name);
        if (!lastNameValidationResult.isValid()) {
            mLastNameInputLayout.setError(lastNameValidationResult.getMessage());
            formValid = false;
            focusView = mLastNameInput;
        }

        ValidationUtil.Result firstNameValidationResult = ValidationUtil.validateRequired(this, firstName, R.string.label_first_name);
        if (!firstNameValidationResult.isValid()) {
            mFirstNameInputLayout.setError(firstNameValidationResult.getMessage());
            formValid = false;
            focusView = mFirstNameInput;
        }

        ValidationUtil.Result passwordValidationResult = ValidationUtil.validateLength(this, password, 4, R.string.label_password);
        if (!passwordValidationResult.isValid()) {
            mPasswordInputLayout.setError(passwordValidationResult.getMessage());
            formValid = false;
            focusView = mPasswordInput;
        }

        ValidationUtil.Result rePasswordValidationResult = ValidationUtil.validateRetype(this, rePassword, password, R.string.label_retype_passord);
        if (!rePasswordValidationResult.isValid()) {
            mRePasswordInputLayout.setError(rePasswordValidationResult.getMessage());
            formValid = false;
            focusView = mRePasswordInput;
        }

        ValidationUtil.Result emailValidationResult = ValidationUtil.validateEmail(this, email);
        if (!emailValidationResult.isValid()) {
            mEmailInputLayout.setError(emailValidationResult.getMessage());
            formValid = false;
            focusView = mEmailInput;
        }

        //endregion Validations

        if (formValid) {
            register(email, password, firstName, lastName, phone, address, zipCode, city);
        } else {
            focusView.requestFocus();
        }
    }

    private void register(String email, String password, String firstName, String lastName,
                          String phone, String address, int zip, String city) {
        PassengerApiManager.register(this,
                address, email, firstName, lastName, password, phone, zip, city,
                new ResponseListener() {
                    @Override
                    public void onSuccess() {
                        Intent data = new Intent();
                        data.putExtra(PARAM_AUTH_EMAIL, email);
                        data.putExtra(PARAM_AUTH_PASSWORD, password);
                        setResult(RESULT_OK, data);
                        finish();
                    }

                    @Override
                    public void onLoadFailure(FailureType failureType) {
                        Toast.makeText(RegistrationActivity.this, failureType.getErrorMessageResId(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //endregion Private Methods

    //region Event Methods

    @OnClick(R.id.submit_button)
    void onSubmitClick() {
        attemptRegistration();
    }

    @OnClick(R.id.privacy_policy_button)
    void onPrivacyPolicyClick() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(PARAM_URL, URL_PRIVACY_POLICY);
        intent.putExtra(PARAM_TITLE, R.string.toolbar_title_privacy_policy);
        startActivity(intent, ActivityTransitionType.SLIDE_BOTTOM);
    }

    @OnTextChanged(R.id.email_input)
    void onEmailInput() {
        mEmailInputLayout.setError(null);
    }

    @OnTextChanged(R.id.password_input)
    void onPasswordInput() {
        mPasswordInputLayout.setError(null);
    }

    @OnTextChanged(R.id.re_password_input)
    void onRePasswordInput() {
        mRePasswordInputLayout.setError(null);
    }

    @OnTextChanged(R.id.first_name_input)
    void onFirstNameInput() {
        mFirstNameInputLayout.setError(null);
    }

    @OnTextChanged(R.id.last_name_input)
    void onLastNameInput() {
        mLastNameInputLayout.setError(null);
    }

    @OnTextChanged(R.id.phone_input)
    void onPhoneInput() {
        mPhoneInputLayout.setError(null);
    }

    @OnTextChanged(R.id.address_input)
    void onAddressInput() {
        mAddressInputLayout.setError(null);
    }

    @OnTextChanged(R.id.zip_input)
    void onZipInput() {
        mZipInputLayout.setError(null);
    }

    @OnTextChanged(R.id.city_input)
    void onCityInput() {
        mCityInputLayout.setError(null);
    }

    @OnFocusChange(R.id.re_password_input)
    void onRePasswordFocusChange(boolean hasFocus) {
        if (hasFocus) {
            return;
        }
        String password = mPasswordInput.getText().toString();
        String rePassword = mRePasswordInput.getText().toString();
        if (password.equals(rePassword)) {
            return;
        }
        ValidationUtil.Result rePasswordValidationResult = ValidationUtil.validateRetype(this, rePassword, password, R.string.label_retype_passord);
        if (!rePasswordValidationResult.isValid()) {
            mRePasswordInputLayout.setError(rePasswordValidationResult.getMessage());
        }
    }

    //endregion Event Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    //endregion Implementation Methods
}
