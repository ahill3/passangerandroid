package si.marcelino.passenger.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import si.marcelino.passenger.R;
import si.marcelino.passenger.cache.DataManager;
import si.marcelino.passenger.cache.models.mapping.StoreMap;
import si.marcelino.passenger.utils.TrackingUtil;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static si.marcelino.passenger.utils.Constants.PARAM_STORE_ID;
import static si.marcelino.passenger.utils.Constants.PARAM_STORE_LAT;
import static si.marcelino.passenger.utils.Constants.PARAM_STORE_LNG;
import static si.marcelino.passenger.utils.Constants.PARAM_STORE_NAME;

/**
 * Created by Andraž Hribar on 17.4.2017.
 * andraz.hribar@gmail.com
 */

public class AddStoreActivity extends BaseActivity implements OnMapReadyCallback {

    //region Variables

    private GoogleMap mMap;
    List<String> mLocationProviders;

    private Double mSelectedLatitude;
    private Double mSelectedLongitude;

    //endregion Variables

    //region UI Element

    @BindView(R.id.store_name_input)
    EditText mStoreNameInput;

    @BindView(R.id.store_phone_input)
    EditText mStorePhoneInput;

    //endregion UI Element

    //region Lifecycle Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_add_store);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        mLocationProvider = String.valueOf(locationManager.getBestProvider(criteria, true));
        mLocationProviders = locationManager.getProviders(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
    }

    //endregion Lifecycle Methods

    //region Event Methods

    @OnClick(R.id.locate_button)
    void onUseMyLocationClick() {
        locate();
    }

    @OnClick(R.id.submit_button)
    void onSubmitClick() {
        boolean locationSet = mSelectedLatitude != null && mSelectedLongitude != null || locate();
        if (!locationSet) {
            Toast.makeText(this, R.string.message_no_location_selected, Toast.LENGTH_SHORT).show();
            return;
        }

        long storeId = System.currentTimeMillis();
        StoreMap.Builder builder = new StoreMap.Builder()
                .setId(storeId)
                .setName(mStoreNameInput.getText().toString())
                .setPhone(mStorePhoneInput.getText().toString())
                .setLatitude(mSelectedLatitude)
                .setLongitude(mSelectedLongitude)
                .setLocal(true);

        Location currentLocation = TrackingUtil.getLastKnownLocation(this);
        if (currentLocation != null) {
            Location storeLocation = new Location("store");
            storeLocation.setLatitude(mSelectedLatitude);
            storeLocation.setLongitude(mSelectedLongitude);
            builder.setDistance((double) currentLocation.distanceTo(storeLocation));
        }

        DataManager.getInstance().addStore(builder.build());
        Intent data = new Intent();
        data.putExtra(PARAM_STORE_ID, storeId);
        data.putExtra(PARAM_STORE_NAME, mStoreNameInput.getText().toString());
        data.putExtra(PARAM_STORE_LAT, mSelectedLatitude);
        data.putExtra(PARAM_STORE_LNG, mSelectedLongitude);
        setResult(RESULT_OK, data);
        finish();
    }

    //endregion Event Methods

    //region Private Methods

    private boolean locate() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            Location location = TrackingUtil.getLastKnownLocation(this);
            if (location != null) {
                mSelectedLatitude = location.getLatitude();
                mSelectedLongitude = location.getLongitude();
                if (mMap == null) {
                    return true;
                }

                LatLng latLng = new LatLng(mSelectedLatitude, mSelectedLongitude);
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place)));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                return true;
            }
        }
        return false;
    }

    //endregion Private Methods

    //region Implementation Methods

    @Override
    void setupTag() {
        setTag(getLocalClassName());
    }

    // OnMapReadyCallback

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            locate();
        }

        mMap.setOnMapClickListener(latLng -> {
            mSelectedLatitude = latLng.latitude;
            mSelectedLongitude = latLng.longitude;
            if (mMap != null) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place)));
            }
        });
    }

    //endregion Implementation Methods
}
